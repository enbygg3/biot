package require http
package require biot::uri
package require utils

namespace eval ::biot::src::http {
    namespace eval vars {
	variable -timeout    20;  # Timeouts for HTTP calls, in seconds.
    }
    namespace export {[a-z]*}
    [namespace parent]::register http* \
	-get [namespace current]::get \
	-resolver [list https [list ssl tls]] \
	-defaults [namespace current]::defaults
}

# ::biot::src::http::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::http::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


# ::biot::src::http::get -- Get the content of a source
#
#       Initiate fetching of the values contained at an URL that is
#       the resolved URL of the source.  Use the hints, that have
#       typically been extracted from the original scheme, to perform
#       the proper HTTP operation.  Arrange for variable extraction to
#       occur on reception.
#
# Arguments:
#	s	Identifier of the source.
#	url	URL to fetch.
#	hints	Hints (from the original scheme).
#
# Results:
#       None.
#
# Side Effects:
#       All network operations necessary to get the information at the
#       URL.
proc ::biot::src::http::get { s url hints } {
    global BDG

    upvar \#0 $s SRC

    # Now initiate the construction of an HTTP operation with the
    # proper headers, i.e. headers as specified at the destination,
    # but also authentication information if necessary.
    set auth [::biot::uri::auth $url curl]
    set hdrs $SRC(-headers)
    if { $auth ne "" } {
	lappend hdrs "Authorization" "Basic [::base64::encode $auth]"
    }
    set cmd [list ::http::geturl $curl -headers $hdrs]

    # Add global timeout information, if necessary
    if { ${vars::-timeout} > 0 } {
	lappend cmd -timeout [expr {int(${vars::-timeout}*1000)}]
    }

    # Specify HTTP method, if specified as part of the hints.
    if { [lsearch -nocase $hints "PUT"] >= 0 } {
	lappend cmd -method PUT
    } elseif { [lsearch -nocase $hints "DELETE"] >= 0 } {
	lappend cmd -method DELETE
    } elseif { [lsearch -nocase $hints "POST"] >= 0 } {
	lappend cmd -method POST
    }

    # Post data out of request
    if { $SRC(-request) ne "" } {
	lappend cmd -query $SRC(-request)
    }

    lappend cmd -command [list ::biot::src::http::Got $s $hints]
    if { [catch {eval $cmd} err] } {
	::utils::debug 1 "Could not get URL at $curl for $SRC(-name): $err" \
	    source
	if { $SRC(-freq) ne "" && $SRC(-freq) >= 0 } {
	    set when [expr {int($SRC(-freq)*1000)}]
	    set SRC(timer) [after $when [list $s connect]]
	}
    } else {
	::utils::debug 4 "Getting source data from $curl" source
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::src::http::Got -- Trigger analysis on acquired data.
#
#       This procedure analyses data that has been acquired from the
#       source on success.  The text first passes through the filter
#       transformations (see ::source:transform) and is then analysed
#       depending on its type.  There are three recognised types: TPL
#       will send data through a generic templater.  RX will perform
#       regular expression extraction and is able to extract specific
#       sub-groups.  Finally, JSON is able to select part of the data
#       tree and to consider fields at the selection point.
#
# Arguments:
#	s	Identifier of the source
#	tok	HTTP token
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::http::Got { s hints tok } {
    upvar \#0 $s SRC

    # Get HTTP numerical code and trigger analysis on 200+, i.e. the
    # OK codes!
    set code [::http::ncode $tok]
    if { $code ne "" && $code >= 200 && $code < 300 } {
	# Get data from HTTP and transform it through initial filters.
	set data [::http::data $tok]
	$s activity $hints
	$s push [[namespace parent]::trimmer $data $hints]
    } else {
	::utils::debug 2 "Could not get $SRC(-url), server responded\
                          $code -- [::http::error $tok]\
                          -- [::http::status $tok]" source
    }

    ::http::cleanup $tok

    if { $SRC(-freq) >= 0 } {
	# Schedule a new get...
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [list $s connect]]
    }
}


package provide biot::src::http 0.1

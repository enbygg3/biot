package require xmpp
package require xmpp::transport::bosh
package require xmpp::auth
package require xmpp::sasl
package require xmpp::starttls
package require xmpp::delay

namespace eval ::biot::src::xmpp {
    namespace eval vars {
	variable -sport   5223
	variable -port    5222
    }
    namespace export {[a-z]*}
    [namespace parent]::register xmpp* \
	-get [namespace current]::get
}

# ::biot::src::xmpp::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::xmpp::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


# ::biot::src::xmpp::get -- Open XMPP connection to acquire data
#
#       This procedure opens an XMPP connection to a remote server and
#       arrange to get data to be analysed for variable extraction.
#       When a frequency is specified, the content of the request will
#       be sent to the remote at regular intervals (for polling).  The
#       'from' field, in the headers, will be used to login at the
#       remote server (and report presence).  Message content will
#       either be got from the -body field or from iq-type messages.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to remote XMPP server
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       Will keep an XMPP connection open.
proc ::biot::src::xmpp::get { s url hints } {
    upvar \#0 $s SRC
    array set HDRS $SRC(-headers)
    if { [info exists HDRS(from)] } {
	if { ![info exists SRC(xmpp:tok)] } {
	    set SRC(xmpp:tok) [[namespace current]::Login $HDRS(from) \
				   [list [namespace current]::Receiver $s $url $hints]\
				   [list [namespace current]::Presence $s]]

	}
    }

    if { [info exists SRC(xmpp:tok)] } {
	if { $SRC(timer) ne "" } {
	    catch {after cancel $SRC(timer)}
	}
	set SRC(timer) [after idle [list [namespace current]::Poll $s $url $hints]]
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################



proc ::biot::src::xmpp::Receiver { s url hints mtype xtok from type x args } {
    ::utils::debug 5 "Receiving XMPP $mtype from $from, type $type" source
    upvar \#0 $s SRC

    switch -- $mtype {
	"msg" {
	    set triggered 0

	    # Look for body, if any
	    foreach {k v} $args {
		switch -- $k {
		    "-body" {
			$s activity $hints
			$s push [[namespace parent]::trimmer $v $hints]
			set triggered 1
		    }
		}
	    }

	    # Convert content of (IQ) message back to proper XML and send it
	    # for extraction
	    if { !$triggered && [llength $x] > 0 } {
		$s activity $hints
		set v [eval [linsert $x 0 ::xmpp::xml::toText]]
		$s push [[namespace parent]::trimmer $v $hints]
		set triggered 1
	    }
	}
	"iq" {
	}
    }
}

proc ::biot::src::xmpp::Presence { s xtok from type x args } {
    ::utils::debug 5 "Receiving XMPP presence data from $from,\
                      type $type" source
    upvar \#0 $s SRC

    set idx -1
    if { [info exists SRC(xmpp:friends)] } {
	set idx [lsearch $SRC(xmpp:friends) $from]
    }

    if { $type eq "" } {
	if { $idx < 0 } {
	    ::utils::debug 4 "Appending $from to list of present friends"
	    lappend SRC(xmpp:friends) $from
	}
    } else {
	if { $idx >= 0 } {
	    ::utils::debug 4 "Removing $from from list of present friends"
	    set SRC(xmpp:friends) [lreplace $SRC(xmpp:friends) $idx $idx]
	}
    }
}

proc ::biot::src::xmpp::Got { s args } {
    # Empty on purpose, but must exist!
}

proc ::biot::src::xmpp::Send { s to hints } {
    upvar \#0 $s SRC
    if { [info exists SRC(xmpp:tok)] } {
	if { [lsearch -nocase $hints "iq"] >= 0 } {
	    ::utils::debug 5 "Sending IQ request to remote JID $to" source
	    set xqry [::xmpp::xml::parseData $SRC(-request)]
	    ::xmpp::sendIQ $SRC(xmpp:tok) get \
		-command [list [namespace current]::Got $s] \
		-to $to \
		-query {*}$xqry
	} else {
	    ::utils::debug 5 "Sending message request to remote JID $to" source
	    ::xmpp::sendMessage $SRC(xmpp:tok) $to -body $SRC(-request)
	}
    }
}

proc ::biot::src::xmpp::Poll { s url hints } {
    upvar \#0 $s SRC
    if { [info exists SRC(xmpp:tok)] } {
	set to [regsub "^xmpp://" $url ""]
	if { [::xmpp::jid::removeResource $to] eq $to } {
	    if { [info exists SRC(xmpp:friends)] } {
		foreach dst $SRC(xmpp:friends) {
		    if { [::xmpp::jid::removeResource $dst] eq $to } {
			[namespace current]::Send $s $dst $hints
		    }
		}
	    } else {
		::utils::debug 3 "No presence information available yet"
	    }
	} else {
	    [namespace current]::Send $s $to $hints
	}
    }

    if { $SRC(-freq) ne "" && $SRC(-freq) >= 0 } {
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [list [namespace current]::Poll $s $url $hints]]
    }	
}


proc ::biot::src::xmpp::Login { from mcmd {pcmd ""} } {
    ::utils::debug 3 "Initialising XMPP connection from $from"
    set xmpp [::xmpp::new \
		  -messagecommand [linsert $mcmd end msg]\
		  -presencecommand $pcmd \
		  -iqcommand [linsert $mcmd end iq]]

    set srv [info hostname]
    set prt 0
    set transport ""
    set lurl ""
    set starttls 0
    set resource ""
    set uname ""
    set paswd ""
    set lhints [uri hints $from hstpath]
    if { [lsearch -nocase $lhints "bosh"] >= 0 } {
	set transport bosh
	set lurl $hstpath
    } elseif { [lsearch -glob -nocase $lhints "xmpp*"] >= 0 } {
	::xmpp::jid::split $hstpath node domain resource
	foreach {srv prt} [split $domain ":"] break
	foreach {uname paswd} [split $node ":"] break
	if { [lsearch -glob -nocase $lhints "xmpps"] >= 0 \
		 || [lsearch -glob -nocase $lhints "tls"] >= 0 } {
	    set transport tls
	    if { $prt eq "" } { set prt ${vars::-sport} }
	} elseif { [lsearch -glob -nocase $lhints "starttls"] >= 0 } {
	    set transport tcp
	    if { $prt eq "" } { set prt ${vars::-port}}
	    set starttls 1
	}
    } else {
	# Default to starttls!
	::xmpp::jid::split $hstpath node domain resource
	foreach {srv prt} [split $domain ":"] break
	foreach {uname paswd} [split $node ":"] break
	set transport tcp
	if { $prt eq "" } { set prt ${vars::-port} }
	set starttls 1
    }
    
    ::utils::debug 4 "Connecting to XMPP server ${srv}:${prt}\
                      (transport: $transport, url: $lurl)"
    ::xmpp::connect $xmpp $srv $prt \
	-transport $transport \
	-url $lurl
    
    ::utils::debug 4 "Opening XMPP Stream"
    if { $starttls } {
	set sid [::xmpp::openStream $xmpp $srv -version 1.0]
	::utils::debug 4 "Switching stream connection to\
                          TLS encryption"
	::xmpp::starttls::starttls $xmpp
	::utils::debug 4 "Logging in as ${uname}:${paswd},\
                          resource: $resource"
	::xmpp::sasl::auth $xmpp \
	    -username $uname \
	    -password $paswd \
	    -resource $resource
    } else {
	set sid [::xmpp::openStream $xmpp $srv]
	::utils::debug 4 "Logging in as ${uname}:${paswd},\
                          resource: $resource"
	::xmpp::auth::auth $xmpp \
	    -username $uname \
	    -password $paswd \
	    -resource $resource
    }
    
    if { $pcmd ne "" } {
	::xmpp::sendPresence $xmpp -priority -1 -status Online
    }

    return $xmpp
}


package provide biot::src::xmpp 0.1

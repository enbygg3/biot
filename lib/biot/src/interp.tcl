namespace eval ::biot::src::interp {
    namespace eval vars {
        variable interps    [dict create]
        variable -timeout    20;  # Timeouts for retries
    }
    namespace export {[a-z]*}
    [namespace parent]::register interp \
            -get [namespace current]::get \
            -defaults [namespace current]::defaults
}

# ::biot::src::interp::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::interp::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::src::interp::get -- Get data from interp
#
#       This procedure creates an interpreter from a script. When the interp is
#       first created, it can be initialised through the -request (which is then
#       understood as a tcl command to execute). Whenever data is to be got, the
#       procedure will first try to understand the -headers as a tcl command and
#       try to execute it. The return value is then sent for further analysis
#       for variable extraction. The procedure will also lookup variables that
#       are bound to the same source and marked with the type DELEGATED. In that
#       case, it will understand the field -extract of these variable as a
#       command to execute in this interpreter and set the variable to the
#       result of the command.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to interp main script
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       Creates a slave interpreter with FULL privileges!
proc ::biot::src::interp::get { s url hints } {
    global BDG
    
    upvar \#0 $s SRC
    
    # Create new interpreter for that source and source the initial content into
    # this interpreter.
    if { ![dict exists $vars::interps $s] } {
        set slave [Init $s $url $hints]
        if { $slave ne "" } {
            dict set vars::interps $s $slave
        }
    }
    
    set bspace [namespace parent [namespace parent]]
    if { [dict exists $vars::interps $s] } {
        set slave [dict get $vars::interps $s]
        
        # First try the obvious, i.e. we consider -headers to form a command and
        # call it. We pass its result to the regular variable extraction
        # mechanisms.
        set cmd [${bspace}::common::resolve $SRC(-headers)]
        if { [llength $cmd] } {
            if { [catch {$slave eval {*}$cmd} dta] == 0 } {
                $s activity $hints
                $s push [[namespace parent]::trimmer $dta $hints]
            } else {
                ::utils::debug error "Cannot get data through $cmd: $dta"
            }
        }
        
        # But now to something more subtle... We get all variables that are
        # associated to us, and for those that are marked of the type
        # "DELEGATED", consider the -extract as a command and call this to set
        # the variable of the variable.
        foreach v [${bspace}::vrbl::find $SRC(-name)] {
            upvar \#0 $v VAR
            if { $VAR(-type) eq "DELEGATED" } {
                set cmd [${bspace}::common::resolve $VAR(-extract)]
                if { [catch {$slave eval {*}$cmd} val] == 0 } {
                    $v setval $val
                } else {
                    ::utils::debug error "Could not compute value of $VAR(-name) through $cmd: $val"
                }
            }
        }
        
        if { $SRC(-freq) >= 0 } {
            set when [expr {int($SRC(-freq)*1000)}]
            set SRC(timer) [after $when [list $s connect]]
        }
    }
}



####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::src::interp::Init { s url hints } {
    upvar \#0 $s SRC
    
    # Arrange for fpath to contain the path to the main tcl script to source
    set i [string first "://" $url]
    if { $i >= 0 } {
        incr i 3
        set fpath [string range $url $i end]
    } else {
        set fpath $url;   ## Really??
    }
    
    # Create an interpreter with all powers, we are not interested in safe
    # interps here, since we want this to be the backend of anything that we
    # could support, including support for other protocols.
    set slave [interp create]
    
    # Initialise the slave interprer through first giving it a script, then, if
    # relevant calling the one-time initialisation procedure. The command to
    # call is taken from the -request of the source, which is in line with what
    # this is usually used for. Note that what is returned by the command is
    # understood as a boolean, a boolean that we use to destroy the interp if
    # initialisation decided so (through a negative boolean).
    if { [catch {$slave eval source $fpath} err] == 0 } {
        ::utils::debug notice "Initialised source interpreter with $fpath"
        if { $SRC(timer) ne "" } {
            catch {after cancel $SRC(timer)}
            set SRC(timer) ""
        }
        
        # Install biot command in slave interp, to enable variable operations
        # bound to the source.
        $slave alias biot [namespace parent]::biot $s
        
        # Initialise interpreter through calling first time procedure
        # (picked from the -request). Respect its return value by destroying
        # the interpreter and possibly restarting.
        if { $SRC(-request) ne "" } {
            set req [[namespace parent [namespace parent]]::common::resolve $SRC(-request)]
            if { [catch {$slave eval {*}$req} res] } {
                ::utils::debug error "Cannot initialise through $req: $res"
            } else {
                if { [string is false -strict $res] } {
                    ::utils::debug notice "Initialisating refused!"
                    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
                        # Restart in a short while
                        set SRC(timer) [after [expr int(${vars::-timeout}*1000)] \
                                [list $s connect]]
                    }
                    interp delete $slave
                }
            }
        }
        
        return $slave
    } else {
        ::utils::debug error "Cannot source initial content from $fpath: $err"
        if { [lsearch -nocase $hints "NORESTART"] < 0 } {
            # Restart in a short while
            set SRC(timer) [after [expr int(${vars::-timeout}*1000)] \
                    [list $s connect]]
        }
        interp delete $slave
    }
    
    return ""
}


package provide biot::src::interp 0.1


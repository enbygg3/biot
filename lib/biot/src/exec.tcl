namespace eval ::biot::src::exec {
    namespace eval vars {
	variable -flush 100;  # Variable flush in ms
    }
    namespace export {[a-z]*}
    [namespace parent]::register exec* \
	-get [namespace current]::get \
	-defaults [namespace current]::defaults
}


# ::biot::src::exec::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::exec::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::src::exec::lineread -- Line reader for sources
#
#       Reads the content from the source line by line and push this
#       data to the relevant variables.  Trimming is performed along
#       the way, if relevant.
#
# Arguments:
#	s	Identifier of the source
#	hints	URL hints, isolated at source creation time
#	reopen	Should we try reopening from end on EOF?
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::exec::lineread { s hints reopen } {
    upvar \#0 $s SRC
    if { $SRC(exec:fd) ne "" } {
	set status [catch {gets $SRC(exec:fd) line} result]
	if { $status != 0 } {
	    Got $s "Cannot get data: $status"
	} elseif { $result >= 0 } {
	    # We have received something from the file descriptor,
	    # push it for analysis if it isn't empty.
	    $s activity $hints
	    if { [string trim $line] ne "" } {
		$s push [[namespace parent]::trimmer $line $hints]
	    }
	} elseif { [eof $SRC(exec:fd)] } {
	    # If we should reopen, remember where we are in the file
	    # and schedule a reopening of the file in a little
	    # while. We use the flushing period, supposedly short and
	    # also inline with the kind of variable set-period that we
	    # are expecting for that application.
	    if { $reopen && [lsearch -nocase $hints "NORESTART"] < 0 } {
		::utils::debug 5 "EOF reached, reopening at end in\
                                  ${vars::-flush} ms"
		set SRC(exec:seek) [tell $SRC(exec:fd)]
		after ${vars::-flush} [list $s connect]
		Got $s
	    } else {
		Got $s "Reached end of file"
	    }
	} elseif { [fblocked $SRC(exec:fd)] } {
	    # Nothing, just wait
	} else {
	    Got $s "Unknown error"
	}
    }
}


# ::biot::src::exec::blockread -- Block reader for sources
#
#       Reads the content from the source in blocks and push this
#       whole content to the relevant variables.  Trimming is
#       performed along the way, if relevant.
#
# Arguments:
#	s	Identifier of the source
#	hints	URL hints, isolated at source creation time
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::exec::blockread { s hints } {
    upvar \#0 $s SRC
    if { $SRC(exec:fd) ne "" } {
	if { [eof $SRC(exec:fd)] } {
	    # Record activity and mark closed.  This is important,
	    # since we want to be able to turn off fileevents (for
	    # reading) on the file descriptor as quickly as possible.
	    $s activity $hints
	    set data [[namespace parent]::trimmer $SRC(exec:block) $hints]
	    set SRC(exec:block) ""
	    Got $s "Reached end of file"

	    # Push data for further analysis if it's not empty
	    if { [string trim $data] ne "" } {
		$s push $data
	    }
	} else {
	    append SRC(exec:block) [read $SRC(exec:fd)]
	}
    }
}


# ::biot::src::exec::get -- Execute external program to acquire data
#
#       This procedure starts an external program and read from its
#       standard output to get data to be analysed for variable
#       extraction.  The source frequency is used to detect how to
#       read from the standard output and how to (re)start the
#       program.  When the frequency is positive, the program will be
#       started with that frequency and its result will be the data
#       that is analysed.  When the frequency is negative, each line
#       send by the (continuously running) program will be passed for
#       analysis.
#
# Arguments:
#	s	Identifier of the source
#	pipe	Command pipe to read data from
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       Will run an external program, there is no control on which
#       program can or cannot be run.
proc ::biot::src::exec::get { s pipe hints } {
    upvar \#0 $s SRC

    set pipe [regsub "^exec://" $pipe ""]

    # exec:pid will contain the program identifiers of the program(s)
    # that are started as directed by the pipe.  Make sure we always
    # have something in that field.
    if { ![info exists SRC(exec:pid)] } {
	set SRC(exec:pid) ""
    }

    # If we had a program running and the external program should be
    # restarted each time (i.e. each the payload or url changes),
    # start by killing prior instance(s) of the program(s) in the
    # pipe.
    if { $SRC(exec:pid) ne "" && $SRC(-freq) >= 0 } {
	set kill [auto_execok kill]
	if { $kill ne "" } {
	    # Kill all instances and mark success by nullifying
	    # exec:pid to an empty list on success.
	    ::utils::debug 3 "Killing previous instance(s) at\
                              $SRC(exec:pid)" source
	    foreach pid $SRC(exec:pid) {
		if { [catch {exec kill $pid} err] } {
		    ::utils::debug 2 "Problems killing prior instance:\
                                      $err" source
		}
	    }
	    set SRC(exec:pid) ""
	} else {
	    ::utils::debug 2 "Cannot kill previous instance(s) at\
                              $SRC(exec:pid)" source
	}
    }

    # If no external program is running, start the program(s) that are
    # specified by the pipe and make sure that we will be able to push
    # data to the pipe.
    if { $SRC(exec:pid) eq "" } {
	# Close previous connection to external program, if any
	if { [info exists SRC(exec:fd)] && $SRC(exec:fd) ne "" } {
	    if { $SRC(-freq) < 0 } {
		catch {fileevent $SRC(exec:fd) readable ""}
	    }
	    catch {close $SRC(exec:fd)}
	}
	set SRC(exec:fd) ""

	# Most of the time, we wish to combine stderr and stdout, but
	# this can be turned off
	if { [lsearch -nocase $hints "NOCOMBINE"] < 0 } {
	    append pipe " 2>@1"
	}

	# Open the pipe, prepend the | sign and open the pipe for
	# reading since we will soon be trying to get data from the
	# stdout.
	if { [catch {open |$pipe r} fd] } {
	    ::utils::debug 2 "Could not start external program $pipe: $fd"
	} else {
	    # Success, remember the identifiers of the programs in the
	    # pipe and the file descriptor that really is the stdout of
	    # the last program in the pipe.
	    set SRC(exec:pid) [pid $fd]
	    set SRC(exec:fd) $fd
	    ::utils::debug 3 "Successfully started external program $pipe with\
                              process identifier(s) $SRC(exec:pid)"

	    if { $SRC(-freq) >= 0 } {
		set SRC(exec:block) ""
		fconfigure $SRC(exec:fd) -buffering full
		fileevent $SRC(exec:fd) readable \
		    [list ::biot::src::exec::blockread $s $hints]
	    } else {
		# Line buffering is forced here.
		fconfigure $SRC(exec:fd) -buffering line
		fileevent $SRC(exec:fd) readable \
		    [list ::biot::src::exec::lineread $s $hints 0]
	    }
	}
    }

    if { $SRC(-freq) >= 0 } {
	# Schedule a new get...
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [list $s connect]]
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::src::exec::Got -- EOF reached on file
#
#       Close file and unregister events before marking the file
#       descriptor context as empty.  This is used to clear state
#       before (re)opening the file/pipe.
#
# Arguments:
#	s	Identifier of the source
#	reason	Reason for the close operation
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::exec::Got { s { reason "" } } {
    upvar \#0 $s SRC
    if { $SRC(exec:fd) ne "" } {
	if { $reason ne "" } {
	    ::utils::debug 4 "Closing channel: $reason" source
	}
	fileevent $SRC(exec:fd) readable {}
	close $SRC(exec:fd)
	set SRC(exec:fd) ""
    }
}

package provide biot::src::exec 0.1

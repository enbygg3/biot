#package require biot::src::exec;   # They are combined!

namespace eval ::biot::src::file {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register file* \
	-get [namespace current]::get
}

# ::biot::src::file::get -- Read from external file to acquire data
#
#       This procedure reads from an external file to get data for
#       variable extraction.  The source frequency is used to detect
#       how to read from the file and how to (re)start reading.  When
#       the frequency is positive, the file is read in chunks with
#       that period and each chunk is analysed.  When the frequency is
#       negative, each line is read from the end of the file and sent
#       for analysis.  When the end of file is reached, we wait a
#       short while and try continuing from that point (think about
#       growing log files).  The hint "rewind" can be used to force
#       line-reading mode from the start of the file.  The hint
#       "norestart" can be used not to reopen at end of file.
#
# Arguments:
#	s	Identifier of source
#	fpath	Full path to file to read
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::file::get { s fpath hints } {
    upvar \#0 $s SRC
    
    set fpath [regsub "^file://" $fpath ""]
 
    # exec:fd will contain the file descriptor for the file.  Make
    # sure we always have something in that field.
    if { ![info exists SRC(exec:fd)] } {
	set SRC(exec:fd) ""
	set SRC(exec:seek) -1
    }

    # Open the file if there is no file opened yet.
    if { $SRC(exec:fd) eq "" } {
	if { [catch {open $fpath r} fd] } {
	    ::utils::debug 2 "Could not open $fpath! $fd"
	} else {
	    set SRC(exec:fd) $fd
	}
    }

    if { $SRC(-freq) >= 0 } {
	fconfigure $SRC(exec:fd) -buffering full
	fileevent $SRC(exec:fd) readable \
	    [list ::biot::src::exec::blockread $s $hints]
    } else {
	if { $SRC(exec:seek) >= 0 } {
	    if { $SRC(exec:seek) > [file size $fpath] } {
		# File probably truncated, zeroing seek position and
		# retry at once, which will take care of all possible
		# hints.
		set SRC(exec:seek) -1
		return [get $s $fpath $hints]
	    } else {
		seek $fd $SRC(exec:seek) start
	    }
	} elseif { [lsearch -glob -nocase $hints "r*w*d"] < 0 } {
	    # Jump to the end of file if we haven't specified rewind hint
	    seek $fd 0 end
	}

	# Read line by line as things are appended (by another
	# program?) to the file.
	fconfigure $SRC(exec:fd) -buffering line
	fileevent $SRC(exec:fd) readable \
	    [list ::biot::src::exec::lineread $s $hints 1]
    }


    if { $SRC(-freq) >= 0 } {
	# Schedule a new get...
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [list $s connect]]
    }
}

package provide biot::src::file 0.1

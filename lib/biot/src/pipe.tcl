package require biot::pipe

namespace eval ::biot::src::pipe {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register pipe* \
	-get [namespace current]::get
}

# ::biot::src::pipe::get -- Read from external file to acquire data
#
#       This procedure reads from an external file to get data for
#       variable extraction.  The source frequency is used to detect
#       how to read from the file and how to (re)start reading.  When
#       the frequency is positive, the file is read in chunks with
#       that period and each chunk is analysed.  When the frequency is
#       negative, each line is read from the end of the file and sent
#       for analysis.  When the end of file is reached, we wait a
#       short while and try continuing from that point (think about
#       growing log files).  The hint "rewind" can be used to force
#       line-reading mode from the start of the file.  The hint
#       "norestart" can be used not to reopen at end of file.
#
# Arguments:
#	s	Identifier of source
#	fpath	Full path to file to read
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::pipe::get { s fpath hints } {
    upvar \#0 $s SRC
    
    set fpath [regsub "^pipe://" $fpath ""]

    # exec:fd will contain the file descriptor for the file.  Make
    # sure we always have something in that field.
    if { ![info exists SRC(exec:fd)] } {
	set SRC(exec:fd) ""
	set SRC(exec:seek) -1
    }

    # Create name for pipe
    set name [string trimleft $fpath "/"]

    # Open the pipe if it does not yet exist.
    set pipe [::biot::pipe::channel $name]
    if { $pipe eq "" } {
        set pipe [::biot::pipe::new $name]
        if { $pipe eq "" } {
            ::utils::debug 2 "Could not create (internal) named pipe $name!"
        }
    }
    
    if { $pipe ne "" } {
        set SRC(exec:fd) $pipe
    }

    if { $SRC(-freq) >= 0 } {
	fconfigure $SRC(exec:fd) -buffering full
	fileevent $SRC(exec:fd) readable \
	    [list ::biot::src::exec::blockread $s $hints]
    } else {
	# Read line by line as things are appended 
	fconfigure $SRC(exec:fd) -buffering line
	fileevent $SRC(exec:fd) readable \
	    [list ::biot::src::exec::lineread $s $hints 1]
    }

    if { $SRC(-freq) >= 0 } {
	# Schedule a new get...
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [list $s connect]]
    }
}

package provide biot::src::pipe 0.1

package require tdbc

namespace eval ::biot::src::tdbc {
    namespace eval vars {
	variable -timeout    20;  # Timeouts for db calls, in seconds.
    }
    namespace export {[a-z]*}
    foreach {driver schemes} [list sqlite3 sqlite* postgres {postgres pgres pg} mysql mysql odbc odbc] {
        if { [catch {package require tdbc::$driver} ver] == 0 } {
            foreach scheme $schemes {
                [namespace parent]::register $scheme \
                    -get [namespace current]::get \
                    -defaults [namespace current]::defaults                
            }
            ::utils::debug DEBUG "DB support for $driver at version $ver"
        } else {
            ::utils::debug WARN "No support for $driver: $ver"
        }
    }
}

# ::biot::src::tdbc::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::tdbc::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::src::tdbc::get -- Open remote WebSocket to acquire data
#
#       This procedure opens a websocket connection to a remote
#       servera and arrange get data to be analysed for variable
#       extraction.  The initial request will be sent as soon as the
#       websocket connection is established, in order to, for example,
#       create a subscription or similar.  The frequency is only used
#       for knowing how long (or not at all) to wait when the
#       connection to the server could not be established.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to remote websocket server
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       Will keep a websocket connection open.
proc ::biot::src::tdbc::get { s url hints } {
    set cx [Connect $s $url $hints]
    if { $cx ne "" } {
        upvar \#0 $s SRC

        # Prepare TDBC statement
        set stmt [$cx prepare $SRC(-request)]
        set rset [$stmt execute]
        set data [$rset allrows -as dicts]
        $s activity $hints
        $s push [[namespace parent]::trimmer $data $hints]
        $rset close
        $stmt close
        
	if { $SRC(-freq) ne "" && $SRC(-freq) >= 0 } {
	    set when [expr {int($SRC(-freq)*1000)}]
	    set SRC(timer) [after $when [list $s connect]]
	}
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::src::tdbc::Connect { s url hints } {
    upvar \#0 $s SRC
    
    if { ![info exists SRC(tdbc:cx)] || $SRC(tdbc:cx) eq "" } {
        # Capture options from the headers specification in the source.
        set opts [dict create -timeout [expr {${vars::-timeout}*1000}]]
        foreach {k v} $SRC(-headers) {
            dict set opts -[string trimleft $k -] $v
        }
        
        # Capture everything after the :// as the connection specification, this
        # will be driver dependent.
        set idx [string first "://" $url]
        incr idx 3
        set cspec [string range $url $idx end]
        
        switch -glob -- $url {
            "odbc://*" -
            "sqlite*://*" {
                if { [catch {::tdbc::sqlite3::connection new $cspec {*}$opts} cx] == 0 } {
                    # Connection made!
                    set SRC(tdbc:cx) $cx
                    if { $SRC(timer) ne "" } {
                        catch {after cancel $SRC(timer)}
                        set SRC(timer) ""
                    }
                    return $SRC(tdbc:cx)
                } else {
                    ::utils::debug 1 "Could not open DB connection to $cspec: $cx"
                    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
                        # Restart in a short while
                        set SRC(timer) [after [expr int(${vars::-timeout}*1000)] \
                                            [list $s connect]]
                    }
                }
            }
            "pg*://*" -
            "postgres://*" -
            "mysql*://*" {
                # Extract user and password from URL, move this into opts
                lassign [split [::biot::uri::auth $url curl] :] user passwd
                if { [info exists user] && $user ne "" } {
                    dict set opts -user $user
                }
                if { [info exists passwd] } {
                    dict set opts -password $passwd
                }
                
                # Isolate start of hostport and start of db name from URL
                set idx [string first "://" $curl]
                incr idx 3
                set slash [string first "/" $curl]
                
                # Extract DB and locate host/port specification
                set db ""
                if { $slash > 0 } {
                    set db [string range $curl [expr {$slash+1}] end]
                    set hostport [string range $curl $idx [expr {$slash-1}]]
                } else {
                    set hostport [string range $curl $idx end]
                }
                
                # Extract host and port.
                lassign [split $hostport :] hostname port
                if { [info exists hostname] && $hostname ne "" } {
                    dict set opts -host $hostname
                }
                if { [info exists port] && $port ne "" } {
                    dict set opts -port $port
                }
                
                if { [string match "mysql*" $url] } {
                    set res [catch {::tdbc::mysql::connection new {*}$opts} cx]
                } else {
                    set res [catch {::tdbc::postgres::connection new {*}$opts} cx]
                }
                if { $res == 0 } {
                    # Connection made!
                    set SRC(tdbc:cx) $cx
                    if { $SRC(timer) ne "" } {
                        catch {after cancel $SRC(timer)}
                        set SRC(timer) ""
                    }
                    return $SRC(tdbc:cx)
                } else {
                    ::utils::debug 1 "Could not open DB connection with $opts: $cx"
                    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
                        # Restart in a short while
                        set SRC(timer) [after [expr int(${vars::-timeout}*1000)] \
                                            [list $s connect]]
                    }
                }
            }
        }
    } else {
        return $SRC(tdbc:cx)
    }
    
    return ""
}


package provide biot::src::tdbc 0.1

package require websocket 1.4

namespace eval ::biot::src::ws {
    namespace eval vars {
	variable -timeout    20;  # Timeouts for HTTP calls, in seconds.
    }
    namespace export {[a-z]*}
    [namespace parent]::register ws* \
	-get [namespace current]::get \
	-resolver [list wss [list ssl tls]] \
	-defaults [namespace current]::defaults
}

# ::biot::src::ws::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::ws::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::src::ws::get -- Open remote WebSocket to acquire data
#
#       This procedure opens a websocket connection to a remote
#       servera and arrange get data to be analysed for variable
#       extraction.  The initial request will be sent as soon as the
#       websocket connection is established, in order to, for example,
#       create a subscription or similar.  The frequency is only used
#       for knowing how long (or not at all) to wait when the
#       connection to the server could not be established.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to remote websocket server
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       Will keep a websocket connection open.
proc ::biot::src::ws::get { s url hints } {
    global BDG

    upvar \#0 $s SRC

    # Now initiate the construction of a WebSocket open operation with
    # the proper headers, i.e. headers as specified at the
    # destination, but also authentication information if necessary.
    set auth [::biot::uri::auth $url curl]
    set hdrs $SRC(-headers)
    if { $auth ne "" } {
	lappend hdrs "Authorization" "Basic [::base64::encode $auth]"
    }
    set cmd [list ::websocket::open $curl \
		 [list [namespace current]::Handler $s $hints] \
		 -headers $hdrs]

    # Add global timeout information, if necessary
    if { $BDG(-timeout) > 0 } {
	lappend cmd -timeout [expr int(${vars::-timeout}*1000)]
    }

    if { [catch {eval $cmd} err] } {
	::utils::debug 1 "Could not open websocket at $curl for $SRC(-name):\
                          $err"
	if { [lsearch -nocase $hints "NORESTART"] < 0 } {
	    # Restart in a short while
	    set SRC(timer) [after [expr int(${vars::-timeout}*1000)] \
				[list $s connect]]
	}
    } else {
	if { $SRC(timer) ne "" } {
	    catch {after cancel $SRC(timer)}
	    set SRC(timer) ""
	}
	::utils::debug 4 "Getting source data from websocket at $curl"
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


proc ::biot::src::ws::Poll { s hints sock } {
    upvar \#0 $s SRC
    if { [lsearch -nocase $hints "BINARY"] >= 0 } {
	set mode "binary"
    } else {
	set mode "binary"
    }
    ::websocket::send $sock $mode $SRC(-request)

    if { $SRC(-freq) ne "" && $SRC(-freq) >= 0 } {
	set when [expr {int($SRC(-freq)*1000)}]
	set SRC(timer) [after $when [namespace current]::Poll $s $hints $sock]
    }
}


proc ::biot::src::ws::Handler { s hints sock type msg } {
    global BDG

    upvar \#0 $s SRC

    switch -- $type {
	"connect" {
	    # If we had a request for that source, treat that as an
	    # initial message that we will send to the remote server
	    # onto the websocket.  Default is to send this as a text
	    # message, but binary is also available if necessary.
	    if { $SRC(-request) ne "" } {
		::utils::debug 4 "WS connected, scheduling initial request"
		set SRC(timer) \
		    [after idle [list [namespace current]::Poll $s $hints $sock]]
	    }
	}
	"binary" -
	"text" {
	    # Anything that has been received from the server is
	    # pushed further for analysis.
	    $s activity $hints
	    $s push [[namespace parent]::trimmer $msg $hints]
	}
	"error" -
	"disconnect" {
	    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
		::utils::debug 3 "Websocket connection lost, reopening"
		set SRC(timer) [after idle [list $s connect]]
	    }
	}
	default {
	    ::utils::debug 5 "Received WS message '$type': $msg"
	}
    }
}


package provide biot::src::ws 0.1

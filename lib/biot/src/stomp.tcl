package require biot::uri
package require stomp::client

namespace eval ::biot::src::stomp {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register stomp \
	-get [namespace current]::get
}

# ::biot::src::stomp::get -- Arrange to get from STOMP server
#
#       Connect to the stomp server and port that are part of the URL,
#       and subscribe to the topic which is the path of the URL.
#       Arrange to push all data received for variable analysis.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to get STOMP info from.
#	hints	Hints (from the original scheme).
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::stomp::get { s url hints } {
    upvar \#0 $s SRC
    
    set auth [::biot::uri::auth $url curl]
    set usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break

    set surl [regsub "^stomp://" $curl ""]
    set slash [string first "/" $surl]
    set topic [string range $surl $slash end]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    # Connect, be smart about the port so we use the default port
    # specified as part of the STOMP library.
    if { $prt eq "" } {
	::utils::debug INFO "Connecting to STOMP server ${hst}"
	set SRC(stomp:cx) [::stomp::client::connect \
			       -host $hst \
			       -user $usr \
			       -password $pasw]
    } else {
	::utils::debug INFO "Connecting to STOMP server ${hst}:${prt}"
	set SRC(stomp:cx) [::stomp::client::connect \
			       -host $hst \
			       -port $prt \
			       -user $usr \
			       -password $pasw]
    }
    
    # Arrange to call Connected once connected.
    ::stomp::client::handler $SRC(stomp:cx) \
	[list [namespace current]::Connected $s $topic $hints] CONNECTED 
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################



# ::biot::src::stomp::Receiver -- Data reception
#
#       Receive data from the STOMP server and push it for variable
#       extraction.
#
# Arguments:
#	s	Identifier of the source
#	hints	Hints (from the original scheme).
#	msg	STOMP message
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::stomp::Receiver { s hints msg } {
    upvar \#0 $s SRC

    ::utils::debug 5 "Received STOMP message of type\
                      [::stomp::message::getHeader $msg content-type]"
    set dta [::stomp::message::getBody $msg]
    $s activity $hints;   # Mark activity
    $s push [[namespace parent]::trimmer $dta $hints]
}


# ::biot::src::stomp::Connected -- Subscription upon connection
#
#       Subscribe to STOMP topic once connected to server.
#
# Arguments:
#	s	Identifier of the source
#	topic	Topic to subscribe to
#	hints	Hints (from the original scheme).
#	msg	STOMP message, ignored.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::stomp::Connected { s topic hints msg } {
    upvar \#0 $s SRC
    
    ::utils::debug 4 "Listening to STOMP topic $topic"
    ::stomp::client::subscribe $SRC(stomp:cx) $topic \
	-handler [list [namespace current]::Receiver $s $hints] 
}


package provide biot::src::stomp 0.1

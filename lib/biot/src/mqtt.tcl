package require biot::uri
package require mqtt

namespace eval ::biot::src::mqtt {
    namespace eval vars {
        variable -keepalive   60
        variable -name        "%hostname%-%pid%-%prgname%"
        variable -clean       on
    }
    namespace export {[a-z]*}
    [namespace parent]::register mqtt* \
        -get [namespace current]::get \
        -resolver [list mqtts [list ssl tls]] \
        -defaults [namespace current]::defaults

}


# ::biot::src::mqtt::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::mqtt::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}



# ::biot::src::mqtt::get -- Arrange to get from MQTT server
#
#       Connect to the mqtt server and port that are part of the URL,
#       and subscribe to the topic which is the path of the URL.
#       Arrange to push all data received for variable analysis.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to get MQTT info from.
#	hints	Hints (from the original scheme).
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::mqtt::get { s url hints } {
    upvar \#0 $s SRC
    
    set auth [::biot::uri::auth $url curl]
    set usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break

    if { [string match "mqtts://*" $url] } {
        set cmd [::toclbox::network::tls_socket]
    } else {
        set cmd [list socket]
    }

    set surl [regsub "^mqtt(s|)://" $curl ""]
    set slash [string first "/" $surl]
    set topic [string range $surl [expr {$slash+1}] end]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    # Connect, be smart about the port so we use the default port
    # specified as part of the MQTT library.
    set SRC(mqtt:cx) [mqtt new \
                        -username $usr \
                        -password $pasw \
                        -socketcmd $cmd \
                        -keepalive ${vars::-keepalive} \
                        -clean ${vars::-clean}]
    set cname [::utils::resolve ${vars::-name} \
                        [list   hostname [info hostname] \
                                pid [pid]]]
    $SRC(mqtt:cx) subscribe \$LOCAL/connection [list [namespace current]::Liveness $s]
    $SRC(mqtt:cx) subscribe \$LOCAL/subscription [list [namespace current]::Liveness $s]
    set cname [string range $cname 0 22]
    if { $prt eq "" } {
        ::utils::debug INFO "Connecting to MQTT server ${hst}"
        $SRC(mqtt:cx) connect $cname $hst
    } else {
        ::utils::debug INFO "Connecting to MQTT server ${hst}:${prt}"
        $SRC(mqtt:cx) connect $cname $hst $prt
    }
    
    # Arrange to call Connected once connected.
    ::utils::debug INFO "Subscribing to MQTT topic $topic"
    $SRC(mqtt:cx) subscribe $topic [list [namespace current]::Receiver $s $hints]
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::src::mqtt::Receiver -- Data reception
#
#       Receive data from the MQTT server and push it for variable
#       extraction.
#
# Arguments:
#	s	Identifier of the source
#	hints	Hints (from the original scheme).
#	msg	MQTT message
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::mqtt::Receiver { s hints topic dta } {
    upvar \#0 $s SRC

    ::utils::debug 5 "Received MQTT message on $topic"
    $s activity $hints;   # Mark activity
    $s push [[namespace parent]::trimmer $dta $hints] -where $topic
}

proc ::biot::src::mqtt::Liveness { s topic dta } {
    upvar \#0 $s SRC

    switch -glob -- $topic {
        "*/connection" {
            switch -- [dict get $dta state] {
                "connected" {
                    ::utils::debug NOTICE "Connected to broker"
                }
                "disconnected" {
                    array set reasons {
                        0 "Normal disconnect"
                        1 "Unacceptable protocol version"
                        2 "Identifier rejected"
                        3 "Server unavailable"
                        4 "Bad user name or password"
                        5 "Not authorized"
                    }
                    ::utils::debug WARN "Disconnected from broker $reasons([dict get $dta reason])"
                }
            }
        }
        "*/subscription" {
            foreach {topic qos} $dta {
                switch -- $qos {
                    "" {
                        ::utils::debug INFO "Unsubscribed from topic at $topic"
                    }
                    "0x80" {
                        ::utils::debug INFO "Could not subscribe to topic $topic"
                    }
                    default {
                        ::utils::debug INFO "Subscribed to topic $topic, QoS: $qos"
                    }
                }
            }
        }
    }
}


package provide biot::src::mqtt 0.1

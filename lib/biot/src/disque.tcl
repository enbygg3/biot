package require biot::uri
package require disque

namespace eval ::biot::src::disque {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register disque \
        -get [namespace current]::get \
        -defaults [namespace current]::defaults

}


# ::biot::src::disque::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::disque::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}



# ::biot::src::disque::get -- Arrange to get from MQTT server
#
#       Connect to the Disque server and port that are part of the URL, and try
#       getting a job from  the queue which is the path of the URL. Arrange to
#       push all data received for variable analysis.
#
# Arguments:
#	s	Identifier of the source
#	url	URL to get disque info from.
#	hints	Hints (from the original scheme).
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::disque::get { s url hints } {
    upvar \#0 $s SRC
    
    set auth [::biot::uri::auth $url curl]
    set usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break

    set surl [regsub "^disque://" $curl ""]
    set slash [string first "/" $surl]
    set queue [string range $surl [expr {$slash+1}] end]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    if { ![info exists SRC(disque:cx)] || $SRC(disque:cx) eq "" } {
        # Connect, be smart about the port so we use the default port
        # specified as part of the Disque library.
        set cmd [list disque \
                        -nodes $hst \
                        -auth $pasw \
                        -liveness [list [namespace current]::Liveness $s]]
        if { $prt ne "" } {
            lappend cmd -port $prt
        }
        set SRC(disque:cx) [eval $cmd]
    }

    # Poll for one job
    ::utils::debug DEBUG "Polling for a job at $queue"
    set poll [$SRC(disque:cx) getjob -nohang -count 1 $queue]
    set jobspec [lindex $poll 0];  # Extract one job from list
    lassign $jobspec q id body
    if { $body ne "" } {
        ::utils::debug INFO "Pulled one job from $queue"
        $s activity $hints
        $s push [[namespace parent]::trimmer $body $hints]
        $SRC(disque:cx) ackjob $id
    }

    if { $SRC(-freq) >= 0 } {
        # Schedule a new get...
        set when [expr {int($SRC(-freq)*1000)}]
        set SRC(timer) [after $when [list $s connect]]
    }            
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::src::disque::Liveness { s d state args } {
    upvar \#0 $s SRC
    ::utils::debug DEBUG "Disque connection status is $state"
}


package provide biot::src::disque 0.1

package require docker
package require toclbox::url

namespace eval ::biot::src::docker {
    namespace eval vars {
        variable -socat    "socat"
        variable -nc       "nc"
        variable -default  "/var/run/docker.sock"
    }
    namespace eval containers {};   # Will host info for each container
    namespace export {[a-z]*}
    [namespace parent]::register docker* \
	-get [namespace current]::get \
        -defaults [namespace current]::defaults
}

# ::biot::src::docker::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::docker::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


# ::biot::src::docker::get -- Read from docker container
#
#       This procedure reads from an external file to get data for
#       variable extraction.  The source frequency is used to detect
#       how to read from the file and how to (re)start reading.  When
#       the frequency is positive, the file is read in chunks with
#       that period and each chunk is analysed.  When the frequency is
#       negative, each line is read from the end of the file and sent
#       for analysis.  When the end of file is reached, we wait a
#       short while and try continuing from that point (think about
#       growing log files).  The hint "rewind" can be used to force
#       line-reading mode from the start of the file.  The hint
#       "norestart" can be used not to reopen at end of file.
#
# Arguments:
#	s	Identifier of source
#	fpath	Full path to docker container
#	hints	Hints as extracted from the source's URL scheme.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::docker::get { s fpath hints } {
    upvar \#0 $s SRC
    if { ![info exists SRC(docker:daemon)] } {
        Init $s $fpath $hints
        Attach $s $hints
    }
}



proc ::biot::src::docker::Attach { s hints } {
    upvar \#0 $s SRC

    set cx [docker connect $SRC(docker:daemon) {*}$SRC(docker:opts)]
    foreach c [$cx containers] {
        set id [dict get $c Id]
        foreach name [dict get $c Names] {
            set slash [string last "/" $name]
            if { $slash >= 0 } {
                set name [string range $name [incr slash] end]
            }
            
            if { [string match $SRC(docker:pattern) $name] } {
                if { ![info exists [namespace current]::containers::$id] } {
                    set sid [string range $id 0 11]
                    ::utils::debug NOTICE "Attaching to container $name: $sid"
                    $cx attach $sid \
                        [list [namespace current]::Forward $s $id $name $hints] \
                        stream 1 stdout 1
                    set [namespace current]::containers::$id $cx
                    # Now we have highjacked the context for that very
                    # container, so connect again to see if we can find more
                    # matching containers. 
                    after idle [list [namespace current]::Attach $s $hints]
                    return
                }
            }
        }
    }
    
    # When we are here we have a connection to the daemon, but no existing
    # container that we should attach to
    $cx disconnect
    if { $SRC(-freq) ne "" && $SRC(-freq) >= 0 } {
        set when [expr {int($SRC(-freq)*1000)}]
        set SRC(timer) [after $when [list [namespace current]::Attach $s $hints]]
    }
}

proc ::biot::src::docker::Forward { s id name hints type line } {
    upvar \#0 $s SRC
    
    if { $type eq "error" } {
        ::utils::debug NOTICE "Connection to $name lost"
        unset [namespace current]::containers::$id
        return
    }
    
    ::utils::debug INFO "Forwarding from $name: $line"
    if { $line ne "" } {
        $s activity $hints
        $s push [[namespace parent]::trimmer $line $hints]
    }
}


proc ::biot::src::docker::Init { s url hints } {
    upvar \#0 $s SRC
    
    # Propagate the relevant default options.
    set SRC(docker:opts) [dict create]
    foreach k [list -socat -nc] {
        dict set SRC(docker:opts) $k [set vars::$k]
    }
    # Capture options from the headers specification in the source.
    foreach {k v} $SRC(-headers) {
        dict set SRC(docker:opts) -[string trimleft $k -] $v
    }
    
    # Capture everything after the :// as the connection specification, this
    # will contain the location of the daemon and the container(s) to listen
    # to.
    set idx [string first "://" $url]
    incr idx 3
    set cspec [string range $url $idx end]
    
    # No slash means use the docker daemon at the UNIX socket default,
    # otherwise we have a location (before the slash) and a container
    # name/pattern
    set slash [string first "/" $cspec]
    if { $slash >= 0 } {
        set loc [string range $cspec 0 [expr {$slash - 1}]]
        if { [string trim $loc] eq "" } { set loc ${vars::-default} }
        set SRC(docker:pattern) [string range $cspec [expr {$slash + 1}]]
    } else {
        set loc ${vars::-default}
        set SRC(docker:pattern) $cspec
    }
        
    # Reconstruct a proper URL for connection to the local/remote docker
    # daemon.
    if { [string first "%" $loc] >= 0 } {
        # If it look likes we have a URL encoding in there, URL decode the
        # whole location.
        set loc [::toclbox::url::decode $loc]
    }
    # If we have a slash, we probably are talking to the local daemon at
    # that very path, otherwise, this will be a remote host.
    if { [string first "/" $loc] >= 0 } {
        set SRC(docker:daemon) unix://$loc
    } elseif { [lsearch $hints "tls"] >= 0 } {
        set SRC(docker:daemon) https://$loc
    } else {
        set SRC(docker:daemon) http://$loc
    }
    ::utils::debug INFO "Will talk to Docker daemon at $SRC(docker:daemon),\
                         looking for containers matching $SRC(docker:pattern)"
}


package provide biot::src::docker 0.1

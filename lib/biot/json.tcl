package require utils

namespace eval ::biot::json {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    namespace ensemble create -command ::json
}

proc ::biot::json::select { dta xpr { separator "/" } {lead ""} } {
    set selection {}

    if { [Object? $dta] } {
	foreach { k v } $dta {
	    set fv $lead$separator$k
	    set selection [concat $selection [select $v $xpr $separator $fv]]
	    if { [string match $xpr $fv] } {
		set selection [concat [list $fv $v] $selection]
	    }
	}
    }

    if { [llength $selection] == 0 } {
	set len [llength $dta]
	if { $len > 1 } {
	    for {set i 0} {$i < $len} {incr i} {
		set fv $lead\($i\)
		set v [lindex $dta $i]
		set selection [concat $selection [select $v $xpr $separator $fv]]
		if { [string match $xpr $fv] } {
		    set selection [concat [list $fv $v] $selection]
		}
	    }
	}
    }
    return $selection
}

proc ::biot::json::match { dta xpr op val } {
    set selection {}
    foreach {k v} [select $dta $xpr] {
	if { [Test $v $op $val] } {
	    lappend selection $k
	}
    }
    return $selection
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


proc ::biot::json::ListOf? { dta class } {
    foreach i $dta {
	if { ![string is $class -strict $i] } {
	    return 0
	}
    }
    return 1
}


proc ::biot::json::Object? { dta } {
    if { [llength $dta]%2 == 0 } {
	if { [ListOf? $dta integer] || [ListOf? $dta double] } {
	    return 0
	}

	foreach {k v} $dta {
	    if { ![string is wordchar $k] } {
		return 0
	    }
	}
	return 1
    }
    return 0
}

proc ::biot::json::Test { v op val } {
    switch -glob -- $op {
	"eq*" {
	    return [string equal $v $val]
	}
	"noneq*" -
	"ne" {
	    return [expr {![string equal $v $val]}]
	}
	"mtc*" -
	"ma*" {
	    return [string match $val $v]
	}
	"rx" -
	"r*" {
	    if { [catch {regexp $val $v} res] == 0 } {
		return $res
	    } else {
		::utils::debug 2 "Could not evaluate matching expression: $res"
	    }
	}
	default {
	    if { [catch {expr \"$v\" $op \"$val\"} res] == 0 } {
		return $res
	    } else {
		::utils::debug 2 "Could not evaluate matching expression: $res"
	    }
	}
    }
    return 0;   # Catch all for failures
}


package provide biot::json 0.1

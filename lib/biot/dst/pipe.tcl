package require utils
package require biot::uri
package require biot::pipe

namespace eval ::biot::dst::pipe {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register pipe \
	-push [namespace current]::push
}


# ::biot::dst::file::push -- Push data to local file
#
#       This procedure send the data coming from the payload to a
#       local file.  Hints (coming from the initial scheme) are used
#       to append to or replace any existing file, and to
#       automatically make (or not) any intermediary directory
#       specified by the file path.
#
# Arguments:
#	d	Identifier of the destination
#	fpath	Path to file
#	payload	Data to append/replace file content with
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Appends to or replace content of file.
proc ::biot::dst::pipe::push { d fpath payload hints headers  } {
    upvar \#0 $d DST

    if { [string first "pipe://" $fpath] >= 0 } {
	set fpath [string range $fpath [string length "pipe://"] end]
    }

    # Decide name out of path to file
    set name [string trimleft $fpath "/"]
    
    # Open the pipe if it does not yet exist.
    set pipe [::biot::pipe::channel $name]
    if { $pipe eq "" } {
        set pipe [::biot::pipe::new $name]
        if { $pipe eq "" } {
            ::utils::debug 2 "Could not open (internal) named path $name!"
            $d memorise
        }
    }
    
    if { $pipe ne "" } {
        puts -nonewline $pipe $payload
        ::utils::debug 3 "Successfully triggered $DST(-name) (to $fpath)"
    }
}

package provide biot::dst::pipe 0.1

package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::interp {
    namespace eval vars {
        variable interps    [dict create]
    }
    namespace export {[a-z]*}
    [namespace parent]::register interp \
            -push [namespace current]::push \
            -defaults [namespace current]::defaults
}

proc ::biot::dst::interp::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::dst::interp::push -- Push data to remote HTTP server
#
#       This procedure will initiate an HTTP operation to push the
#       payload to the remote URL using HTTP.  Hints (coming from the
#       initial scheme) are used to know what kind of http operation
#       to perform and if it should be encrypted or not.
#
# Arguments:
#	d	Identifier of the destination
#	url	Valid URL to send data to.
#	payload	Data to send (can be empty)
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Perform an HTTP operation to remote site.
proc ::biot::dst::interp::push { d url payload hints headers } {
    upvar \#0 $d DST
    
    # Create new interpreter for that destination and source the initial content into
    # this interpreter.
    if { ![dict exists $vars::interps $d] } {
        set slave [Init $d $url $hints]
        if { $slave ne "" } {
            dict set vars::interps $d $slave
        }
    }
    
    set bspace [namespace parent [namespace parent]]
    if { [dict exists $vars::interps $d] } {
        set slave [dict get $vars::interps $d]
        
        # First try the obvious, i.e. we consider -headers to form a command and
        # call it. We pass its result to the regular variable extraction
        # mechanisms.
        set cmd [${bspace}::common::resolve $headers]
        if { [llength $cmd] } {
            if { [catch {$slave eval [linsert $cmd end $payload]} res] == 0 } {
                ::utils::debug 5 "Triggered remote destination with changes"
            } else {
                ::utils::debug ERROR "Cannot send through $cmd: $res"
            }
        }
    }
}



####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################



proc ::biot::dst::interp::Init { d url hints } {
    upvar \#0 $d DST
    
    # Arrange for fpath to contain the path to the main tcl script to source
    set i [string first "://" $url]
    if { $i >= 0 } {
        incr i 3
        set fpath [string range $url $i end]
    } else {
        set fpath $url;   ## Really??
    }
    
    # Create an interpreter with all powers, we are not interested in safe
    # interps here, since we want this to be the backend of anything that we
    # could support, including support for other protocols.
    set slave [interp create]
    
    # Initialise the slave interprer through first giving it a script, then, if
    # relevant calling the one-time initialisation procedure. The command to
    # call is taken from the -request of the source, which is in line with what
    # this is usually used for. Note that what is returned by the command is
    # understood as a boolean, a boolean that we use to destroy the interp if
    # initialisation decided so (through a negative boolean).
    if { [catch {$slave eval source $fpath} err] == 0 } {
        ::utils::debug notice "Initialised destination interpreter with $fpath"
        
        # Install biot command in slave interp, to enable variable operations
        # bound to the source.
        $slave alias biot [namespace parent]::biot $d
        
        return $slave
    } else {
        ::utils::debug error "Cannot source initial content from $fpath: $err"
        interp delete $slave
    }
    
    return ""
}


package provide biot::dst::interp 0.1

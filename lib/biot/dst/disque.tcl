package require disque
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::disque {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register disque \
        -push [namespace current]::push \
        -connect [namespace current]::connect \
        -defaults [namespace current]::defaults
}


# ::biot::src::disque::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::dst::disque::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


proc ::biot::dst::disque::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    set hstpath [regsub "^disque://" $hstpath ""]
    if { $payload ne "" } {
        if { $DST(_disque) ne "" } {
            set slash [string first "/" $hstpath]
            set queue [string range $hstpath [expr {$slash+1}] end]
            $DST(_disque) addjob -async $queue $payload
            ::utils::debug 5 "Successfully added job to queue $queue"
        } else {
            ::utils::debug 3 "No disque connection yet,\
                                payload will be lost!"
        }
    }
}


proc ::biot::dst::disque::connect { d uri hints headers } {
    upvar \#0 $d DST
    set DST(_disque) ""

    set auth [::biot::uri::auth $uri curl]
    set  usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break
    
    ::utils::debug 4 "Connecting to remote disque server at $curl"

    set surl [regsub "^disque://" $curl ""]
    set slash [string first "/" $surl]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    set cmd [list disque \
                    -nodes $hst \
                    -auth $pasw \
                    -liveness [list [namespace current]::Liveness $d]]
    if { $prt ne "" } {
        lappend cmd -port $prt
    }
    set DST(_disque) [eval $cmd]
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::dst::disque::Liveness { d c state args } {
    ::utils::debug DEBUG "Disque connection status is $state"
}


package provide ::biot::dst::disque 0.1

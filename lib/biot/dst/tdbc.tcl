package require tdbc
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::tdbc {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    foreach {driver schemes} [list sqlite3 sqlite* postgres {postgres pgres pg} mysql mysql odbc odbc] {
        if { [catch {package require tdbc::$driver} ver] == 0 } {
            foreach scheme $schemes {
                [namespace parent]::register $scheme \
                    -push [namespace current]::push \
                    -connect [namespace current]::connect                
            }
            ::utils::debug DEBUG "DB support for $driver at version $ver"
        } else {
            ::utils::debug WARN "No support for $driver: $ver"
        }
    }
}

proc ::biot::dst::tdbc::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    if { $payload ne "" } {
	if { $DST(tdbc:cx) ne "" } {
            set stmt [$DST(tdbc:cx) prepare $payload]
            set rset [$stmt execute]
            $rset close
            $stmt close
	} else {
	    ::utils::debug 3 "No DB connection yet (?),\
                              payload will be lost!"
	}
    }
}


proc ::biot::dst::tdbc::connect { d uri hints headers } {
    upvar \#0 $d DST

    if { ![info exists DST(tdbc:cx)] } {
        # Capture options from the headers specification in the source.
        set opts [dict create]
        foreach {k v} $headers {
            dict set opts -[string trimleft $k -] $v
        }
        
        # Capture everything after the :// as the connection specification, this
        # will be driver dependent.
        set idx [string first "://" $uri]
        incr idx 3
        set cspec [string range $uri $idx end]
        
        switch -glob -- $uri {
            "odbc://*" -
            "sqlite*://*" {
                if { [catch {::tdbc::sqlite3::connection new $cspec {*}$opts} cx] == 0 } {
                    # Connection made!
                    set DST(tdbc:cx) $cx
                } else {
                    ::utils::debug 1 "Could not open DB connection to $cspec: $cx"
                    # Reopen if proper hints
                    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
                        $d reconnect
                    }
                }
            }
            "pg*://*" -
            "postgres://*" -
            "mysql*://*" {
                # Extract user and password from URL, move this into opts
                lassign [split [::biot::uri::auth $uri curl] :] user passwd
                if { [info exists user] && $user ne "" } {
                    dict set opts -user $user
                }
                if { [info exists passwd] } {
                    dict set opts -password $passwd
                }
                
                # Isolate start of hostport and start of db name from URL
                set idx [string first "://" $curl]
                incr idx 3
                set slash [string first "/" $curl]
                
                # Extract DB and locate host/port specification
                set db ""
                if { $slash > 0 } {
                    set db [string range $curl [expr {$slash+1}] end]
                    set hostport [string range $curl $idx [expr {$slash-1}]]
                } else {
                    set hostport [string range $curl $idx end]
                }
                
                # Extract host and port.
                lassign [split $hostport :] hostname port
                if { [info exists hostname] && $hostname ne "" } {
                    dict set opts -host $hostname
                }
                if { [info exists port] && $port ne "" } {
                    dict set opts -port $port
                }
                
                if { [string match "mysql*" $uri] } {
                    set res [catch {::tdbc::mysql::connection new {*}$opts} cx]
                } else {
                    set res [catch {::tdbc::postgres::connection new {*}$opts} cx]
                }
                if { $res == 0 } {
                    # Connection made!
                    set DST(tdbc:cx) $cx
                } else {
                    ::utils::debug 1 "Could not open DB connection with $opts: $cx"
                    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
                        $d reconnect
                    }
                }
            }
        }
    }
}

package provide ::biot::dst::tdbc 0.1
package require http
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::http {
    namespace eval vars {
        variable -timeout    20;  # Timeouts for HTTP calls, in seconds.
    }
    namespace export {[a-z]*}
    [namespace parent]::register http* \
            -push [namespace current]::push \
            -resolver [list https [list ssl tls]] \
            -defaults [namespace current]::defaults
}

proc ::biot::dst::http::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::dst::http::push -- Push data to remote HTTP server
#
#       This procedure will initiate an HTTP operation to push the
#       payload to the remote URL using HTTP.  Hints (coming from the
#       initial scheme) are used to know what kind of http operation
#       to perform and if it should be encrypted or not.
#
# Arguments:
#	d	Identifier of the destination
#	url	Valid URL to send data to.
#	payload	Data to send (can be empty)
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Perform an HTTP operation to remote site.
proc ::biot::dst::http::push { d url payload hints headers } {
    upvar \#0 $d DST
    
    # Now initiate the construction of an HTTP operation with the
    # proper headers, i.e. headers as specified at the destination,
    # but also authentication information if necessary.
    set auth [::biot::uri::auth $url curl]
    if { $auth ne "" } {
        lappend headers "Authorization" "Basic [::base64::encode $auth]"
    }
    set cmd [list ::http::geturl $curl -headers $headers]
    
    # Add global timeout information, if necessary
    if { ${vars::-timeout} > 0 } {
        lappend cmd -timeout [expr {int(${vars::-timeout}*1000)}]
    }
    
    # Specify HTTP method, if specified as part of the hints.
    if { [lsearch -nocase $hints "PUT"] >= 0 } {
        lappend cmd -method PUT
    } elseif { [lsearch -nocase $hints "DELETE"] >= 0 } {
        lappend cmd -method DELETE
    } elseif { [lsearch -nocase $hints "POST"] >= 0 } {
        lappend cmd -method POST
    }
    
    # Keepalive by default
    if { [lsearch -nocase $hints "NOKEEPALIVE"] < 0 } {
        lappend cmd -keepalive 1
    }
    
    # Push out payload, if any and relevant
    if { $payload ne "" } {
        lappend cmd -query $payload -type application/json
    }
    
    # And do the call, arranging to be called back once done.
    lappend cmd -command [list [namespace current]::Pushed $d $url]
    if { [catch {eval $cmd} err] } {
        ::utils::debug 1 "Could not trigger HTTP url at $url for $DST(-name): $err"
        $d memorise;  # Nullify cache since we couldn't open connection
    } else {
        ::utils::debug 5 "Triggering remote destination with changes"
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::dst::http::Pushed -- HTTP finalisation callback
#
#       Finalise an HTTP push operation. The HTTP context is cleaned
#       up and the result of the operation is logged out.
#
# Arguments:
#	d	Identifier of destination
#	tok	HTTP operation token.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::dst::http::Pushed { d url tok } {
    upvar \#0 $d DST

    set code [::http::ncode $tok]
    if { $code ne "" && $code >= 200 && $code < 300 } {
        ::utils::debug 3 "Successfully triggered $DST(-name) (at $url): $code"
    } else {
        $d memorise;    # Nullifies cache, since we didn't succeed
	    ::utils::debug 1 "Could not trigger $DST(-name) (at $url):\
                          $code -- err:[::http::error $tok]\
                          -- status:[::http::status $tok]\
                          -- data:[::biot::common::human [::http::data $tok] 30]"
    }
    ::http::cleanup $tok
}



package provide biot::dst::http 0.1
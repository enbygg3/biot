package require mime
package require smtp
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::smtp {
    namespace eval vars {
	variable -queue    on;  # Should we queue messages.
        variable -port     25;  # Default port
    }
    namespace export {[a-z]*}
    [namespace parent]::register smtp \
	-push [namespace current]::push \
	-defaults [namespace current]::defaults
}

proc ::biot::dst::smtp::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::dst::smtp::push -- Push data to remote SMTPP server
#
#       This procedure will initiate an HTTP operation to push the
#       payload to the remote URL using HTTP.  Hints (coming from the
#       initial scheme) are used to know what kind of http operation
#       to perform and if it should be encrypted or not.
#
# Arguments:
#	d	Identifier of the destination
#	url	Valid URL to send data to. 
#	payload	Data to send (can be empty)
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects: 
#       Perform an HTTP operation to remote site.
proc ::biot::dst::smtp::push { d url payload hints headers } {
    upvar \#0 $d DST

    # Now initiate the construction of an HTTP operation with the
    # proper headers, i.e. headers as specified at the destination,
    # but also authentication information if necessary.
    set auth [::biot::uri::auth $url curl]
    foreach {host port} [split [::biot::uri::hostport $url ${vars::-port}] :] break
    
    if { [catch {::mime::initialize -canonical text/plain -string $payload} mtok] == 0 } {
        # Construct beginning of command to send message and dynamically add
        # username and password information if relevant. Connection will
        # automatically be upgrade to TLS while handshaking to the server.
        set cmd [list ::smtp::sendmessage $mtok -servers $host -ports $port -queue ${vars::-queue}]
        if { $auth ne "" } {
            foreach {uname paswd} [split $auth :] break
            lappend cmd -username $uname -password $paswd
        }
        
        foreach {k v} $headers {
            lappend cmd -header [list $k $v]
        }

        if { [catch {eval $cmd} res] == 0 } {
            ::utils::debug INFO "Sent mail message to ${host}:${port}"
        } else {
            ::utils::debug WARN "Cannot send message via ${host}:${port}: $res"
            $d memorise
        }
        ::mime::finalize $mtok
    } else {
        ::utils::debug WARN "Cannot parse MIME content from payload: $mtok"
        $d memorise
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


package provide biot::dst::smtp 0.1

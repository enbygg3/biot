package require mqtt
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::mqtt {
    namespace eval vars {
        variable -keepalive   60
        variable -name        "%hostname%-%pid%-%prgname%"
        variable -clean       on
        variable -qos         1
        variable -retain      off
        variable -retransmit  5000
    }
    namespace export {[a-z]*}
    [namespace parent]::register mqtt \
        -push [namespace current]::push \
        -connect [namespace current]::connect \
        -resolver [list mqtts [list ssl tls]] \
        -defaults [namespace current]::defaults
}


# ::biot::src::mqtt::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::dst::mqtt::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


proc ::biot::dst::mqtt::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    set hstpath [regsub "^mqtt(s|)://" $hstpath ""]
    if { $payload ne "" } {
	if { $DST(_mqtt) ne "" } {
	    set slash [string first "/" $hstpath]
            set topic [string range $hstpath [expr {$slash+1}] end]
            set qos ${vars::-qos}
            if { [dict exists $headers qos] } {
                set qos [dict get $headers qos]
            } elseif { [dict exists $headers QoS] } {
                set qos [dict get $headers QoS]
            }
            set retain [string is true ${vars::-retain}]
            if { [dict exists $headers retain] } {
                set retain [string is true [dict get $headers retain]]
            }
	    $DST(_mqtt) publish $topic $payload $qos $retain
            ::utils::debug 5 "Successfully triggered MQTT topic $topic"
	} else {
	    ::utils::debug 3 "No MQTT connection yet,\
                              payload will be lost!"
	}
    }
}


proc ::biot::dst::mqtt::connect { d uri hints headers } {
    upvar \#0 $d DST
    set DST(_mqtt) ""

    set auth [::biot::uri::auth $uri curl]
    set  usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break
    
    ::utils::debug 4 "Connecting to remote MQTT server at $curl"
    if { [string match "mqtts://*" $url] } {
        set cmd [::toclbox::network::tls_socket]
    } else {
        set cmd [list socket]
    }

    set surl [regsub "^mqtt(s|)://" $curl ""]
    set slash [string first "/" $surl]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    set DST(_mqtt) [mqtt new \
                        -username $usr \
                        -password $pasw \
                        -socketcmd $cmd \
                        -keepalive ${vars::-keepalive} \
                        -retransmit ${vars::-retransmit} \
                        -clean ${vars::-clean}]
    $SRC(mqtt:cx) subscribe \$LOCAL/connection[list [namespace current]::Liveness $d]
    $SRC(mqtt:cx) subscribe \$LOCAL/publication [list [namespace current]::Liveness $d]
    set cname [::utils::resolve ${vars::-name} \
                        [list   hostname [info hostname] \
                                pid [pid]]]
    set cname [string range $cname 0 22]
    if { $prt eq "" } {
        ::utils::debug INFO "Connecting to MQTT server ${hst}"
        $DST(_mqtt) connect $cname $hst
    } else {
        ::utils::debug INFO "Connecting to MQTT server ${hst}:${prt}"
        $DST(_mqtt) connect $cname $hst $prt
    }
}

####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::dst::mqtt::Liveness { d topic dta } {
    switch -glob -- $topic {
        "*/connection" {
            switch -- [dict get $dta state] {
                "connected" {
                    ::utils::debug NOTICE "Connected to broker"
                }
                "disconnected" {
                    array set reasons {
                        0 "Normal disconnect"
                        1 "Unacceptable protocol version"
                        2 "Identifier rejected"
                        3 "Server unavailable"
                        4 "Bad user name or password"
                        5 "Not authorized"
                    }
                    ::utils::debug WARN "Disconnected from broker $reasons([dict get $dta reason])"
                }
            }
        }
        "*/publication" {
            ::utils::debug DEBUG "Data has been published at [dict get $dta topic]"
        }
    }
}



package provide ::biot::dst::mqtt 0.1

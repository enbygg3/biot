package require tls
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::tcp {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register tcp* \
	-push [namespace current]::push \
	-connect [namespace current]::connect \
	-resolver [list tcps [list ssl tls]]
}

proc ::biot::dst::tcp::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    if { $payload ne "" } {
	if { $DST(_sock) ne "" } {
	    if { [lsearch -nocase $hints "BINARY"] >= 0 } {
		fconfigure $DST(_sock) -encoding binary -translation binary
	    }
	    if { [catch {puts $DST(_sock) [string trim $payload]}] == 0 } {
		if { [catch {flush $DST(_sock)}] == 0 } {
		    ::utils::debug 5 "Successfully sent data to remote server" \
			dst
		} else {
		    set DST(_sock) ""
		    ::utils::debug 3 "Could not send data to remote server" dst
		}
	    } else {
		set DST(_sock) ""
		::utils::debug 3 "Could not send data to remote server" dst
	    }
	    if { $DST(_sock) eq "" || [eof $DST(_sock)] } {
		set DST(_sock) ""
		if { [lsearch -nocase $hints "NORESTART"] < 0 } {
		    ::utils::debug 3 "Connection to server lost, reopening" dst
		    $d reconnect
		}
	    }
	} else {
	    ::utils::debug 3 "No connection to server, payload will be\
                              lost" dst
	    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
		::utils::debug 3 "Connection to server lost, reopening" dst
		$d reconnect
	    }
	}
    }
}


proc ::biot::dst::tcp::connect { d uri hints headers } {
    upvar \#0 $d DST
    set DST(_sock) ""
    set s [string first "://" $uri]
    set scheme [string range $uri 0 [expr {$s-1}]]
    set slash [string first "/" $uri [expr {$s+3}]]
    if { $slash >= 0 } {
	set hstprt [string range $uri [expr {$s+3}] [expr {$slash-1}]]
    } else {
	set hstprt [string range $uri [expr {$s+3}] end]
    }
    foreach {hst prt} [split $hstprt ":"] break
    ::utils::debug 4 "Connecting to remote TCP server at ${hst}:${prt}"
    
    # Choose command: tls::socket or socket.
    if { [string index $scheme end] eq "s" } {
	set s_cmd [list ::tls::socket -tls1 1]
    } else {
	set s_cmd [list socket]
    }
    
    # Open socket.
    if { [catch {eval [linsert $s_cmd end $hst $prt]} sock] } {
	::utils::debug 3 "Cannot connect to server: $sock"
    } else {
	set DST(_sock) $sock
    }
}

package provide biot::dst::tcp 0.1

package require stomp::client
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::stomp {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register stomp \
	-push [namespace current]::push \
	-connect [namespace current]::connect
}

proc ::biot::dst::stomp::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    set hstpath [regsub "^stomp://" $hstpath ""]
    if { $payload ne "" } {
	if { $DST(_stomp) ne "" } {
	    set slash [string first "/" $hstpath]
	    set topic [string range $hstpath $slash end]
	    if { [::stomp::client::send $DST(_stomp) $topic \
		      -headers $headers \
		      -body $payload] } {
		::utils::debug 5 "Successfully triggered STOMP topic $topic"
	    } else {
		::utils::debug 3 "Could not send payload to\
                                  STOMP topic $topic"
	    }
	} else {
	    ::utils::debug 3 "No STOMP connection yet,\
                              payload will be lost!"
	}
    }
}


proc ::biot::dst::stomp::connect { d uri hints headers } {
    upvar \#0 $d DST
    set DST(_stomp) ""

    set auth [::biot::uri::auth $uri curl]
    set  usr ""; set pasw ""
    foreach { usr pasw } [split $auth ":"] break
    
    ::utils::debug 4 "Connecting to remote STOMP server at $curl"
    set surl [regsub "^stomp://" $curl ""]
    set slash [string first "/" $surl]
    set topic [string range $surl $slash end]
    set hstprt [string range $surl 0 [expr {$slash-1}]]
    foreach {hst prt} [split $hstprt ":"] break
    
    if { $prt eq "" } {
	set stomp [::stomp::client::connect \
		       -host $hst \
		       -user $usr \
		       -password $pasw]
    } else {
	set stomp [::stomp::client::connect \
		       -host $hst \
		       -port $prt \
		       -user $usr \
		       -password $pasw]
    }
    
    ::stomp::client::handler $stomp \
	[list [namespace current]::Handler $d $stomp $hints] *
}

####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

proc ::biot::dst::stomp::Handler { d stomp hints type } {
    upvar \#0 $d DST
    switch -nocase -- $type {
	"CONNECTED" {
	    set DST(_stomp) $stomp
	    ::utils::debug 4 "Connected to STOMP server"
	}
	"ERROR" -
	"DISCONNECT" {
	    set DST(_stomp) ""
	    # Reopen if proper hints
	    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
		::utils::debug 3 "STOMP connection lost, reopening"
		$d reconnect
	    }
	}
	default {
	    ::utils::debug 5 "Received STOMP message $type"
	}
    }
}


package provide ::biot::dst::stomp 0.1

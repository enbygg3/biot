package require utils
package require biot::uri

namespace eval ::biot::dst::file {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register file \
	-push [namespace current]::push
}


# ::biot::dst::file::push -- Push data to local file
#
#       This procedure send the data coming from the payload to a
#       local file.  Hints (coming from the initial scheme) are used
#       to append to or replace any existing file, and to
#       automatically make (or not) any intermediary directory
#       specified by the file path.
#
# Arguments:
#	d	Identifier of the destination
#	fpath	Path to file
#	payload	Data to append/replace file content with
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Appends to or replace content of file.
proc ::biot::dst::file::push { d fpath payload hints headers  } {
    upvar \#0 $d DST

    # Decide upon writing mode.  Standard is to overwrite the file
    # each time, the hint "append" can be used to append to the file
    # each time instead.
    set mode "w"
    if { [lsearch -nocase $hints "APPEND"] >= 0 } {
	set mode "a"
    }

    if { [string first "file://" $fpath] >= 0 } {
	set fpath [string range $fpath [string length "file://"] end]
    }
    
    if { [string trimleft $fpath "/"] eq "stdout" \
	    || [string trimleft $fpath "/"] eq "stderr" } {
	# stdout and stderr are always opened
	puts -nonewline $fpath $payload
	flush $fpath
	::utils::debug 3 "Successfully triggered $DST(-name) (to $fpath)" dst
    } else {
	# Create all necessary directories along the path to the file
	# unless the hint "nomkdir" was specified.
	if { [lsearch -nocase $hints "NOMKDIR"] < 0 } {
	    set fdir [file dirname $fpath]
	    if { ! [file isdirectory $fdir] } {
		::utils::debug 3 "Creating directory at $fdir to host destination file" dst
		if { [catch {file mkdir $fdir} err] } {
		    ::utils::debug 2 "Could not make directory $fdir!" dst
		}
	    }
	}

	# Open file and dump data to it. Close at once, we don't want to
	# keep the descriptor opened.
	if { [catch {open $fpath $mode} fd] } {
	    ::utils::debug 2 "Could not open $fpath (mode: $mode)! $fd" dst
	    $d memorise
	} else {
	    puts -nonewline $fd $payload
	    close $fd
	    ::utils::debug 3 "Successfully triggered $DST(-name) (to $fpath)" dst
	}
    }
}

package provide biot::dst::file 0.1

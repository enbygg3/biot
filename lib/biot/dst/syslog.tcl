package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::syslog {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register syslog \
	-push [namespace current]::push
}


proc ::biot::dst::syslog::push { d hstpath payload hints headers } {
    upvar \#0 $d DST

    # syslog:app will contain the connected application context that
    # we will use to output data.  Make sure we always have one.
    if { ![info exists DST(syslog:app)] } {
	set DST(syslog:app) ""
    }

    # No application context to send syslog message with, open the
    # connection, possibly extract the name of the application that we
    # wish to be represented with and bind the connection to the
    # application context.
    if { $DST(syslog:app) eq "" } {
	set DST(syslog:cx) [::biot::common::sconnect $hints $hstpath appname]
	if { $DST(syslog:cx) ne "" } {
	    ::utils::debug 3 "Successfully connected to syslog daemon"
	    if { $appname ne "" } {
		set DST(syslog:app) [::syslog::application -name $appname]
	    } else {
		set DST(syslog:app) [::syslog::application]
	    }
	    ::syslog::bind $DST(syslog:app) $DST(syslog:cx)
	}
    }

    # We have a syslog application context. Try to find valid facility
    # and severity keywords from the hints and use them, otherwise we
    # will default.  Once we have, send the payload as one log line.
    if { $DST(syslog:app) ne "" } {
	set facility "user"
	foreach k [array names ::syslog::FACILITY] {
	    if { [lsearch -nocase $hints $k] } {
		set facility $k
		break
	    }
	}

	set severity "notice"
	foreach k [array names ::syslog::SEVERITY] {
	    if { [lsearch -nocase $hints $k] } {
		set severity $k
		break
	    }
	}
	::utils::debug 3 "Passing payload data to (remote) syslog daemon\
                          $DST(-name)"
	::syslog::log $DST(syslog:app) $payload \
	    -facility $facility \
	    -severity $severity
    }
}

package provide ::biot::dst::syslog 0.1

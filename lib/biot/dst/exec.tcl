package require utils
package require biot::uri

namespace eval ::biot::dst::exec {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register exec \
            -push [namespace current]::push
}

# ::biot::dst::exec::push -- Push data to external program
#
#       This procedure send the data coming from the payload to a the
#       standard input of an external program.  Hints (coming from the
#       initial scheme) are used to restart the program every time the
#       payload changes or not to forward data (in which case the
#       program is simply restarted everytime).
#
# Arguments:
#	d	Identifier of the destination
#	pipe	Command pipe to pass data to
#	payload	Data to append/replace file content with
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Will run an external program, there is no control on which
#       program can or cannot be run.
proc ::biot::dst::exec::push { d pipe payload hints headers } {
    upvar \#0 $d DST
    
    if { [string first "exec://" $pipe] >= 0 } {
        set pipe [string range $pipe [string length "exec://"] end]
    }
    
    # exec:pid will contain the program identifiers of the program(s)
    # that are started as directed by the pipe.  Make sure we always
    # have something in that field.
    if { ![info exists DST(exec:pid)] } {
        set DST(exec:pid) ""
    }
    
    # If we had a program running and the external program should be
    # restarted each time (i.e. each the payload or url changes),
    # start by killing prior instance(s) of the program(s) in the
    # pipe.
    if { $DST(exec:pid) ne "" } {
        if { [lsearch -nocase $hints "RESTART"] >= 0 } {
            set kill [auto_execok kill]
            if { $kill ne "" } {
                # Kill all instances and mark success by nullifying
                # exec:pid to an empty list on success.
                ::utils::debug 3 "Killing previous instance(s) at\
                                $DST(exec:pid)" dst
                foreach pid $DST(exec:pid) {
                    if { [catch {exec kill $pid} err] } {
                        ::utils::debug 2 "Problems killing prior instance:\
                                        $err" dst
                    }
                }
                set DST(exec:pid) ""
            } else {
                ::utils::debug 2 "Cannot kill previous instance(s) at\
                                $DST(exec:pid)" dst
            }
        } elseif { [lsearch -nocase $hints "IGNORE"] >= 0 \
                    || [lsearch -nocase $hints "NOKILL"] >= 0 } {
            ::utils::debug 3 "Ignoring previous instance at $DST(exec:pid)" dst
            set DST(exec:pid) ""
        }
    }
    
    # If no external program is running, start the program(s) that are
    # specified by the pipe and make sure that we will be able to push
    # data to the pipe.
    if { $DST(exec:pid) eq "" } {
        # Close previous connection to external program, if any
        if { [info exists DST(exec:fd)] && $DST(exec:fd) ne "" } {
            catch {close $DST(exec:fd)} err
            ::utils::debug INFO "Prior call returned $err" dst
        }
        set DST(exec:fd) ""
        
        # Open the pipe, prepend the | sign and open the pipe for
        # writing since we will soon be trying to push data to the
        # stdin.
        if { [catch {open |$pipe w} fd] } {
            ::utils::debug 2 "Could not start external program $pipe: $fd" dst
            $d memorise
        } else {
            # Success, remember the identifiers of the programs in the
            # pipe and the file descriptor that really is the stdin of
            # the first program in the pipe.
            set DST(exec:pid) [pid $fd]
            set DST(exec:fd) $fd
            # Line buffering is forced here.
            if { [lsearch -glob -nocase $hints "BIN*"] >= 0 } {
                fconfigure $DST(exec:fd) -encoding binary -translation binary
            } else {
                fconfigure $DST(exec:fd) -buffering line
            }
            ::utils::debug 3 "Successfully started external program $pipe with\
                              process identifier(s) $DST(exec:pid)" dst
        }
    }
    
    # If we have an opened file descriptor to the pipe, a payload to
    # send and no hint that would stop us, send all payload to the
    # stdin of the first program in the external program pipe.
    if { $DST(exec:fd) ne "" && $payload ne "" } {
        if { [lsearch -glob -nocase $hints "NOF*W*D"] < 0 } {
            puts $DST(exec:fd) $payload
            flush $DST(exec:fd)
	        ::utils::debug 3 "Successfully passed payload to external program\
                              $DST(-name)" dst
        }
    }
}

package provide biot::dst::exec 0.1


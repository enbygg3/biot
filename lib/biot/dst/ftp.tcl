# Lazily load the version of the FTP library into a namespace local variable to
# be able to take decisions later.
namespace eval ::biot::dst::ftp {
    namespace eval vars {
        variable version     "";  # Version of the FTP library
	variable -timeout    20;  # Timeouts for HTTP calls, in seconds.
    }
}
if { [catch {package require ftp} ver] == 0 } {
    set ::biot::dst::ftp::vars::version $ver
}

package require utils
package require biot::uri

namespace eval ::biot::dst::ftp {
    namespace export {[a-z]*}
    # Register plugin only if we actually are able to perform FTP operations
    # using the library.
    if { $vars::version ne "" } {
        [namespace parent]::register ftp \
            -push [namespace current]::push \
            -defaults [namespace current]::defaults
    }
}

proc ::biot::dst::ftp::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}

# ::biot::dst::ftp::push -- Push data to remote FTP server
#
#       This procedure send the data coming from the payload to an
#       FTP server.  Hints (coming from the initial scheme) are used
#       to automatically make (or not) any intermediary directory specified by
#       the file path.
#
# Arguments:
#	d	Identifier of the destination
#	url	Path to server and file
#	payload	Data to append/replace file content with
#	hints	Hints as extracted from the destination's URL scheme
#
# Results:
#       None.
#
# Side Effects:
#       Appends to or replace content of file.
proc ::biot::dst::ftp::push { d url payload hints headers  } {
    upvar \#0 $d DST

    # Now extract authorisation details from the URL and default to anonymous.
    set auth [::biot::uri::auth $url curl]
    if { $auth ne "" } {
        foreach {user paswd} [split $auth ":"] break
    } else {
        set user Anonymous
        set paswd ""
        foreach k [list "USER" "USERNAME"] {
            if { [info exists ::env($k)] } {
                set paswd [set ::env($k)]@[info hostname]
                break
            }
        }
    }

    # Extract destination path and host and port from URL
    if { [string first "ftp://" $curl] >= 0 } {
	set hostpath [string range $curl [string length "ftp://"] end]
        set slash [string first "/" $hostpath]
        if { $slash >= 0 } {
            set fpath [string range $hostpath $slash end]
            set hostport [string range $hostpath 0 [expr {$slash-1}]]
        } else {
            set fpath /
            set hostport $hostpath
        }
    }
    foreach {host port} [split $hostport ":"] break
    if { $port eq "" } { set port 21}
    
    set ftp [::ftp::Open $host $user $paswd \
                    -port $port \
                    -timeout ${vars::-timeout} \
                    -mode passive]
    if { $ftp < 0 } {
        ::utils::debug ERROR "Cannot open FTP connection to ${host}:${port}!"
        $d memorise
    } else {
        set fdir [file dirname $fpath]
        if { $fdir eq "/" } {
            set output 1
        } else {
            set output 0
            if { [::ftp::Cd $ftp $fdir] == 0 } {
                if { [lsearch -nocase $hints "NOMKDIR"] < 0 } {
                    if { [::ftp::MkDir $ftp $fdir] == 0 } {
                        ::utils::debug ERROR "Cannot create direction $fdir at remote"
                    } else {
                        set output 1
                    }
                } else {
                    ::utils::debug ERROR "Destination direction $fdir does not exist at server"
                }
            } else {
                ::utils::debug ERROR "Cannot change directory to $fdir"
            }
        }
        
        if { $output } {
            if { [string is ascii $payload] } {
                ::utils::debug DEBUG "Using text mode for data transfer"
                ::ftp::Type $ftp text
            } else {
                ::utils::debug DEBUG "Using binary mode for data transfer"
                ::ftp::Type $ftp binary
            }
            if { [lsearch -nocase $hints "APPEND"] >= 0 } {
                ::ftp::Append $ftp -data $payload [file tail $fpath]
            } else {
                ::ftp::Put $ftp -data $payload [file tail $fpath]
            }
            ::ftp::Close $ftp
            ::utils::debug 3 "Successfully triggered $DST(-name) (to $curl)" dst
        } else {
            $d memorise
        }
    }
}

package provide biot::dst::ftp 0.1

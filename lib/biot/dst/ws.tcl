package require websocket 1.4
package require utils
package require biot::uri
package require biot::common

namespace eval ::biot::dst::ws {
    namespace eval vars {
    }
    namespace export {[a-z]*}
    [namespace parent]::register ws* \
	-push [namespace current]::push \
	-connect [namespace current]::connect \
	-resolver [list wss [list ssl tls]]
}

proc ::biot::dst::ws::push { d hstpath payload hints headers } {
    upvar \#0 $d DST
    if { $payload ne "" } {
	if { $DST(_ws) ne "" } {
	    set mode "text"
	    if { [lsearch -nocase $hints "BINARY"] >= 0 } {
		set mode "binary"
	    }
	    set len [::websocket::send $DST(_ws) $mode $payload]
	    if { $len < 0 } {
		::utils::debug 3 "Could not send payload to websocket,\
                                  type: $mode"
	    }
	} else {
	    # Loose the payload, but we could, instead, enqueue and
	    # try sending the content of the queue when connection has
	    # been (re)established.  Queue should then probably bound
	    # to a proper maximum to keep low on memory resources.
	    ::utils::debug 3 "No WS connection yet, payload will be lost!"
	}
    }
}

proc ::biot::dst::ws::connect { d uri hints headers } {
    upvar \#0 $d DST
    set DST(_ws) ""
    set auth [::biot::uri::auth $uri curl]
    if { $auth ne "" } {
	lappend headers "Authorization" "Basic [::base64::encode $auth]"
    }
    ::utils::debug 4 "Connecting to remote WS server at $curl"
    ::websocket::open $curl [list [namespace current]::Handler $d $hints] \
	-headers $headers
}


proc ::biot::dst::ws::Handler { d hints sock type msg } {
    upvar \#0 $d DST
    switch -- $type {
	"connect" {
	    # Remember socket now only, so we use DST(_ws) as a flag
	    # for proper connection to the remote server.
	    set DST(_ws) $sock
	    ::utils::debug 4 "WS for $DST(-url) connected"
	}
	"error" -
	"disconnect" {
	    set DST(_ws) ""
	    # Reopen socket connection as soon as possible.
	    if { [lsearch -nocase $hints "NORESTART"] < 0 } {
		::utils::debug 3 "Websocket connection lost, reopening"
		$d reconnect
	    }
	}
	default {
	    ::utils::debug 5 "Received WS message '$type': $msg"
	}
    }
}


package provide biot::dst::ws 0.1

##################
## Module Name     --  src
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      This module will create "objects" (Tk-style) convention for
##      all known sources of information.  Each source is represented
##      by a unique namespace variable and this variable is also the
##      name of a command which can be used to perform operations on
##      the source.  Upon connection, sources will be handled to the
##      relevant plugin, which will arrange to receive (or poll, if
##      necessary) data from the source and push it further using the
##      push procedure.  Plugins are read from the list of plugins
##      directories -plugdirs.
##
## Commands Exported:
##      All procedures starting with a lowercase are exported.  Each
##      source will be associated to a command for tk-style API.  Also
##      this creates a biots (s as in source) global command to ease
##      calling from the outside.
##################

namespace eval ::biot::src {
    namespace eval vars {
        variable -timeout      20
        variable -flush        100
        variable -plugdirs     {%libdir%/src}
        variable -separators   ".:,"
        variable -watchdog     600
        
        variable libdir        [file dirname [file normalize [info script]]]
    }
    
    namespace export {[a-z]*}
    namespace ensemble create -command ::biots
}


# ::biot::src::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::src::defaults { {args {}}} {
    set dfts [::utils::mset [namespace current]::vars $args -]
    PropagateDefaults
    return $dfts
}


# ::biot::src::new -- Create a new source
#
#       Create and initialise a new source.  All variables for the
#       source (internal and "external") are initialised to good
#       defaults.
#
# Arguments:
#	name	Name of source (mandatory)
#	fname	Name of file from which source was read from (empty=ok)
#	args	Dash-led options and values for the source parameters.
#
# Results:
#       Return an identifier for the array representing the source in
#       this namespace (also a command).
#
# Side Effects:
#       None.
proc ::biot::src::new { name fname args } {
    set vname [::utils::identifier [namespace current]::source:$name:]
    upvar \#0 $vname SRC
    
    # Initialise internals
    set SRC(id) $vname
    set SRC(timer) ""
    set SRC(latest) 0
    set SRC(watchdog) ""
    set SRC(fname) $fname
    
    # Get parameters and values, initialise to good defaults.  Params
    # and dash-led in the array.
    foreach {k v} [list name $name url "" freq -1 headers "" \
            transforms "" request ""] {
        ::utils::getopt args -$k SRC(-$k) $v
    }
    
    # Perform one-time resolution to get rid of %-surrounded strings.
    Resolve $SRC(id) $SRC(fname)
    
    # Create object command, arrange for only the relevant procedures
    # to implement the relevant "methods".
    interp alias {} $vname {} \
            ::utils::rdispatch $vname [namespace current] \
            [list transform activity push connect]
    
    return $vname
}


# ::biot::src::multi -- Read definitions of sources
#
#       Read definitions from sources (or get them from the main
#       program arguments) and create and initialise representations
#       for these sources.
#
# Arguments:
#       data	A list multiple of 6 or the reference to a file containing the
#               sources representation.
#
# Results:
#       The identifiers of the sources that were created.
#
# Side Effects:
#       None.
proc ::biot::src::multi { data } {
    # Resolve file content to list of 6 elements.
    set data [::biot::common::inline $data \
            6 \
            "sources" \
            fname \
            [list] \
            "src"]
    
    # Create all sources in turns.
    set sources {}
    foreach { name url headers req transforms freq } $data {
        set id [new $name $fname \
                -url $url \
                -headers $headers \
                -request $req \
                -transforms $transforms \
                -freq $freq]
        lappend sources $id
    }
    
    return $sources
}


# ::biot::src::names -- Return names of sources
#
#       Return the names of the sources which match the pattern passed
#       as an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against source names.
#
# Results:
#       Return the names of all matching sources, empty if none.
#
# Side Effects:
#       None.
proc ::biot::src::names { { ptn * } } {
    set names {}
    foreach s [find $ptn] {
        upvar \#0 $s SRC
        lappend names $SRC(-name)
    }
    return $names
}


# ::biot::src::find -- Find source(s) by name pattern.
#
#       Return the identifiers of the sources which names match the
#       pattern passed as an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against source names.
#
# Results:
#       Return the identifiers of all matching sources, empty if none.
#
# Side Effects:
#       None.
proc ::biot::src::find { {ptn *} } {
    set sources {}
    foreach s [info vars [namespace current]::source:*:*] {
        upvar \#0 $s SRC
        if { [string match $ptn $SRC(-name)] } {
            lappend sources $s
        }
    }
    return $sources
}


# ::biot::src::connectall -- Connect all matching sources.
#
#       Connect all the sources which name match the pattern passed as
#       an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against source names.
#
# Results:
#       The identifiers of the sources that were connected and are
#       active.
#
# Side Effects:
#       None.
proc ::biot::src::connectall { {ptn *} } {
    set connected {}
    foreach s [find $ptn] {
        connect $s
        lappend connected $s
    }
    return $connected
}


# ::biot::src::transform -- Transform source data
#
#       This procedure applies, in order, the transformations that
#       have been defined for a given source onto an incoming data
#       string.  If the transform start with the keyword
#       "encodingfrom" followed by any of the globally registered
#       separators (: best here), the data is converted from the
#       encoding which identifier follows encodingfrom:.  Otherwise,
#       the transformation is a sed-like operation.  See ::utils::sed
#       for more details over which filters can be applied.  All
#       transformations are applied in order so that the result of one
#       transformation is passed further as input to the following
#       transformation in the pipe.
#
# Arguments:
#	s	Identifier of source.
#	data	(acquired) string data to transform
#
# Results:
#       Return the result of all transformations.
#
# Side Effects:
#       None.
proc ::biot::src::transform { s data } {
    upvar \#0 $s SRC
    
    ::utils::debug 5 "Executing [llength $SRC(-transforms)]\
            transformation(s) on data" source
    foreach t $SRC(-transforms) {
        if { $t ne "" } {
            if { [string match -nocase "encodingfrom*" $t] } {
                foreach {- enc} [[namespace parent]::common::psplit $t ${vars::-separators}] break
                set data [encoding convertfrom $enc $data]
            } else {
                set data [::utils::sed $t $data]
            }
        }
    }
    return $data
}


# ::biot::src::trimmer -- Trim leading/ending spaces
#
#       Recognises trimleft, trimright and trim as hints and trim data
#       that was acquired from spaces that would be at (respectively)
#       the left, the right or both ends.
#
# Arguments:
#	data	Data (string) to trim
#	hints	Hints from URL
#
# Results:
#       Trimmed (or not) incoming data
#
# Side Effects:
#       None.
proc ::biot::src::trimmer { data hints } {
    if {[lsearch -glob -nocase $hints "trimleft*"] >= 0 } {
        set data [string trimleft $data]
    }
    if {[lsearch -glob -nocase $hints "trimright*"] >= 0 } {
        set data [string trimright $data]
    }
    if {[lsearch -glob -nocase $hints "trim"] >= 0 } {
        set data [string trim $data]
    }
    
    return $data
}


# ::biot::src::activity -- Source activity marker
#
#       Register some (positive) activity from the source.  Normally,
#       sources that have been silent for too long (-watchdog seconds)
#       will automatically be (re-)connected.  This behaviour can be
#       switched off using the norestart URL hint.
#
# Arguments:
#	s	Identifier of the source
#	hints	Source hints.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::activity { s hints } {
    global BDG
    
    upvar \#0 $s SRC
    
    set SRC(latest) [clock seconds]
    if { $SRC(watchdog) ne "" } {
        after cancel $SRC(watchdog)
        set SRC(watchdog) ""
    }
    
    if { ${vars::-watchdog} > 0 } {
        set when [expr {int(${vars::-watchdog}*1000)}]
        if { [lsearch -glob -nocase $hints "NORESTART"] < 0 } {
            ::utils::debug 5 "Will reconnect $SRC(-name) if silent for\
                              $when ms from now" source
            set SRC(watchdog) [after $when [list $s connect 1]]
        }
    }
}


# ::biot::src::push -- Push data from source
#
#       Push data that was acquired from a source to all the variables
#       that have been bound to that source.  The data passes through
#       the transformations encoding and replacement (sed-like)
#       transformations that might be associated to the source.
#
# Arguments:
#	s	Identifier of the source
#	data	(raw) data that was got from the source.
#	where	Where the data was acquired at the source (e.g. topic).
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::push { s data args } {
    global BDG
    
    upvar \#0 $s SRC
    
    # Transformed incoming data
    set response [transform $s $data]
    ::utils::debug 5 "Analysing transformed data from source $response"
    
    # Update all variables that are bound to the source
    set vars [list]
    foreach v [::biot::vrbl::find $SRC(-name)] {
        if { [eval [linsert $args 0 $v update $response]] } {
            lappend vars $v
        }
    }
    ::utils::debug 4 "[llength $vars] variable(s) updated"
}


# ::biot::src::register -- Register a plugin
#
#       All plugins should call this procedure to be registered as a
#       handler for the shemes that match the pattern passed as a
#       parameter.  Plugins can use the following dash-led options:
#       -get      The command to call to start receiving data from source.
#       -defaults The command to call to receive plugin defaults.
#       -resolver List for hints resolution
#
# Arguments:
#	match	Pattern to match against source URI.
#	args	Dash-led options and values for plugin arguments.
#
# Results:
#       Identifier of the plugin object.
#
# Side Effects:
#       None.
proc ::biot::src::register { match args } {
    set p [::utils::identifier [namespace current]::plugin:]
    upvar \#0 $p PIN
    
    set PIN(id) $p
    set PIN(match) $match
    foreach {k v} [list resolver {} get "" defaults ""] {
        ::utils::getopt args -$k PIN(-$k) $v
    }
    
    return $p
}


# ::biot::src::connect -- Open connection to source
#
#       Open connection to source. This extracts the hints out of the
#       scheme and resolve the URL using environment variables and
#       variables global to the program.  Different methods are then
#       triggered depending on the main switch (HTTP, websocket, exec,
#       file, etc.)
#
# Arguments:
#	s	Identifier of the source
#
# Results:
#       None.
#
# Side Effects:
#       Will open necessary socket, sometimes starting to poll, etc.
proc ::biot::src::connect { s {forced 0}} {
    upvar \#0 $s SRC
    
    # Verbose reconnection
    if { $forced && $SRC(latest) != 0 } {
        set now [clock seconds]
        set howlong [expr {$now-$SRC(latest)}]
            ::utils::debug 3 "Source $SRC(-name) has been silent for $howlong s,\
                              attempting reconnection"
    }
    
    # Use a main switching core to detect plugins and get data.
    Switch $s
}


# ::biot::src::biot -- Internal interfacing for interpreters
#
#      This procedure will typically be aliased into sub-interpreters (e.g. when
#      implementing sources as interpreters or when templating using interps) so
#      as to provide them with a generic interface to access internal details of
#      the implementation.
#
# Arguments:
#      s        Identifier of the source this is being run in
#      cmd      Sub-command to execute
#      args     Arguments to the command
#
# Results:
#      Depends on the command.
#
# Side Effects:
#      Provides commands for interacting with the internal state, including
#      creating/setting variables.
proc ::biot::src::biot { s cmd args } {
    upvar \#0 $s SRC
    
    switch -- $cmd {
        vars {
            set ptn [lindex $args 0]
            lassign $args ptn src
            if { $ptn eq "" } { set ptn "*" }
            if { $src eq "" } { set src $SRC(-name) }
            return [[namespace parent]::vrbl::names $src $ptn]
        }
        new {
            if { [llength $args] < 1 } {
                return -code error "You need at least to specifiy a variable name"
            }
            lassign $args v dft
            if { [llength [[namespace parent]::vrbl::find $SRC(-name) $v]] } {
                return -code error "Variable $v already exists in source $SRC(-name)"
            }
            set var [[namespace parent]::vrbl::new $v DELEGATED "" $SRC(-name) {*}[lrange $args 2 end]]
            if { [info exists dft] && $dft ne "" } {
                upvar \#0 $var VAR
                set VAR(value) $dft
            }
        }
        set {
            # Look for the name of the variable as the first argument and
            # resolve it to an existing variable at the same source.
            set var [FindVar $s [lindex $args 0]]
            return [eval [lreplace $args 0 0 [namespace parent]::vrbl::setval $var]]
        }
        get {
            lassign $args vname src
            set var [FindVar $s $vname $src]
            upvar \#0 $var VAR
            return $VAR(value)
        }
        "when" -
        "where" {
            lassign $args vname
            set v [FindVar $s $vname]
            if { $v ne "" } {
                upvar \#0 $v VAR
                if { [llength $args] >= 2 } {
                    set val [lindex $args 1]
                    set VAR($cmd) $val
                } else {
                    return $VAR($cmd)
                }
            }
        }
        flush {
            [namespace parent]::vrbl::Flush
        }
        log -
        debug {
            lassign $args lvl msg
            ::utils::debug $lvl $msg
        }
    }
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################

# ::biot::src::FindVar -- Find source
#
#       Find a source, this is a wrapper that depends on the origin source and
#       is implemented so as to minimise code in the biot procedure.
#
# Arguments:
#      s        Identifier of the source this is being run in
#      v        Name of variable to look for
#      src      Pattern for selecting which source to look in, empty means s.
#
# Results:
#      Identifier of the variable or an error.
#
# Side Effects:
#       None.
proc ::biot::src::FindVar { s { v "" } { src "" } } {
    upvar \#0 $s SRC
    
    if { $v eq "" } {
        return -code error "No variable name specified!"
    }
    if { $src eq "" } { set src $SRC(-name) }
    set var [lindex [[namespace parent]::vrbl::find $src $v] 0]
    if { $var eq "" } {
        return -code error "Cannot find variable $v in source $src"
    }
    return $var
}


# ::biot::src::Plugins -- Load plugins
#
#       Load the plugins found in the (unresolved) paths found in the
#       -plugdirs module option.
#
# Arguments:
#	force	Set to 1 to force reloading the plugins.
#
# Results:
#       List of plugins identifiers.
#
# Side Effects:
#       None.
proc ::biot::src::Plugins { {force 0} } {
    # Load the plugins
    set plugins [::biot::common::plugins \
            ${vars::-plugdirs} \
            [list libdir $vars::libdir] \
            [namespace current]::plugin:* \
            "source" \
            $force]
    
    # Propagate the module defaults to relevant plugins.
    PropagateDefaults
    
    return $plugins
}


# ::biot::src::PropagateDefaults -- Defaults inheritance
#
#       Arrange so that the plugins that have registered for it will
#       receive the list of defaults that were specified to this
#       module.  This allows inheriting main configuration parameters
#       to all plugins.
#
# Arguments:
#       None.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::PropagateDefaults {} {
    foreach p [info vars [namespace current]::plugin:*] {
        upvar \#0 $p PIN
        if { $PIN(-defaults) ne "" } {
            {*}$PIN(-defaults) \
                    -timeout ${vars::-timeout} \
                    -flush ${vars::-flush}
        }
    }
}


# ::biot::src::Resolve -- Resolve source
#
#       Resolve the source, i.e. arrange for its URL not to contain
#       any %-surrounded strings (they will be replaced by the values
#       of the global variables) and read the content of the request
#       from a template if necessary.
#
# Arguments:
#	s	Identifier of the source
#	context	Context for resolution of variables.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::src::Resolve { s { context "" } } {
    upvar \#0 $s SRC
    
    set SRC(-url) [[namespace parent]::common::resolve $SRC(-url) $context]
    
    # Resolve -request to content of template, if appropriate.  NOTE:
    # This will resolve the content once, and only once.  Maybe do we
    # want to execute this operation each time, maybe passing it the
    # set of variables??
    foreach idx [list -request -headers] {
        set firstchar [string index $SRC($idx) 0]
        if { $firstchar eq "@" || $firstchar eq "<" } {
            set stpl [::toclbox::templater::new]
            set spec [string trim [string range $SRC($idx) 1 end]]
            set fname [[namespace parent]::common::resolve \
                    [lindex $spec 0] $context]
            ::toclbox::templater::link $stpl $fname
            ::toclbox::templater::config $stpl \
                    -sourceCmd [list [namespace parent]::common::tplcache $fname]
            foreach v [lrange $spec 1 end] {
                set v [string trim $v %]
                ::utils::debug 5 "Passing local/env variable $v to template"
                ::toclbox::templater::setvar $stpl $v \
                        [[namespace parent]::common::resolve %$v% $context]
            }
            set SRC($idx) [::toclbox::templater::render $stpl]
        } else {
            set SRC($idx) [[namespace parent]::common::resolve $SRC($idx) $context]
        }
    }
}


# ::biot::src::connect::Switch -- Open connection to source
#
#       Open connection to source. This extracts the hints out of the
#       scheme and resolve the URL using environment variables and
#       variables global to the program.  The relevant opening command
#       will be triggered depending on the plugin options.  The options
#       are:
#       match     The pattern to match one of the hints to be part of the
#                 group of this handler
#       -resolver Possibly a list of several duplets scheme and list of hints,
#                 which might force in the scheme when one of the hints is
#                 found
#       -get      The command to call to start getting data
#
# Arguments:
#	s	Identifier of the source
#
# Results:
#       Return the plugin that got triggered, empty string otherwise
#
# Side Effects:
#       None.
proc ::biot::src::Switch { s } {
    upvar \#0 $s SRC
    
    # Extract the hints from the URI of the source, but also making
    # sure we capture the path
    set hints [[namespace parent]::uri::hints $SRC(-url) hstpath]
    
    # Now look among the known types and dispatch to the matching
    # command.  Matching can be fuzzy and some additional hints can be
    # used to resolve to definitive hints (this is how we recognised
    # https, but also http+tls for example).
    foreach p [info vars [namespace current]::plugin:*] {
        upvar \#0 $p PIN
        set uri [[namespace parent]::uri::reconstruct \
                $hstpath $hints $PIN(match) $PIN(-resolver)]
        if { $uri ne "" } {
            # Now the variable uri contains either an official URL or
            # the path or the incoming URL and we have in the variable
            # type the type of the URI (file, exec, http, etc.).
            # Perform dispatch to command if it exists.
            if { [llength [info commands $PIN(-get)]] >= 0 } {
                set uri [[namespace parent]::common::resolve $uri $SRC(fname)]
                {*}$PIN(-get) $s $uri $hints
                
                return $p
            } else {
                ::utils::debug 2 "Cannot find opening command for $PIN(match)"
            }
        }
    }
    return ""
}

# One time loading of plugins.  This is protected so we won't load
# them several times even if we'd call this several times.
::biot::src::Plugins

package provide biot::src 0.1



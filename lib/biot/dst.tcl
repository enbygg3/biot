##################
## Module Name     --  dst
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      This module will create "objects" (Tk-style) convention for
##      all known destinations of information.  Each destination is
##      represented by a unique namespace variable and this variable
##      is also the name of a command which can be used to perform
##      operations on the destination.  Plugins will receive requests
##      for connections to the destination, and data to be pushed to
##      the destination.  Plugins are read from the list of plugins
##      directories -plugdirs.
##
## Commands Exported:
##      All procedures starting with a lowercase are exported.  Each
##      destination will be associated to a command for tk-style API.
##      Also this creates a biotd (d as in destination) global command
##      to ease calling from the outside.
##################

package require netstats

namespace eval ::biot::dst {
    namespace eval vars {
        variable -separators   ".:,"
        variable -timeout      20
        variable -flush        100
        variable -silent       ""
        variable -plugdirs     {%libdir%/dst}
        
        variable libdir        [file dirname [file normalize [info script]]]
        variable idx           0
    }
    
    namespace export {[a-z]*}
    namespace ensemble create -command ::biotd
}

# ::biot::dst::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::dst::defaults { {args {}}} {
    set dfts [::utils::mset [namespace current]::vars $args -]
    PropagateDefaults
    return $dfts
}


# ::biot::dst::new -- Create a new destination
#
#       Create and initialise a new destination.  All variables for
#       the destination (internal and "external") are initialised to
#       good defaults.
#
# Arguments:
#	name	Name of destination (mandatory)
#	fname	Name of file from which dest was read from (empty=ok)
#	args	Dash-led options and values for the dest parameters.
#
# Results:
#       Return an identifier for the array representing the
#       destination in this namespace (also a command).
#
# Side Effects:
#       None.
proc ::biot::dst::new { name fname args } {
    set vname [::utils::identifier [namespace current]::destination:$name:]
    upvar \#0 $vname DST
    
    set DST(id) $vname
    set DST(fname) $fname
    set DST(templater) ""
    set DST(index) [incr vars::idx]
    set DST(rx) 0
    set DST(tx) 0
    set DST(-name) $name
    
    foreach {k v} [list url "" filters {"*"} template "" headers ""] {
        ::utils::getopt args -$k DST(-$k) $v
    }
    # Empty filters means all
    if { $DST(-filters) eq "" } {
        set DST(-filters) [list "*"]
    }
    
    # Create object command, arrange for only the relevant procedures
    # to implement the relevant "methods".
    interp alias {} $vname {} \
            ::utils::rdispatch $vname [namespace current] \
            [list memorise push reconnect connect]
    
    return $vname
}


# ::biot::dst::multi -- Read destinations description
#
#       Read descriptions of destination to send extracted variables
#       to from file(s) (or acquire directly from program arguments)
#       and create global representations for these destinations
#       within the program.
#
# Arguments:
#       data	A list multiple of 4 or the reference to a file containing the
#               destination representation.
#
# Results:
#       Return the list of destination identifiers that were created.
#
# Side Effects:
#       Will read definitions and templates if necessary.
proc ::biot::dst::multi { data } {
    set data [::biot::common::inline $data \
            4 \
            "destinations" \
            fname \
            [list "sources" [[namespace parent]::src::names] "vars" [::biot::vrbl::names]] \
            "dst"]
    
    set dests {}
    foreach { spec url template headers } $data {
        if { $spec ne "" } {
            set sl [[namespace parent]::common::psplit $spec ${vars::-separators}]
            set name [lindex $sl 0]
            set vname [new $name $fname \
                    -filters [lrange $sl 1 end] \
                    -url $url \
                    -template $template \
                    -headers $headers]
            lappend dests $vname
        }
    }
    
    return $dests
}


# ::biot::dst::register -- Register a plugin
#
#       All plugins should call this procedure to be registered as a
#       handler for the shemes that match the pattern passed as a
#       parameter.  Plugins can use the following dash-led options:
#       -push     The command to call to start receiving data to send.
#       -connect  The command to call to open connection
#       -defaults The command to call to receive plugin defaults.
#       -resolver List for hints resolution
#
# Arguments:
#	match	Pattern to match against destination URI.
#	args	Dash-led options and values for plugin arguments.
#
# Results:
#       Identifier of the plugin object.
#
# Side Effects:
#       None.
proc ::biot::dst::register { match args } {
    set p [::utils::identifier [namespace current]::plugin:]
    upvar \#0 $p PIN
    
    set PIN(id) $p
    set PIN(match) $match
    foreach {k v} [list resolver "" push "" connect "" defaults ""] {
        ::utils::getopt args -$k PIN(-$k) $v
    }
    
    return $p
}



# ::biot::dst::names -- Return names of destinations
#
#       Return the names of the destinations which match the pattern
#       passed as an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against destination names.
#
# Results:
#       Return the names of all matching destinations, empty if none.
#
# Side Effects:
#       None.
proc ::biot::dst::names { { ptn * } } {
    set names {}
    foreach d [find $ptn] {
        upvar \#0 $d DST
        lappend names $DST(-name)
    }
    return $names
}


# ::biot::dst::find -- Find destination(s) by name pattern.
#
#       Return the identifiers of the destinations which names match
#       the pattern passed as an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against destination names.
#
# Results:
#       Return the identifiers of all matching destinations, empty if none.
#
# Side Effects:
#       None.
proc ::biot::dst::find { {ptn *} } {
    set dests {}
    foreach d [info vars [namespace current]::destination:*:*] {
        upvar \#0 $d DST
        if { [string match $ptn $DST(-name)] } {
            lappend dests $d
        }
    }
    return $dests
}


# ::biot::dst::connectall -- Connect all matching destinations
#
#       Connect all the destinations which name match the pattern
#       passed as an argument.
#
# Arguments:
#	ptn	Glob-style pattern to match against destination names.
#
# Results:
#       The identifiers of the destinations that were connected and are
#       active.
#
# Side Effects:
#       None.
proc ::biot::dst::connectall { {ptn *} } {
    set connected {}
    foreach d [find $ptn] {
        connect $d
        lappend connected $d
    }
    return $connected
}


# ::biot::dst::memorise -- Memorise latest url and payload
#
#       Memorise the value of the latest URL and payload so we won't
#       generate the same trigger again if this was actual.  The
#       default is to remember empty strings, which effectively turns
#       off memorisation for the next time.
#
# Arguments:
#	d	Identifier of the destination
#	url	URL to memorise (since the URL of the destination is resolved).
#	payload	Payload to remember
#
# Results:
#       1 if something new was remembered since last call for that
#       destination, 0 otherwise.
#
# Side Effects:
#       None.
proc ::biot::dst::memorise { d { url "" } { payload "" } } {
    upvar \#0 $d DST
    
    set changed 0
    if { ![info exists DST(_payload)] || $DST(_payload) ne $payload } {
        set DST(_payload) $payload
        set changed 1
    }
    if { ![info exists DST(_url)] || $DST(_url) ne $url } {
        set DST(_url) $url
        set changed 1
    }
    return $changed
}


# ::biot::dst::push -- Push data to destination
#
#       Decide which "protocol" to use to send data to the
#       destination.  Hints are extracted from the destination URL,
#       and operations will be depending on the mail type of the
#       scheme (HTTP, file or external program).  The variable
#       snapshot and the list of variables that were changed are
#       passed further to the implementation for these operations.
#
# Arguments:
#	d	Identifier of the destination
#	mapper	Snapshot of all variables at the time of change
#	changes	List of variables that were changed.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::dst::push { d mapper changes } {
    upvar \#0 $d DST
    
    set uri [[namespace parent]::common::resolve \
            [string map $mapper $DST(-url)] $DST(fname)]
    set hints [::biot::uri::hints $uri hstpath]
    
    foreach p [info vars [namespace current]::plugin:*] {
        upvar \#0 $p PIN
        
        set uri [::biot::uri::reconstruct $hstpath $hints $PIN(match) $PIN(-resolver)]
        if { $uri ne "" } {
            # Now the variable uri contains either an official URL or
            # the path of the destination URL and we have in the
            # variable type the type of the URI (file, exec, http,
            # etc.).  Perform dispatch to command if it exists, which
            # would enable us to move away some of this code in a
            # plugin manner later on.
            if { [llength [info commands $PIN(-push)]] >= 0 } {
                # Compute payload and URL out of the current mapper,
                # which, really contains the variable mappings to
                # their value at the time of the call.
                set payload [trimmer [Payload $d $mapper $changes] $hints]
                if { [string trim $payload] eq "" } {
                    ::utils::debug 5 "Empty payload, skipping!"
                } else {
                    set headers [string map $mapper $DST(-headers)]
                    if { [memorise $d $uri $payload] } {
                        ::utils::debug 5 "Pushing data to local $PIN(match) destination\
					                      $DST(-name) at $uri, hints: $hints,\
                                          data: [::biot::common::human $payload]"
                        #netstats capture [list [namespace which Collect] $d] [list "open" "close" "read" "write"]
                        {*}$PIN(-push) $d $uri $payload $hints $headers
                        #netstats release
                    } else {
			            ::utils::debug 5 "Skipped posting same data to\
                                          $DST(-name)"
                    }
                }
                return $p
            } else {
                ::utils::debug 2 "Cannot find push command for $PIN(match)"
            }
        }
    }
}


# ::biot::dst::actives -- Ordered list of currently active destinations
#
#       This procedure returns the ordered list of currently active
#       destinations, i.e. the destinations which names has not been
#       silenced through the -silent option.  These destinations are
#       ordered according to the index, i.e. the order that they had
#       in the description (file).
#
# Arguments:
#       None.
#
# Results:
#       Ordered list of active destinations, can be empty.
#
# Side Effects:
#       None.
proc ::biot::dst::actives {} {
    set sactives {}
    foreach d [info vars [namespace current]::destination:*:*] {
        upvar \#0 $d DST
        
        # Decide if destination should be silent, i.e. not triggered
        set silent 0
        foreach ptn ${vars::-silent} {
            if { [string match $ptn $DST(-name)] } {
                set silent 1
            }
        }
        
        if { $silent } {
            ::utils::debug 3 "Destination $DST(-name) is silenced"
        } else {
            lappend sactives [list $d $DST(index)]
        }
    }
    
    set actives {}
    foreach dinfo [lsort -integer -index 1 $sactives] {
        lappend actives [lindex $dinfo 0]
    }
    
    return $actives
}


# ::biot::dst::reconnect -- descr
#
#      descr
#
# Arguments:
#      d        descr
#
# Results:
#      None.
#
# Side Effects:
#      None.
proc ::biot::dst::reconnect { d } {
    upvar \#0 $d DST
    
    if { $DST(_reconn) ne "" } {
        after cancel $DST(_reconn)
        set DST(_reconn) ""
    }
    
    if { ${vars::-flush} < 0 } {
        set when idle
    } else {
        set when ${vars::-flush}
    }
    set DST(_reconn) [after $when ::biot::dst::connect $d]
}


# ::biot::dst::connect -- Open connection to destination
#
#       Open connection to destination. This extracts the hints out of
#       the scheme and resolve the URL using environment variables and
#       variables global to the program.  Different methods are then
#       triggered depending on the main switch (HTTP, websocket, exec,
#       file, etc.), as registered by the plugins.
#
# Arguments:
#	d	Identifier of the destination
#
# Results:
#       None.
#
# Side Effects:
#       Will open necessary socket, etc.
proc ::biot::dst::connect { d } {
    upvar \#0 $d DST
    set DST(_reconn) ""
    set uri [[namespace parent]::common::resolve $DST(-url) $DST(fname)]
    set hints [::biot::uri::hints $uri hstpath]
    
    foreach p [info vars [namespace current]::plugin:*] {
        upvar \#0 $p PIN
        set uri [::biot::uri::reconstruct $hstpath $hints $PIN(match) $PIN(-resolver)]
        if { $uri ne "" } {
            if { $PIN(-connect) ne "" } {
                #netstats capture [list [namespace which Collect] $d] [list "open" "close" "read" "write"]
                {*}$PIN(-connect) $d $uri $hints $DST(-headers)
                #netstats release
            }
        }
    }
}


# ::biot::dst::biot -- Internal interfacing for interpreters
#
#      This procedure will typically be aliased into sub-interpreters (e.g. when
#      implementing destinations as interpreters or when templating using
#      interps) so as to provide them with a generic interface to access
#      internal details of the implementation.
#
# Arguments:
#      d        Identifier of the destination this is being run in
#      cmd      Sub-command to execute
#      args     Arguments to the command
#
# Results:
#      Depends on the command.
#
# Side Effects:
#      Provides commands for interacting with the internal state, including
#      creating/setting variables.
proc ::biot::dst::biot { d cmd args } {
    upvar \#0 $d DST

    switch -- $cmd {
        sources {
            lassign $args ptn
            return [[namespace parent]::src::names $ptn]
        }
        vars {
            lassign $args src ptn
            if { $ptn eq "" } { set ptn "*" }
            if { $src eq "" } { set src "*" }
            
            set vars [list]
            foreach s [[namespace parent]::src::names $src] {
                set vars [concat $vars [[namespace parent]::vrbl::names $s $ptn]]
            }
            return $vars
        }
        new {
            if { [llength $args] < 2 } {
                return -code error "You need at least to specifiy a source and a variable name"
            }
            lassign $args src v dft
            if { [llength [[namespace parent]::vrbl::find $src $v]] } {
                return -code error "Variable $v already exists in source $src"
            }
            set var [[namespace parent]::vrbl::new $v DELEGATED "" $src {*}[lrange $args 3 end]]
            if { [info exists dft] && $dft ne "" } {
                upvar \#0 $var VAR
                set VAR(value) $dft
            }
        }
        origin {
            lassign $args v
            set var [FindVar $v]
            upvar \#0 $var VAR
            upvar \#0 $VAR(-source) SRC
            return $SRC(-name)
        }
        set {
            # This is written in a way so that it provides backwards
            # compatibility with older scripts. We prefer having setval optional
            # arguments as dash-led options placed at the end of the argument
            # list, but still are able to cover these without dashes when the
            # programmer cares to provide the name of the source. Most of the
            # job is done by setval which accepts both styles of programming.
            # This let older scripts where these extra arguments to setval did
            # not exist continue working unmodified.
            set idx [lsearch -glob $args "-*"]
            if { $idx >= 2 } {
                set opts [lrange $args $idx end]
                set args [lrange $args 0 [expr {$idx-1}]]
            }
            if { [llength $args] >= 3 } {
                lassign $args src k val
                set opts [lrange $args 3 end]
            } else {
                lassign $args k val
                set src *
            }
            set var [FindVar $k $src]
            return [eval [linsert $opts 0 [namespace parent]::vrbl::setval $var $val]
        }
        get {
            if { [llength $args] >= 2 } {
                lassign $args src k
            } else {
                lassign $args k
                set src *
            }
            set var [FindVar $k $src]
            upvar \#0 $var VAR
            return $VAR(value)
        }
        "when" -
        "where" {
            lassign $args vname
            set v [FindVar $vname]
            if { $v ne "" } {
                upvar \#0 $v VAR
                if { [llength $args] >= 2 } {
                    set val [lindex $args 1]
                    set VAR($cmd) $val
                } else {
                    return $VAR($cmd)
                }
            }
        }
        log -
        debug {
            lassign $args lvl msg
            ::utils::debug $lvl $msg
        }
    }
    
}


# ::biot::dst::trimmer -- Trim leading/ending spaces
#
#       Recognises trimleft, trimright and trim as hints and trim data
#       that will be sent from spaces that would be at (respectively)
#       the left, the right or both ends.
#
# Arguments:
#	data	Data (string) to trim
#	hints	Hints from URL
#
# Results:
#       Trimmed (or not) incoming data
#
# Side Effects:
#       None.
proc ::biot::dst::trimmer { data hints } {
    if {[lsearch -glob -nocase $hints "trimleft*"] >= 0 } {
        set data [string trimleft $data]
    }
    if {[lsearch -glob -nocase $hints "trimright*"] >= 0 } {
        set data [string trimright $data]
    }
    if {[lsearch -glob -nocase $hints "trim"] >= 0 } {
        set data [string trim $data]
    }
    
    return $data
}



####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::dst::FindVar -- Find var
#
#       Find a source, this is a wrapper and is implemented so as to minimise
#       code in the biot procedure.
#
# Arguments:
#      v        Name of variable to look for
#      src      Pattern for selecting which source to look in.
#
# Results:
#      Identifier of the variable or an error.
#
# Side Effects:
#       None.
proc ::biot::dst::FindVar { v {src "*"} } {
    if { $v eq "" } {
        return -code error "No variable name specified!"
    }
    foreach s [[namespace parent]::src::names $src] {
        set vars [[namespace parent]::vrbl::find $s $v]
        if { [llength $vars] } {
            return [lindex $vars 0]
        }
    }
    return -code error "Cannot find variable $v"
}


# ::biot::dst::Plugins -- Load plugins
#
#       Load the plugins found in the (unresolved) paths found in the
#       -plugdirs module option.
#
# Arguments:
#	force	Set to 1 to force reloading the plugins.
#
# Results:
#       List of plugins identifiers.
#
# Side Effects:
#       None.
proc ::biot::dst::Plugins { {force 0} } {
    # Load the plugins
    set plugins [::biot::common::plugins \
            ${vars::-plugdirs} \
            [list libdir $vars::libdir] \
            [namespace current]::plugin:* \
            "destination" \
            $force]
    
    # Propagate the module defaults to the relevant plugins
    PropagateDefaults
}


# ::biot::dst::PropagateDefaults -- Defaults inheritance
#
#       Arrange so that the plugins that have registered for it will
#       receive the list of defaults that were specified to this
#       module.  This allows inheriting main configuration parameters
#       to all plugins.
#
# Arguments:
#       None.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::dst::PropagateDefaults {} {
    foreach p [info vars [namespace current]::plugin:*] {
        upvar \#0 $p PIN
        if { $PIN(-defaults) ne "" } {
            {*}$PIN(-defaults) \
                    -timeout ${vars::-timeout} \
                    -flush ${vars::-flush}
        }
    }
}

# ::biot::dst::Payload -- Compute payload out of current variable mapping
#
#       Compute the data payload to be sent to the destination using
#       the current list of variables that were changed and a snapshot
#       of the variable space at the time of the decision.  The
#       payload is the result of the templating of the original file
#       associated to the destination.  The template is sent the
#       following variables: __changes__ will contain the list of
#       changes, __vars__ the list of known variables, __sources__ the
#       list of sources and the list of ALL the variables together
#       with their values at the time of the changed detection.
#
# Arguments:
#	d	Identifier of the destination
#	mapper	Snapshot of all variables at the time of change
#	changes	List of variables that were changed.
#
# Results:
#       Return the templated payload.
#
# Side Effects:
#       None.
proc ::biot::dst::Payload { d mapper changes } {
    upvar \#0 $d DST
    
    if { [string index $DST(-template) 0] eq "@" } {
        if { $DST(templater) eq "" } {
            set DST(templater) [::toclbox::templater::new]
            set spec [string trim [string range $DST(-template) 1 end]]
            set uri [[namespace parent]::common::resolve \
                    [lindex $spec 0] \
                    $DST(fname)]
            set tpl [[namespace parent]::uri::content $uri]
            ::toclbox::templater::alias $DST(templater) biot [namespace current]::biot $d
            ::toclbox::templater::parse $DST(templater) $tpl
            ::toclbox::templater::config $DST(templater) \
                    -sourceCmd [list [namespace parent]::common::tplcache $uri]
            set DST(_vars) [lrange $spec 1 end]
        }
        #::toclbox::templater::reset $DST(templater)
        ::toclbox::templater::setvar $DST(templater) __changes__ $changes
        ::toclbox::templater::setvar $DST(templater) __vars__ [[namespace parent]::vrbl::names]
        ::toclbox::templater::setvar $DST(templater) __sources__ [[namespace parent]::src::names]
        ::toclbox::templater::setvar $DST(templater) __dst__ $DST(-name)
        foreach v $DST(_vars) {
            set v [string trim $v %]
            ::utils::debug 5 \
                    "Passing local/environment variable $v to template" dst
            ::toclbox::templater::setvar $DST(templater) $v \
                    [[namespace parent]::common::resolve %$v% $DST(fname)]
        }
        foreach {k v} $mapper {
            ::toclbox::templater::setvar $DST(templater) [string trim $k "%"] $v
        }
        set payload [::toclbox::templater::render $DST(templater)]
    } else {
        set payload [string map $mapper $DST(-template)]
    }
    
    return $payload
}


# ::biot::dst::Collect -- descr
#
#      descr
#
# Arguments:
#      d         descr
#      direction descr
#      sock      descr
#      len       descr
#
# Results:
#      None.
#
# Side Effects:
#      None.
proc ::biot::dst::Collect { d direction sock { len -1 } } {
    upvar \#0 $d DST
    
    switch -- $direction {
        "read" {
            incr DST(rx) $len
        }
        "write" {
            incr DST(tx) $len
        }
        default {}
    }
}


#### XXX: Not used and wrong, replaced by a string map on the whole list of
#### headers.
proc ::biot::dst::Headers { d mapper changes } {
    upvar \#0 $d DST
    
    set headers [dict create]
    parray DST
    dict for {k v} $DST(-headers) {
        dict set headers $k [string map $mapper $v]
    }
    return $headers
}


# One time loading of plugins.  This is protected so we won't load
# them several times even if we'd call this several times.
::biot::dst::Plugins

package provide biot::dst 0.1



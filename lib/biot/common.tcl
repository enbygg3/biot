##################
## Module Name     --  common
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      Contains common code to all sub-packages of the biot
##      implementation.
##
## Commands Exported:
##      All commands starting with a lowercase.
##################

package require syslog
package require utils
package require biot::uri
package require toclbox

namespace eval ::biot::common {
    namespace eval vars {
	variable -env           {};    # Internal environment
	variable -log           ""
	variable -syslog_port   514;   # Default syslog port
	variable -sources       *.tcl; # Pattern matching source files

	variable log            ""
    }
    namespace export {[a-z]*}
}

# ::biot::common::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#       args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::common::defaults { fname {args {}}} {
    set old ${vars::-env}
    if { $fname ne "" } {
        foreach {k v} $args {
            set vars::CENV($k) $fname
        }
    }
    set dfts [::utils::mset [namespace current]::vars $args -]
    if { ${vars::-env} ne $old } {
        catch {unset vars::GENV}
    }
    return $dfts
}


# ::biot::common::psplit -- Protected split
#
#       This is a re-implementation of the split command, but also
#       supporting protection of characters.  Characters preceeded by
#       the protecting char will not be considered as split
#       separators.
#
# Arguments:
#	str	Strint to split
#	seps	Separtors to use for split
#	protect	Protecting character to avoid splitting on next char.
#
# Results:
#       The list of separated tokens
#
# Side Effects:
#       None.
proc ::biot::common::psplit { str seps {protect "\\"}} {
    set out [list]
    set prev ""
    set current ""
    foreach c [split $str ""] {
	if { [string first $c $seps] >= 0 } {
	    if { $prev eq $protect } {
		set current [string range $current 0 end-1]
		append current $c
	    } else {
		lappend out $current
		set current ""
	    }
	    set prev ""
	} else {
	    append current $c
	    set prev $c
	}
    }
    
    if { $current ne "" } {
	lappend out $current
    }

    return $out
}


# ::biot::common::sconnect -- Syslog connect
#
#       Connect to a remote syslog server, accomodating some
#       well-known hints.  The first "path" after the server and port
#       specification in the URL is used as the name of the
#       application for syslog.  When no hostname is specified, the
#       connection will be to the local host using a UNIX domain
#       socket.
#
# Arguments:
#	hints	List of hints: tcp for TCP connection
#	hstpath	URL specification for (remote) server
#	appnm_	Pointer to application name
#
# Results:
#       Return identifier of syslog connection
#
# Side Effects:
#       Connect to (remote) syslog server.
proc ::biot::common::sconnect { hints hstpath { appnm_ "" } } {
    if { $appnm_ ne "" } {
	upvar $appnm_ appname
    }

    # Look for a slash in what was after the scheme.  Slash at
    # starts introduces the path to a UNIX domain socket,
    # otherwise we understand this as a host:port construct
    # (with a possible (but not used) path).
    set slash [string first "/" $hstpath]
    if { $slash == 0 } {
	# Domain sockets.
	set hstport "";   # Empty
    } elseif { $slash > 0 } {
	# Extract host port part and ignore path
	set hstport [string range $hstpath 0 [expr {$slash-1}]]
	set appname [string range $hstpath [expr {$slash+1}] end]
    } else {
	set hstport $hstpath
    }
    # We have a host and port specification, extract them
    if { $hstport ne "" } {
	foreach {hst port} [split $hstport ":"] break
	if { $port eq "" } { set port ${vars::-syslog_port} }
    } else {
	set hst ""
    }
    
    # Open the syslog connection, make sure we follow the
    # hints.
    set cx ""
    if { [lsearch -nocase $hints "tcp"] >= 0 } {
	set cx [::syslog::connect -host $hst -port $port -tcp]
    } elseif { $hst eq "" } {
	set cx [::syslog::connect -domain $hstport]
    } else {
	set cx [::syslog::connect -host $hst -port $port -udp]
    }

    return $cx
}


# ::biot::common::tplcache -- Source entry points for templates
#
#       This procedure is formed so as to be registered as the
#       callback for template inlining.  It uses a local cache to
#       avoid reading (remote) content many times.  Mainly, this is an
#       indirection into biot::uri::content, which performs the main
#       bulk of the work.
#
# Arguments:
#	cx_f	Path to context file name, i.e. the location of template file
#	t	Identifier of context object.
#	fname	Name of "file" which content is to return (we support URIs!)
#
# Results:
#       Return the content of the resolved URIs, i.e. taking into
#       account environment variables, but also supporting reading
#       from remote etcd and http servers.
#
# Side Effects:
#       None.
proc ::biot::common::tplcache { cx_f t fname } {
    variable vars::templates

    set uri [::utils::resolve $fname [environment $cx_f]]
    if { ![info exists vars::templates($uri)] } {
	::utils::debug 4 "Getting content of $uri for template inlining"
	set vars::templates($uri) [::biot::uri::content $uri]
    }
    return $vars::templates($uri)
}


# ::biot::common::plugins -- Source plugins
#
#       Source plugins from a number of directories.  These plugins
#       should register themselves and fully-qualified variables
#       matching the pattern passed as a parameter should be created
#       for each plugin created.
#
# Arguments:
#	dirs	List of directories (%-surrounded strings allowed)
#	resvars	List of variables to add to resolution
#	ptn	Pattern of FQ-variable names to detect presence of plugins
#	type	A plugin type, just for nice output
#	force	Boolean, set to 1 to force reloading of plugins
#
# Results:
#       Return the list of plugins that were loaded, which supposes
#       that variables matching the pattern are created when these
#       plugins register themselves.
#
# Side Effects:
#       Source plugin files in the *main* interpreter, no
#       security-measures implemented.
proc ::biot::common::plugins { dirs resvars ptn {type ""} {force 0} } {
    # Force? Remove existing representations of the plugins (no
    # "unload" command implemented here, this is rough).
    if { $force } {
	foreach p [info vars $ptn] {
	    unset $p
	}
    }

    # Force or no plugins existing, loop through the directories,
    # resolving those paths and loading in all files matching the
    # plugin pattern.
    if { [llength [info vars $ptn]] == 0 || $force } {
	foreach plugdir $dirs {
	    set pdir [::utils::resolve $plugdir $resvars]
	    ::utils::debug INFO "Checking directory $pdir for $type plugins"
	    set plugins [glob -nocomplain -directory $pdir -- ${vars::-sources}]
	    foreach fname $plugins {
		::utils::debug 4 "Dynamically loading $type plugin at $fname"
		if { [catch {source $fname} res] } {
		    ::utils::debug 3 "Could not load $type plugin at $fname: $res"
		} else {
		    ::utils::debug DEBUG "Plugin $fname loaded successfully: $res"
		}
	    }
	}
    }

    return [info vars $ptn]
}


# ::biot::common::environment -- Resolve path
#
#       This procedure returns a resolved copy of the local
#       environment.  The local environment is formed of a number of
#       program-global variables that can be used in all path
#       resolutions onwards.  These variables are defined using the
#       -env option, and they can contain references to one another
#       and to (shell) environement variables.
#
# Arguments:
#	fpath	Additional contextual path, leading to more auto-variables.
#
# Results:
#       Return a list of the local environment variables and their
#       resolved values.
#
# Side Effects:
#       None.
proc ::biot::common::environment { { fpath "" } } {
    # We cache the environment in the GENV variable.  But since the
    # procedure is called very early in the initialisation phase of
    # the program, we can't resolved all values containing a leading
    # @-sign at once.  So the code block below tries to cover all
    # cases by allowing to resolve several times.
    set resolved 0
    if { [info exists vars::GENV] } {
	set resolved 1
	foreach {k v} [array get vars::GENV] {
	    if { [string index $v 0] eq "@" } {
		set resolved 0
		::utils::debug 5 "Environment still not resolved entirely"
		break
	    }
	}
    } else {
	array set vars::GENV {}
    }

    if { !$resolved } {
	# Resolve global environment.  Should only happen the first
	# time since this can be time consuming as it might involve
	# reading from files and remote resources.
	set env [inline ${vars::-env} \
		     2 \
		     "environment" \
		     fname \
		     [list] \
		     ""\
		     [list "" ""]]
	foreach {k v} $env {
	    set rval [::utils::resolve $v [array get vars::GENV]]
	    # Resolve reference to URLs or recognised remote data
	    # space, arrange to still be able to have an @-sign as the
	    # first character of the value (rare cases!).
	    if { [string range $rval 0 1] eq "\\@" } {
		set rval @[string range $rval 2 end]
	    } elseif { [string index $rval 0] eq "@" } {
                if { [info exists vars::CENV(-env)] } {
                    set rval [::biot::uri::content [string range $rval 1 end] $vars::CENV(-env)]
                } else {
                    set rval [::biot::uri::content [string range $rval 1 end]]
                }
	    }
	    set vars::GENV($k) $rval
	}
    }

    # Copy global environement into local array and amend with
    # variables that would be extracted from the path.
    array set LENV [array get vars::GENV]
    if { $fpath eq "" && [info exists vars::CENV(-env)] } {
        set fpath $vars::CENV(-env)
    }
    if { $fpath ne "" } {
	set slash [string last "/" $fpath]
	if { $slash > 0 } {
	    set dir [string range $fpath 0 [expr {$slash-1}]]
	} else {
	    set dir .
	}
	set LENV(dir) [::utils::resolve $dir [array get LENV]]
	set LENV(dirname) [::utils::resolve $dir [array get LENV]]
	set LENV(fname) [::utils::resolve [file tail $fpath] [array get LENV]]
	set LENV(rootname) [::utils::resolve \
				[file rootname [file tail $fpath]] \
				[array get LENV]]
    }
    ::utils::debug 5 "Local environment is: [array get LENV]"
    return [array get LENV]
}


# ::biot::common::inline -- Resolve inlining and templating commands
#
#       This procedure takes a string command and will perform two
#       "resolving" actions depending on the first character of the
#       string.  If the first character is a "@", the remaining of the
#       command is understood to be the path to a file which content
#       is to be read and returned.  This path can contain % signs,
#       which will be resolved using environment variables and also
#       local program environment variables (see ::environment
#       procedure).  If the first character is a "<", the remaining of
#       the command is understood as to be a templating command
#       composed of a list pointing at a template file and a number of
#       arguments.  The first element of the list is the path to the
#       template (can be %-resolved, as described above).  If the
#       argument that follows starts with an @-sign, it is understood
#       as pointing to a file and the content of that file will be
#       read and result in the arguments passed to the template (one
#       argument per valid line).  Otherwise, the remaining of the
#       list consists of the arguments that are passed to the
#       template.  However they were acquired, the arguments are
#       passed to the template in a variable called args.  Additional
#       variables can be passed to the template using the third
#       argument to this procedure, which has, then, to be an
#       even-long list.  The result of the template is parsed using
#       the same list-parsing routing as when inlining with @.
#
# Arguments:
#	prm	Command to resolve
#	sz	Unit size of the list to read when inlining with @
#	name	Name of list file to read, when inlining with @
#	tplvars	List of additional variables to pass to template, if any.
#	module	Name of verbosity module, if relevant.
#	enviro	Explicit environment to use, empty for good defaults (program's)
#
# Results:
#       The result of parsing and/or inlining, or the original data
#       passed in prm if no special leading character was detected.
#
# Side Effects:
#       Will read a number of files..
proc ::biot::common::inline {
		   prm
		   sz
		   name
		   { fname_p "" }
		   { tplvars {} }
		   { module "" }
		   { enviro {} }} {
    # Access where to place the main contextual filename, if any.
    if { $fname_p ne "" } {
	upvar $fname_p fname
    }
    set fname ""

    # Read indirection file if necessary, otherwise list of source
    # URLs is content of -sources option.
    if { [string index $prm 0] eq "@" } {
	if { [llength $enviro] == 0 } {
	    set fname [::utils::resolve \
			   [string trim [string range $prm 1 end]] \
			   [environment]]
	} else {
	    set fname [::utils::resolve \
			   [string trim [string range $prm 1 end]] \
			   $enviro]
	}
	set prm [::utils::lscan [::biot::uri::content $fname] $sz $name]
    }
    if { [string index $prm 0] eq "<" } {
	set pipe [string trim [string range $prm 1 end]]
	::utils::debug 3 "Getting $name definition from templating $pipe" \
	    $module
	set vtpl [::toclbox::templater::new]
	if { [llength $enviro] == 0 } {
	    set fname [::utils::resolve [lindex $pipe 0] [environment]]
	} else {
	    set fname [::utils::resolve [lindex $pipe 0] $enviro]
	}
	::toclbox::templater::parse $vtpl [::biot::uri::content $fname]
	#::toclbox::templater::link $vtpl $fname
	set args [lrange $pipe 1 end]
	if { [string index $args 0] eq "@" } {
	    if { [llength $enviro] == 0 } {
		set lfname [::utils::resolve \
				[string range $args 1 end] \
				[environment $fname]]
	    } else {
		set lfname [::utils::resolve \
				[string range $args 1 end] \
				$enviro]
	    }
	    set args [::utils::lscan [::biot::uri::content $lfname] 1 "$name arguments"]
	    ::utils::debug 3 "Extracted $args as arguments to template\
                              from $lfname" $module
	}
	::toclbox::templater::setvar $vtpl args $args
	foreach { var val } $tplvars {
	    ::toclbox::templater::setvar $vtpl $var $val
	}

	set prm [::utils::lclean [split [::toclbox::templater::render $vtpl] "\n"]]
    }

    return $prm
}


# ::biot::common::outlog -- Log output.
#
#       This should be registered as a callback to the debug output
#       facility.  Depending on the leading scheme, this will either
#       send the debug data to a syslog daemon or to a file.
#
# Arguments:
#	lvl	Debug level of message (an integer)
#	pkg	Package where it occured
#	line	Debug line
#
# Results:
#       None.
#
# Side Effects:
#       Send data to syslog daemon and/or append/overwrite file
proc ::biot::common::outlog { lvl pkg line } {
    # Silencing all modules to avoid infinite recursion
    set verbosity [::utils::verbosity * 0]

    # Resolve log URI and extract hints
    set loguri [resolve ${vars::-log}]
    set hints [::biot::uri::hints $loguri hstpath scheme]

    if { [lsearch -nocase $hints "syslog"] >= 0 } {
	# First time, try opening connection to syslog daemon, either
	# remote, either domain socket.
	if { $vars::log eq "" } {
	    # Open connection
	    set cx [sconnect $hints $hstpath appname]

	    # We have a connection, create an application context and
	    # bind it to the connection.
	    if { $cx ne "" } {
		if { $appname ne "" } {
		    set vars::log [::syslog::application -name $appname]
		} else {
		    set vars::log [::syslog::application]
		}
		::utils::debug 4 "Sending log information to $hstpath,\
                                  identifying as $appname"
		::syslog::bind $vars::log $cx
	    }
	}

	# We already have opened a working connection to a (remote?)
	# daemon and bound it to an application context, then send
	# away log information.
	if { $vars::log ne "" } {
	    ::syslog::log $vars::log $line -facility 1 -severity $lvl
	}
    } else {
	if { $vars::log eq "" } {
	    # Open the log file, if we had use a file:// scheme, we
	    # can then isolate a hint and from there look if we had
	    # written overwrite as part of the hints.  If so, then the
	    # log file will be overwritten.
	    set mode a
	    if { [lsearch -nocase $hints "file"] >= 0 } {
		if { [lsearch -nocase $hints "overwrite"] >= 0 } {
		    set mode w
		}
	    }
	    set vars::log [open $hstpath $mode]
	    fconfigure $vars::log -buffering line
	}
	
	if { $vars::log ne "" } {
	    puts $vars::log [::utils::dbgfmt $lvl $pkg $line]
	}
    }
    
    # Restoring verbosity
    eval ::utils::verbosity $verbosity
}


# ::biot::common::resolve -- Environment-aware resolution
#
#       This procedure performs the same job as ::utils::resolve
#       (i.e. replacing occurences of %-surrounded strings with their
#       values), but also using the variables defined in the argument
#       and also in the context passed as an argument.
#
# Arguments:
#	path	Path to resolve
#	context	Additional context to pass to environment procedure
#
# Results:
#       Returned the resolved path.
#
# Side Effects:
#       None.
proc ::biot::common::resolve { path {context ""}} {
    return [::utils::resolve $path [environment $context]]
}

proc ::biot::common::tweaks { data } {
    set data [inline $data \
		  2 \
		  "tweaks" \
		  fname \
		  [list] \
		  "tweak"]

    set tweaks [list]
    foreach { tweak value } $data {
        foreach {ns var} [split $tweak .] break
        if { $ns ne "" && $var ne "" } {
            set tgt [namespace parent [namespace current]]::[string trimleft $ns :]
            if { [namespace exists $tgt] } {
                if { [catch {toclbox defaults $tgt $var $value} res] == 0 } {
                    lappend tweaks $tweak
                } else {
                    ::utils::debug WARN "Could not apply tweak $tweak: $res"
                }
            } else {
                ::utils::debug WARN "Cannot find sub-namespace $ns"
            }
        }
    }

    return $tweaks
}

proc ::biot::common::human { val {max 20} } {
    return [toclbox human $val $max]

    set ellipsis ""
    if { $max > 0 } {
	if { [string length $val] > $max } {
	    set val [string range $val 0 [expr {$max-1}]]
	    set ellipsis "..."
	}
    }
    
    if { [string is print $val] } {
	return ${val}${ellipsis}
    } else {
	set hex "(binary) "
	foreach c [split $val ""] {
	    binary scan $c c1 num
	    set num [expr { $num & 0xff }]
	    append hex [format "%.2X " $num]
	}
	return [string trim $hex]${ellipsis}
    }
    
    return ""
}

package provide biot::common 0.1

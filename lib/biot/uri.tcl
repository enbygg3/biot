##################
## Module Name     --  uri
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      Contains routines to extract information from URIs, including
##      recognition of "hints" in the sheme, i.e. keywords separated
##      from the main scheme using the '+' sign.
##
## Commands Exported:
##      All commands starting with a lowercase!
##################


namespace eval ::biot::uri {
    namespace eval vars {
        variable -timeout    20;  # Timeouts for HTTP calls, in seconds.
        variable -separator  "+"; # Scheme hint separator
        variable etcd "";         # Version number of etcd package
    }
    namespace export {[a-z]*}
}

package require http
package require utils
if { [catch {package require etcd} ver] == 0 } {
    set ::biot::uri::vars::etcd $ver
} else {
    ::utils::debug NOTICE "etcd support disabled, can't find package: $ver"
}

# ::biot::uri::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::uri::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


# ::uri:hints -- Isolate hints from URI schemes
#
#       This procedure recognises the use of hints in the scheme of a
#       URI.  The scheme is considered to be the part PRIOR to :// in
#       the URI and hints are separated from the main (and
#       standardised scheme) using the + sign.
#
# Arguments:
#	uri	 Hinted URI
#	hstpath_ "Pointer" to variable that will contain after ://
#	scheme_  "Pointer" to variable that will contain original scheme
#
# Results:
#       Return the list of hints that were found, so passing a URI
#       that starts with http+ssl:// will return a list composed of
#       the strings http and ssl.
#
# Side Effects:
#       None.
proc ::biot::uri::hints {uri {hstpath_ ""} {scheme_ ""}} {
    if { $hstpath_ ne "" } {
        upvar $hstpath_ hstpath
    }
    if { $scheme_ ne "" } {
        upvar $scheme_ scheme
    }
    
    set sep [string first "://" $uri]
    if { $sep < 0 } {
        set hstpath $uri
        set scheme ""
        return {}
    }
    
    incr sep -1
    set scheme [string range $uri 0 $sep]
    set hstpath [string replace $uri 0 [expr {$sep+3}] ""]
    
    set hints [split $scheme ${vars::-separator}]
    return $hints
}


# ::biot::uri::reconstruct -- Back to canonical form
#
#       This procedure will reconstruct a URI so that the scheme is
#       modified whenever some hints are found.  The main goal is to
#       transform back hints coming from http+tls to https (which is
#       what the http module would expect).  This procedure exists
#       more for the sake of completion.
#
# Arguments:
#	htspath	Everything behind ://
#	hints	The list of original hints
#	match	Match for one of the hints
#	resolv	List of scheme and hint, will force to scheme when hint found.
#
# Results:
#       Resolver URI.
#
# Side Effects:
#       None.
proc ::biot::uri::reconstruct {hstpath hints match resolv } {
    if { [lsearch -glob -nocase $hints $match] >= 0 } {
        # In case we have resolution hints available (e.g. tls to
        # force secure schemes), use those.
        if { [llength $resolv] >= 0 } {
            # Default to the scheme found in the URL
            set scheme [lindex $hints [lsearch -glob -nocase $hints $match]]
            
            # But override to another scheme in case we had matching hints
            foreach {d_scheme d_hints} $resolv {
                foreach h $d_hints {
                    if { [lsearch -glob -nocase $hints $h] >= 0 } {
                        set scheme $d_scheme
                    }
                }
            }
            
            set uri "${scheme}://${hstpath}"
        } else {
            set uri $hstpath
        }
        return $uri
    }
    return ""
}


# ::biot::uri::auth -- Extract authorisation info from URIs
#
#       This extracts the authorisation information from URIs and
#       return it, if present.  The procedure is also able to pass a
#       cleaned URL that can be used for, for example, log output.
#
# Arguments:
#	url	URL to extract information from
#	curl_	"Pointer" to variable that will contain the cleaned URI
#
# Results:
#       Return the authorisation information from the URL.
#
# Side Effects:
#       None.
proc ::biot::uri::auth { url { curl_ "" } } {
    if { $curl_ ne "" } { upvar $curl_ curl }
    
    set scheme_end [string first "://" $url]
    if { $scheme_end < 0 } {
        return -code error "$url is not a valid URL"
    }
    incr scheme_end 3
    
    set at_sign [string first "@" $url $scheme_end]
    if { $at_sign >= 0 } {
        set auth_info [string range $url $scheme_end [expr {$at_sign-1}]]
        set curl [string range $url 0 [expr {$scheme_end-1}]]
        append curl [string range $url [expr {$at_sign+1}] end]
        return $auth_info
    } else {
        set curl $url
        return ""
    }
}


proc ::biot::uri::hostport { url { default 80 } { separator ":" } } {
    set len [string length $url]
    set schema_end [string first "://" $url]
    incr schema_end 3
    
    set at_sign [string first "@" $url $schema_end]
    if { $at_sign >= 0 } {
        incr at_sign
        set start $at_sign
    } else {
        set start $schema_end
    }
    
    set slash [string first "/" $url $start]
    if { $slash >= 0 } {
        set hostport [string trimright [string range $url $start $slash] "/"]
    } else {
        set hostport [string range $url $start end]
    }
    
    foreach {host port} [split $hostport ":"] break
    if { $port eq "" } {
        set port $default
    }
    return ${host}${separator}${port}
}


# ::biot::uri::content -- Get data at URI.
#
#       Get the data behind the URI passed as a parameter.  This
#       recognises HTTP, etcd and file (default) as the schemes.  We
#       would perhaps gain at extend this to a plugin system so we can
#       also recognise other key-value stores (consul, etc.).  Note
#       that anything that do not start with http or etcd will be
#       considered a file path (a path that will be resolved
#       (extraction of %-surrounded environment variables).
#
# Arguments:
#	uri	URI (or file path, the default)
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::uri::content { uri {fname ""}} {
    switch -regexp -- $uri {
        "^http" {
            ::utils::debug 4 "Getting data from $uri"
            set cmd [list ::http::geturl $uri]
            if { ${vars::-timeout} > 0 } {
                lappend cmd -timeout [expr {int(${vars::-timeout}*1000)}]
            }
            set token [eval $cmd]
            set ncode [::http::ncode $token]
            if { $ncode >= 200 && $ncode < 300 } {
                set data [::http::data $token]
                ::http::cleanup $token
                return $data
            } else {
                ::http::cleanup $token
                return -code error "Cannot get URL at $uri"
            }
        }
        "^etcd" {
            if { $vars::etcd ne "" } {
                set hints [::uri:hints $uri hstpath]
                set slash [string first "/" $hstpath]
                set hstprt [string range $hstpath 0 [expr {$slash-1}]]
                foreach {hst prt} [split $hstprt ":"] break
                if { $prt eq "" } {
                    set etcd [lindex [::etcd::find -host $hst] 0]
                    if { $etcd eq "" } {
                        set etcd [::etcd::new -host $hst]
                    }
                } else {
                    set etcd [lindex [::etcd::find -host $hst -port $prt] 0]
                    if { $etcd eq "" } {
                        set etcd [::etcd::new -host $hst -port $prt]
                    }
                }
                set path [string range $hstpath [expr {$slash+1}] end]
                ::utils::debug 4 "Getting data from etcd key $path at $hst"
                return [::etcd::read $etcd $path]
            } else {
                return -code error "No support for etcd!"
            }
        }
        default {
            ::utils::debug 4 "Getting data from local file $uri"
            set uri [[namespace parent]::common::resolve $uri $fname]
            set fd [open $uri]
            set data [read $fd]
            close $fd
            return $data
        }
    }
    return "";  # Never reached
}

package provide biot::uri 0.1


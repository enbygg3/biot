package require utils

namespace eval ::biot::rest {
    namespace eval vars {
    }
    namespace export {[a-z]*}
}
    
# ::biot::rest::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::rest::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


proc ::biot::rest::listen { port { www "" } } {
    if { [catch {package require minihttpd} ver] } {
        ::utils::debug 2 "Cannot find mini HTTPd implementation from til!"
        return -1
    }
    
    set srv [::minihttpd::new $www $port]
    if { $srv < 0 } {
	::utils::debug 3 "Cannot start web server on port $port"
	return $srv
    }

    ::minihttpd::handler $srv /var/* [namespace current]::Get "text/plain"
    ::minihttpd::handler $srv /key/* [namespace current]::Get "text/plain"
    ::minihttpd::handler $srv /ops/get/* [namespace current]::Get "text/plain"

    ::minihttpd::handler $srv /vars/* [list [namespace current]::MGet 2] "text/plain"
    ::minihttpd::handler $srv /keys/* [list [namespace current]::MGet 2] "text/plain"
    ::minihttpd::handler $srv /ops/mget/* [list [namespace current]::MGet 3] "text/plain"

    ::minihttpd::handler $srv /ops/set/* [list [namespace current]::Set 3] "text/plain"
    ::minihttpd::handler $srv /ops/put/* [list [namespace current]::Set 3] "text/plain"

    ::minihttpd::handler $srv /ops/mset/* [list [namespace current]::Set 3] "text/plain"
    ::minihttpd::handler $srv /ops/mput/* [list [namespace current]::Set 3] "text/plain"

    ::utils::debug NOTICE "Started web server on port $port, root: $www"

    return $srv
}

####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


proc ::biot::rest::Get { prt sock url qry } {
    set vname [lindex [split $url /] end]
    foreach v [::biot::vrbl::find] {
	upvar \#0 $v VAR
	if { $VAR(-name) eq $vname } {
	    return $VAR(value)
	}
    }
    return ""
}

proc ::biot::rest::MGet { start prt sock url qry } {
    set result ""
    set patterns [lrange [split $url /] $start end]
    if { [llength $patterns] == 0 } {
	set patterns [list "*"]
    }
    foreach vptn $patterns {
	foreach v [::biot::vrbl::find] {
	    upvar \#0 $v VAR
	    if { [string match $vptn $VAR(-name)] } {
		append result $VAR(-name)\t$VAR(value)\n
	    }
	}
    }
    return [string trim $result]
}

proc ::biot::rest:SetVar { vname val } {
    foreach v [::biot::vrbl::find] {
	upvar \#0 $v VAR
	if { $VAR(-name) eq $vname } {
	    return [$v setval $val]
	}
    }
    return ""
    
}

proc ::biot::rest::Set { start prt sock url qry } {
    set args [lrange [split $url /] $start end]
    if { [llength $args] == 1 } {
	array set Q $qry
	if { [info exists Q(value)] } {
	    return [SetVar [lindex $args 0] $Q(value)]
	}
    } elseif { [llength $args] > 0 } {
	foreach { vname val } $args break
	return [SetVar $vname $val]
    }
}



package provide biot::rest 0.1

##################
## Module Name     --  vrbl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      This module will create "objects" (Tk-style) convention for
##      all variables (known or auto-generated).  Each variable is
##      represented by a unique namespace variable and this variable
##      is also the name of a command which can be used to perform
##      operations on the variable.  The content of variables should
##      be updated through the update procedure, which is internally
##      what pushing data to a source does.
##
## Commands Exported:
##      All procedures starting with a lowercase are exported.  Each
##      variable will be associated to a command for tk-style API.
##      Also this creates a biotv (v as in variable) global command to
##      ease calling from the outside.
##################

package require json
package require biot::json

namespace eval ::biot::vrbl {
    namespace eval vars {
        variable -flush      100
        variable -maxiter    200
        variable -separators ".:,"
        variable -history    10
        variable -update     ""
        
        variable flush       ""
        variable touched     {}
        variable types       {RX* JSON* TPL* EXPR* XPR* DATE DELEGATED}
        variable interps     [dict create]
        variable pulse       ""
    }
    
    namespace export {[a-z]*}
    namespace ensemble create -command ::biotv
}


# IMPLEMENTATION NOTE:
#
# Internally, we use the special name "-" as the source name that will
# represent orphane variables.


# ::biot::vrbl::defaults -- Set/get default parameters
#
#       This procedure takes an even list of keys and values used to
#       set the values of the options supported by the library.  The
#       list of options is composed of all variables starting with a
#       dash in the vars sub-namespace.  In the list, the dash
#       preceding the key is optional.
#
# Arguments:
#        args        List of key and values to set for module options.
#
# Results:
#       A dictionary with all current keys and their values
#
# Side Effects:
#       None.
proc ::biot::vrbl::defaults { {args {}}} {
    return [::utils::mset [namespace current]::vars $args -]
}


# ::biot::src::new -- Create a new variable
#
#       Create and initialise a new variable.
#
# Arguments:
#	name	Name of variable (mandatory)
#	type	Type of variable (mandatory), will trigger extractions algo.
#	fname	Name of file from which variable was read from (empty=ok)
#	src	Name of source to which variable is bound (empty=ok)
#	args	Dash-led options and values for the variable parameters.
#
# Results:
#       Return an identifier for the array representing the variable in
#       this namespace (also a command).
#
# Side Effects:
#       None.
proc ::biot::vrbl:::new { name type fname src args } {
    if { $src eq "" } { set src "-" }
    if { $src ne "-" } {
        set src [[namespace parent]::src::names $src]
    }
    if { $src ne "" } {
        set vname [::utils::identifier [namespace current]::var:$src:$name:]
        upvar \#0 $vname VAR
        
        set VAR(id) $vname
        set VAR(fname) $fname
        
        # Bind to source, maybe should we be more intelligent about
        # source names and recognise source identifiers properly
        # (otherwise we *could* bind to the wrong source if there are
        # two sources with the same name).
        set VAR(-source) ""
        if { $src ne "-" } {
            set VAR(-source) [[namespace parent]::src::find $src];  # Only one, really
        }
        set VAR(-name) $name
        set VAR(-type) $type
        foreach {k v} [list extract "" trigger ""] {
            ::utils::getopt args -$k VAR(-$k) $v
        }
        
        set VAR(templater) ""
        set VAR(generated) {}
        if { ${vars::-history} > 0 } {
            set VAR(history) {}
        }
        set VAR(value) "";      # Current value
        set VAR(where) "";      # Wherefrom the value was acquired at the source,
                                # e.g. a specific MQTT topic, etc.
        set VAR(when) "";       # When was the variable modified according to the source.
        #set VAR(flushed);      # Will contain value when variable is to be flushed
        # further to destinations.
        
        # Create object command, arrange for only the relevant
        # procedures to implement the relevant "methods".
        interp alias {} $vname {} \
                ::utils::rdispatch $vname [namespace current] [list setval update]
        
        return $vname
    }
    return ""
}


# ::biot::vrbl::multi -- Read variable description
#
#       Read descriptions of variables to extract from the sources (or
#       acquire directly from program arguments) and create global
#       representations for these variables within the program.  This
#       will also, if relevant, create a number of orphane variables
#       that will be updated as time goes by.  Each of these variables
#       has one-letter name and contain details about the current date
#       and time.
#
# Arguments:
#       vspec	A list multiple of 3 or the reference to a file containing the
#               variables representation.
#	auto	The period at which to automatically update auto vars.
#
# Results:
#       Return the list of the object containing the newly created
#       variables.
#
# Side Effects:
#       Will read definitions and templates if necessary.
proc ::biot::vrbl::multi { vspec {auto -1} } {
    set vspec [::biot::common::inline $vspec \
            {3 4} \
            "variables" \
            fname \
            [list "sources" [[namespace parent]::src::names]] \
            "vars"]
    
    # Backward-compatibility detection: for lists that contain a number of
    # elements that is both a multiple of 4 and 3, try detecting if they are
    # from the old school of the new school.
    set len [llength $vspec]
    if { $len%3==0 } {
        set 3to4 1;     # This is a 3-ary list.
        if { $len%4==0 } {
            for {set i 0} {$i<$len} {incr i 3} {
                # Get the second argument of the sublist and check if this one
                # of the valid variable types. If it isn't a valid type, then
                # the list is of the new 4-ary type of variable specification.
                set t [lindex $vspec [expr {$i+1}]]
                if { ! [ValidType $t] } {
                    set 3to4 0;
                    break
                }
            }
        }
        
        # If we should, convert the variable specification list to a 4-ary list
        # by inserting an empty trigger, which will be understood as the default
        # only-if-changed trigger, which is completely backwards compatible.
        if { $3to4 } {
            ::utils::debug INFO "Converting old-style variable specifications to new specs. with triggers"
            set nvspec [list]
            foreach { spec type extract } $vspec {
                lappend nvspec $spec $type $extract ""
            }
            set vspec $nvspec
        }
    }
    
    set vars {}
    foreach { spec type extract trigger } $vspec {
        # Split the spec to get name of source, name of variable and
        # possibly initial value of var.
        foreach {src name init} [[namespace parent]::common::psplit $spec ${vars::-separators}] break
        
        set mapper [list]
        if { $src ne "" } {
            set mapper [list %source% $src %src% $src %var% $name %varname% $name]
        }
        lappend mapper %name% $name
        
        set vname [::biot::vrbl::new $name $type $fname $src \
                        -extract [string map $mapper $extract] \
                        -trigger $trigger]
        if { $vname ne "" } {
            upvar \#0 $vname VAR
            set VAR(value) $init;    # Do not trigger history mechanisms
            lappend vars $vname
            ::utils::debug INFO "Created variable $VAR(-name)"
        } else {
            ::utils::debug 1 "$src is not a known source!"
        }
    }
    
    # Create automatic time-based variables, if relevant.  These will
    # be of the (internal) type DATE.  Also, in that case, make sure
    # that we will have a pulse that will update those variables as
    # time passes.
    set autov {}
    if { $auto > 0 } {
        set autov [Auto DATE [list a A b B C d e g G H I \
                j J k l m M N p P s S \
                u U V w W x y Y z Z]]
        if { $vars::pulse eq "" } {
            set vars::pulse [after idle [list [namespace current]::Pulse $auto]]
        }
    }
    
    return [concat $vars $autov]
}


# ::biot::vrbl::find -- Return matching variables
#
#       Return the list of variable identifiers that match the source
#       and variable name patterns passed as parameters.
#
# Arguments:
#	src	Pattern to match against source names
#	vars	Pattern to match against var names within the selected sources
#
# Results:
#       Return the list of variable identifiers, empty when no match
#
# Side Effects:
#       None.
proc ::biot::vrbl::find { {src *} {vnames *} } {
    set vars {}
    foreach s [concat [[namespace parent]::src::find] "-"] {
        if { $s eq "-" } {
            set srcname $s
        } else {
            upvar \#0 $s SRC
            set srcname $SRC(-name)
        }
        
        if { [string match $src $srcname] } {
            foreach v [info vars [namespace current]::var:$srcname:*:*] {
                upvar \#0 $v VAR
                if { [string match $vnames $VAR(-name)] } {
                    lappend vars $v
                }
            }
        }
    }
    
    return $vars
}


# ::biot::vrbl::names -- Return matching names of variables.
#
#       Return the name of the variables which names match the pattern
#       passed as an argument, under the sources which names match the
#       pattern passed as an argument.
#
# Arguments:
#	src	Pattern to match against source names
#	vars	Pattern to match against var names within the selected sources
#
# Results:
#       Return the list of matching variable names, empty when no match.
#
# Side Effects:
#       None.
proc ::biot::vrbl::names { {src *} { vars * } } {
    set names {}
    foreach v [find $src $vars] {
        upvar \#0 $v VAR
        lappend names $VAR(-name)
    }
    return $names
}


# ::biot::vrbl::update -- Update a variable
#
#       Given data that was dynamically acquired from a source, this
#       procedure will perform variable extraction and mark the
#       variables which data has changed for output.  All relevant
#       destinations will be triggered once the respit period has
#       expired.
#
# Arguments:
#	src	Name of source (and not identifier!)
#	indata	Clean source data to extract variables from
#
# Results:
#       Return the list of variables that were changed as part of the
#       extraction process.
#
# Side Effects:
#       Performs extraction and triggers destinations.
proc ::biot::vrbl::update { v indata args} {
    # For all variables that have been associated to that source,
    # perform textual analysis on the source (see details below).
    # This depends on the -type of the variable extraction.  The
    # type is composed of a main string, followed by an argument,
    # separated by one of the program-wide separators (. is best
    # here).
    upvar \#0 $v VAR
    
    set prev $VAR(value)
    switch -glob -nocase -- $VAR(-type) {
        "RX*" {
            eval [linsert $args 0 UpdateRX $v $indata]
        }
        "JSON*" {
            eval [linsert $args 0 UpdateJSON $v $indata]
        }
        "TPL*" {
            upvar \#0 $VAR(-source) SRC
            set vars [list value $indata \
                    source $SRC(-name) \
                    varname $VAR(-name)]
            eval [linsert $args 0 UpdateTPL $v $vars ""]
        }
    }
    
    # Schedule a change on the variable if its value has changed.
    return [Trigger $v on]
}


# ::biot::vrbl::setval -- Set variable to value
#
#       Set the value of a variable to the one passed as a parameter
#       and mark the variable as updated so as to trigger relevant
#       destinations.  There is no extraction here.
#
# Arguments:
#	v	Identifier of variable
#	value	Value to be given to variable.
#
# Results:
#       Return the previous value of the variable.
#
# Side Effects:
#       None.
proc ::biot::vrbl::setval { v value args} {
    upvar \#0 $v VAR
    eval [linsert $args 0 Store $v $value]
    Trigger $v on
    return $VAR(flushed)
}


####################################################################
#
# Procedures below are internal to the implementation, they shouldn't
# be changed unless you wish to help...
#
####################################################################


# ::biot::vrbl::ValidType -- Validate variable type
#
#       Validate the type of the variable against the official types that can
#       appear in file specifications and/or on the command line.
#
# Arguments:
#	type	Variable type.
#
# Results:
#       1 if type is known and valid, 0 otherwise. Matching is made glob-style
#       and without taking care to the casing.
#
# Side Effects:
#       None.
proc ::biot::vrbl::ValidType { type } {
    foreach ptn $vars::types {
        if { [string match -nocase $ptn $type] } {
            return 1
        }
    }
    return 0
}


proc ::biot::vrbl::Store { v value args } {
    upvar \#0 $v VAR
    
    set VAR(value) $value
    foreach {k v} $args {
        set k [string tolower [string trimleft $k -]]
        switch -- $k {
            "when" -
            "where" {
                set VAR($k) $v
            }
            default {
                ::utils::debug ERROR "$k is not allowed!"
            }
        }
    }
}


proc ::biot::vrbl::Trigger { v {schedule off}} {
    upvar \#0 $v VAR
    
    set trigger 0
    if { [info exists VAR(flushed)] } {
        # Request variable trigger (default to whenever the old flushed value is
        # different from the current one.)
        if { $VAR(-trigger) eq "" } {
            set trigger [expr {$VAR(flushed) ne $VAR(value)}]
        } elseif { [string index $VAR(-trigger) 0] eq "<" } {
            set cspec [string trim [string range $VAR(-trigger) 1 end]]
            set prg [lindex $cspec 0]
            if { [string equal -nocase [file extension $prg] ".tcl"] } {
                # Specific treatment for Tcl scripts, since we are able to call
                # procedures in them, etc. We'll create interpreters to run them in, see
                # below for details.
                set arobas [string first @ $prg]
                if { $arobas >= 0 } {
                    # When we have an @ in the string, we understand this as a procedure
                    # to call within an interpreter (source from the file after the @
                    # sign). In this case, we keep the interpreter from one call to the
                    # next, so it can store whichever state it needs to.
                    set uri [string trim [string range $prg [expr {$arobas+1}] end]]
                    set uri [[namespace parent]::common::resolve $uri $VAR(fname)]
                    
                    # Create the interpreter once, we'll reuse it
                    if { ![dict exists $vars::interps $uri] } {
                        set script [[namespace parent]::uri::content $uri]
                        set itrp [interp create]
                        $itrp eval [list set ::argv0 $uri]
                        $itrp eval [list set ::argv [lrange $cspec 1 end]]
                        $itrp eval [list set ::argc [llength [lrange $cspec 1 end]]]
                        if { [catch {$itrp eval $script} res] != 0 } {
                            ::utils::debug ERROR "Cannot load script from $uri: $res"
                            interp delete $itrp
                            return 0
                        } else {
                            ::utils::debug INFO "Loaded script from $uri for triggers on $VAR(-name)"
                        }
                        dict set vars::interps $uri $itrp
                    }
                    
                    # We have an interpreter. Split what is before the @ sign along the
                    # possible ! sign (to be able to give parameters to the procedure,
                    # if necessary) and use the return code of the procedure.
                    if { [dict exists $vars::interps $uri] } {
                        set itrp [dict get $vars::interps $uri]
                        set call [concat [split [string range $cspec 0 [expr {$arobas-1}]] !] \
                                [list $VAR(flushed) $VAR(value)]]
                        ::utils::debug INFO "Executing $call from $uri to decide upon trigger"
                        try {
                            set trigger [$itrp eval $call]
                        } on error {res} {
                            ::utils::debug WARN "Cannot execute $call in interp: $res"
                        }
                    }
                } else {
                    # When the specification is only a tcl script, we'll source it in a
                    # new interpreter on and on. We use the return value of the last
                    # command as the status.
                    set itrp [interp create]
                    set arglist [concat [lrange $cspec 1 end] [list $VAR(flushed) $VAR(value)]]
                    set uri [[namespace parent]::common::resolve $prg $VAR(fname)]
                    ::utils::debug INFO "Executing $uri to decide upon trigger"
                    set script [[namespace parent]::uri::content $uri]
                    $itrp eval [list set ::argv0 $uri]
                    $itrp eval [list set ::argv $arglist]
                    $itrp eval [list set ::argc [llength $arglist]]
                    try {
                        set trigger [$itrp eval $script]
                    } on error {res} {
                        ::utils::debug ERROR "Cannot execute Tcl code at $cspec: $res"
                        set trigger 0
                    } finally {
                        interp delete $itrp
                    }
                }
            } else {
                # Otherwise, we execute the command and use its result to know what to
                # do.
                set cmd [[namespace parent]::common::resolve $cspec $VAR(fname)]
                ::utils::debug INFO "Executing $cmd to decide upon trigger"
                try {
                    set res [exec -ignorestderr -- {*}[concat $cmd [list $VAR(flushed) $VAR(value)]]]
                } trap CHILDSTATUS {res options} {
                    set trigger [lindex [dict get $options -errorcode] 2]
                } on error {res} {
                    ::utils::debug ERROR "Cannot execute external command: $res"
                }
            }
        } else {
            set xpr [string map [list %old% $VAR(flushed) %new% $VAR(value)] $VAR(-trigger)]
            if { [catch {expr $xpr} res] == 0 } {
                set trigger $res
            } else {
                ::utils::debug ERROR "Cannot understand $xpr: $res"
            }
        }
        
        # If we should trigger, save the old value in the history (if relevant)
        # and remember current value until next time we are requested to trigger
        # (or not).
        if { $trigger } {
            if { ${vars::-history} > 0 } {
                set VAR(history) [linsert $VAR(history) 0 $VAR(flushed) [clock milliseconds]]
                set VAR(history) [lrange $VAR(history) 0 [expr {(${vars::-history}*2)-1}]]
            }
            set VAR(flushed) $VAR(value)
            ::utils::debug 5 "Value of $VAR(-name) is now '[::biot::common::human $VAR(value)]'"
        }
    } else {
        # First time, we store current value of variable and request a flush
        set VAR(flushed) $VAR(value)
        set trigger 1
        ::utils::debug 5 "Value of $VAR(-name) is now '[::biot::common::human $VAR(value)]'"
    }
    
    # Schedule triggering of destinations based on new variable value.
    if { $trigger && $schedule } {
        Schedule $v
    }
    
    return $trigger;  # Catch all
}


# ::biot::vrbl::Auto -- Initialise auto variables
#
#       Create and initialise a series of automatic variables which
#       will be orphane.
#
# Arguments:
#	type	Type of the variables to create
#	vnames	List of variable names to create
#
# Results:
#       Return the list of the object containing the newly created
#       variables.
#
# Side Effects:
#       None.
proc ::biot::vrbl::Auto { type vnames } {
    set vars {}
    
    foreach v $vnames {
        set vname [new $v $type "" ""]
        if { $vname ne "" } {
            lappend vars $vname
        }
    }
    
    return $vars
}


# ::biot::vrbl::Snapshot -- Create a variable state snapshot
#
#       Creates a snapshot of variables and their values, to be used
#       later within program flow.  Only variables which names match
#       the source and variable name patterns passed as an argument
#       are considered.  The header and footer are prepended and
#       appended to the name of the variable in the snapshot.
#
# Arguments:
#	src	Pattern to match against source names
#	vars	Pattern to match against var names within the selected sources
#	header	Characters to prepend to variable name
#	footer	Characters to append to variable name
#
# Results:
#       Return an even-long list of variable names and their values,
#       names being properly prepended and appended.
#
# Side Effects:
#       None.
proc ::biot::vrbl::Snapshot { {src *} {vars *} { header % } { footer % } { history ":history"} } {
    # Arrange for sources to contain the names of the existing sources
    # that match the incoming filter.
    set sources [concat [[namespace parent]::src::names] "-"]
    
    # For all sources and for all variables matching the incoming
    # filter, output their values in the snapshot.
    set snapshot {}
    foreach src $sources {
        foreach v [info vars [namespace current]::var:$src:$vars:*] {
            upvar \#0 $v VAR
            set nm $header
            append nm $VAR(-name)
            append nm $footer
            lappend snapshot $nm $VAR(value)
            if { ${vars::-history} > 0 } {
                set nm $header
                append nm $VAR(-name) $history
                append nm $footer
                lappend snapshot $nm $VAR(history)
            }
        }
    }
    
    return $snapshot
}


proc ::biot::vrbl::Orphane { snapshot } {
    set modified {}
    
    set now [clock seconds]
    foreach v [info vars [namespace current]::var:-:*:*] {
        upvar \#0 $v VAR
        
        set prev $VAR(value)
        switch -glob -nocase -- $VAR(-type) {
            "TPL*" {
                UpdateTPL $v $snapshot "%"
            }
            
            "EXPR*" -
            "XPR*" {
                set xpr [string map $snapshot $VAR(-extract)]
                if { [catch {expr $xpr} res] == 0 } {
                    Store $v $res
                } else {
                    ::utils::debug 3 "Could not evaluate expression: $res"
                }
            }
            "DATE" {
                # This is the internal type of the date-based orphane
                # variables.
                if { [catch {clock format $now \
                            -format "%$VAR(-name)"} res] == 0} {
                    Store $v $res
                } else {
                    ::utils::debug 3 "Could not convert\
                                      date format for $VAR(-name): $res"
                }
            }
        }
        
        if { [Trigger $v] } {
            lappend modified $v
        }
    }
    
    return $modified
}


# ::biot::vrbl::Stabilise -- Stabilise values of all orphane variables
#
#       Iterate a number of times to compute a stable state for all
#       the orphane variables.  Iterating allows to cope with
#       interdependencies between variables.  The maximum number of
#       iterations can be negative, in which case this procedure will
#       stabilise until stability has been reached.  However, this
#       isn't recommended since it could lead to an infinite loop.
#
# Arguments:
#	iterations	Max number of iterations
#	snapshot_p	"Pointer" to variable snapshot
#
# Results:
#       Return the list of orphane variables that have changed
#
# Side Effects:
#       Can iterate indefinitely if called with a negative maximum
#       number of iterations in cases where variables are
#       inter-dependent.
proc ::biot::vrbl::Stabilise { iterations {snapshot_p ""} } {
    if { $snapshot_p ne "" } {
        upvar $snapshot_p snapshot
    }
    
    ::utils::debug 4 "Computing values of orphane variables"
    set orphane [list]
    set snapshot [Snapshot];  # Initial complete state
    
    # Iterate until two snapshots of all variables are the same and
    # cumulate the identifier of the orphane variables that have been
    # touched.  Iteration testing is on PURPOSE != 0 since this will
    # cover infinite iterations whenever the maximum was negative
    # (which isn't recommended!).
    while { $iterations != 0 } {
        # Get a list of all orphane variables modified by the current
        # value snaphot.  If none, we are done!
        set modified [Orphane $snapshot]
        if { [llength $modified] <= 0 } {
            break
        }
        
        # Arrange for the list orphane to contain all orphane
        # variables that have been concerned by the changes so far.
        set orphane [lsort -unique [concat $orphane $modified]]
        
        # Make sure we end as soon as nothing has changed.
        set newsnapshot [Snapshot]
        if { $newsnapshot == $snapshot } {
            break
        } else {
            set snapshot $newsnapshot
        }
        incr iterations -1;   # Count iterations
    }
    
    # Warning when max number of iterations has been reached.
    if { $iterations == 0 } {
        ::utils::debug 2 "Max number of iterations ${vars::-maxiter} reached,\
                          data inconsistent!"
    }
    
    return $orphane
}


# ::biot::vrbl::Flush -- Flush touched variables to their destinations
#
#       This procedure capture the list of variables that have been
#       modified since last time, selects the destinations that should
#       be notified of these changes and trigger those destinations
#       with the change.
#
# Arguments:
#       None.
#
# Results:
#       None.
#
# Side Effects:
#       Will trigger one or several destinations
proc ::biot::vrbl::Flush {} {
    # Capture list of variables to test for output and reinitialise at
    # once so we can start gathering changes again.
    set vars [lsort -unique $vars::touched]
    set vars::touched {}
    set vars::flush ""
    
    # Now that we have a set of variables that have been changed
    # because something happened at a source, compute all the orphane
    # variables, i.e. the variables that are not connected to a
    # source.  Since there might be inter-dependencies, we only
    # compute a finite number of time and give up if the data space
    # does not stabilise.
    set orphane [Stabilise ${vars::-maxiter} snapshot]
    set timestamp [clock milliseconds]
    set epoch [expr {$timestamp/1000}]
    
    # For all active destinations, try telling them about the change
    # that has occured.  This time the change is composed of all the
    # variables that were touched as a result of the source update,
    # but also of all the orphane variables that changed.
    set vars [concat $vars $orphane]
    ::utils::debug 4 "Flushing out content of [llength $vars] variable(s)"
    foreach d [[namespace parent]::dst::actives] {
        upvar \#0 $d DST
        
        foreach ptn $DST(-filters) {
            # Compute signature of current change, i.e. the list of
            # the names of the variables that had changed.
            set change {}
            foreach v $vars {
                upvar \#0 $v VAR
                if { [string match $ptn $VAR(-name)] } {
                    lappend change $VAR(-name)
                }
            }
            
            # And try mediating about this change, one variable at a
            # time (to be able to write %var% and %value% in payload
            # and templates).
            if { [llength $change] > 0 } {
                ::utils::debug 4 "Triggering $DST(-name) with [llength $change]\
                                  variable change(s)"
                foreach v $vars {
                    upvar \#0 $v VAR
                    $d push \
                            [concat [list %var% $VAR(-name) \
                            %value% $VAR(value) \
                            %__epoch__% $epoch \
                            %__timestamp__% $timestamp] \
                            $snapshot] \
                            $change
                }
            }
        }
    }

    if { ${vars::-update} ne "" } {
        ::utils::debug DEBUG "Forcing update"
        if { [string match "idle*" ${vars::-update}] } {
            ::update idletasks
        } else {
            ::update
        }
    }
}


# ::biot::vrbl::Schedule -- Schedule variable change
#
#       Schedule notification change for a variable. As we want to
#       capture as many changes as possible within one "go", some
#       slight respit is given before this change is notified to
#       relevant destinations, which allows other change notifications
#       to occur before we properly notify the destinations of the
#       changes.
#
# Arguments:
#	v	Identifier of the variable that has been changed.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::vrbl::Schedule { { v "" } } {
    # Cancel existing flush, if any.
    if { $vars::flush ne "" } {
        after cancel $vars::flush
        set vars::flush ""
    }
    
    # Schedule a new flush in a short while
    set vars::flush [after ${vars::-flush} [namespace current]::Flush]
    
    # Accumulate the identifier of the variable to the list of the
    # ones that were touched within this time frame.  An empty name is
    # allowed since we use this for the pulse of automatically updated
    # time-based variables.
    if { $v ne "" } {
        lappend vars::touched $v
    }
}


proc ::biot::vrbl::UpdateRX { v indata args } {
    upvar \#0 $v VAR
    
    if { [string index $VAR(-name) 0] eq "<" } {
        # If the variable name starts with the < sign, this means that
        # the variable will spontaneously generate variables based on
        # the template contained in its name
        
        # Make sure we have the path to the template.
        set fname [string range $VAR(-name) 1 end]
        
        # Extract the "period" of the regular expression by trying it
        # once only, by period we really mean the number of groups
        # within the regular expression.
        set nb_grps [llength [regexp -inline -- $VAR(-extract) $indata]]
        ::utils::debug 5 "Spontaneous variable has a regexp period of\
                          $nb_grps"
        if { $nb_grps > 0 } {
            # Perform extraction accross the complete data.
            set res [regexp -all -inline -- $VAR(-extract) $indata]
            set len [llength $res]
            # Check the type and extract which sub-group from the
            # regular expression we should extract the name of the
            # variable to be created from.  None means the result of
            # the regular expression.
            foreach {t g} [[namespace parent]::common::psplit $VAR(-type) ${vars::-separators}] break
            if { $g eq "" } {
                set g 0
            }
            
            # So now loops through the result of the regular
            # expression at the given period.  Jump to the specified
            # sub-group, which will be the name of the variable that
            # we are attempting to create.
            set VAR(value) [list];  # Value of templating var will be
            # list of spontaneous vars that it
            # has created.
            upvar \#0 $VAR(-source) SRC
            for {set i 0} {$i < $len} {incr i $nb_grps} {
                # Jump to name of spontaneous variable in list.
                set vname [lindex $res [expr {$i+$g}]]
                lappend VAR(value) $vname
                
                # If this name hasn't led to the generation of a
                # variable with that name yet: apply the template to
                # create some variables.
                set sv ""
                foreach {gvname gv} $VAR(generated) {
                    if { $gvname eq $vname } {
                        set sv $gv
                    }
                }
                
                if { $sv eq "" } {
                    ::utils::debug 5 "Considering $vname for spontaneous\
                                      creation"
                    
                    set uri [[namespace parent]::common::resolve \
                            $fname \
                            $VAR(fname)]
                    if { $VAR(templater) eq "" } {
                        set VAR(templater) [::toclbox::templater::new]
                        ::toclbox::templater::alias $VAR(templater) ::biot [namespace parent]::src::biot $VAR(-source)
                        set tpl [[namespace parent]::uri::content $uri]
                        ::toclbox::templater::parse $VAR(templater) $tpl
                        ::toclbox::templater::config $VAR(templater) \
                                -sourceCmd [list ::biot::common::tplcache $uri]
                    }
                    ::toclbox::templater::setvar $VAR(templater) src $SRC(-name)
                    ::toclbox::templater::setvar $VAR(templater) name $vname
                    ::toclbox::templater::setvar $VAR(templater) var $vname
                    # Run the template and create one (or several!)
                    # variables using its result.  Usually, these
                    # variables would be bound to the same source
                    # (thus using the variable src set in the
                    # template), but not necessarily.
                    set output [::toclbox::templater::render $VAR(templater)]
                    if { [string trim $output] eq "" } {
                        ::utils::debug DEBUG "Empty templater, skipping creation of $vname"
                    } else {
                        ::utils::debug 5 "Using generated template: '$output'\
                                          for creating $vname"
                        foreach nv [multi \
                                [::utils::lclean [split $output "\n"]]] {
                            upvar \#0 $nv NVAR
                            ::utils::debug 4 "Spontaneously created new variable\
                                              $NVAR(-name) (-> $nv), using\
                                              template from original variable\
                                              $VAR(-name)"
                            set NVAR(fname) $uri
                            # Remember name of variable that we created
                            # from the template (this is because we might
                            # have modified the name as part of the
                            # template).
                            lappend VAR(generated) $NVAR(-name) $nv
                            
                            # Update each newly created variable with the
                            # data that we've just been given, so we find
                            # its initial value.
                            eval [linsert $args 0 update $nv $indata]
                        }
                    }
                } else {
                    ::utils::debug DEBUG "Variable $vname has already been\
                                          created as $sv"
                }
            }
        }
    } else {
        # Regular expression matching considers -extract as being a
        # regular expression to apply on the text.  This expression
        # can contain sub-groups, isolated through parenthesis.  If
        # -type contained an argument, this should be an integer
        # identifying the subgroup to extract from the match.  If not,
        # the first group (at index 0) is returned.
        set res [regexp -inline -- $VAR(-extract) $indata]
        if { $res ne "" } {
            foreach {t g} [[namespace parent]::common::psplit $VAR(-type) ${vars::-separators}] break
            if { $g eq "" } {
                eval [linsert $args 0 Store $v [lindex $res 0]]
                return 1
            } else {
                eval [linsert $args 0 Store $v [lindex $res $g]]
                return 1
            }
        }
    }
    
    return 0
}


proc ::biot::vrbl::UpdateJSON { v indata args } {
    upvar \#0 $v VAR
    
    upvar \#0 $VAR(-source) SRC
    if { ![info exists SRC(cache:indata)] } {
        set SRC(cache:indata) ""
    }
    
    # Since JSON parsing takes time, we cache the dictionary to
    # minimise the number of conversions.
    if { $SRC(cache:indata) ne $indata } {
        set SRC(cache:indata) $indata
        if { [catch {::json::json2dict $indata} dta] } {
            ::utils::debug 2 \
                    "JSON parsing error '$dta' when analysing: $indata"
            return 0
        }
        set SRC(cache:dta) $dta
    }
    
    # The JSON matcher converts the indata from JSON to a dictionary.
    # It then selects (parts) of that dictionary using -extract (see
    # below).  If the type had an argument, this would be the field of
    # the JSON object that was selected using -extract (which allows
    # to extract a different field that the one(s) used for
    # selection).  If not, the result of the extraction is used.
    # Selection using extract uses an X-Path like syntax, () to select
    # items in arrays, / to separate in the tree, fields and array
    # indices can use glob-style patterns.  Following the X-Path
    # selection, there can be an operator (==, etc.) and a value.  See
    # ::biot::json::match for more details.
    
    # Replace %-sugar strings in the selection using some dynamic
    # context, i.e. the name of the variable, the type and the
    # source.
    set mapper {}
    foreach k [list "source" "name" "type"] {
        lappend mapper %${k}% $VAR(-$k)
    }
    set extraction [string map $mapper $VAR(-extract)]
    
    # Now considers the extraction, i.e. the resolved -extract to
    # be a list.  Either it is a list with 3 items, and therefor
    # contains an operator, either a one item list.
    set output {}
    if { [llength $extraction] > 1 } {
        # Note this only allows one extraction out of the
        # data. However, we might, instead want to support things
        # like (*)/alias == "hel*" AND (*)/value > 45
        foreach {select op value} $extraction break
            ::utils::debug 5 "Testing the result of $select against $value\
                              with operator $op"
        set res [json match $SRC(cache:dta) $select $op $value]
        ::utils::debug 5 "Extracted $res out of JSON tree"
        
        # Once we have (plenty of?) "objects" out of the JSON
        # tree, either we keep the value of what we had extracted,
        # i.e. we've performed some conditional extraction, or we
        # replace the selected field by another. We might want to
        # do better...
        foreach {t id} [[namespace parent]::common::psplit $VAR(-type) ${vars::-separators}] break
        if { $id eq "" } {
            # No replacement needed, just pick up all the values,
            # concatenate and this will be the value.
            foreach js $res {
                foreach {jt jv} [json select $SRC(cache:dta) $js] break
                lappend output $jv
            }
        } else {
            # Go on doing the replacement (maybe do we want a
            # better syntax/fix for this, e.g. building upon the
            # sed ideas.
            foreach js $res {
                set slash [string last "/" $js]
                set xpr [string range $js 0 $slash]
                append xpr $id
                ::utils::debug 5 "Extracting $xpr out of JSON tree to get\
                                  final value" vars
                
                foreach {jt jv} [json select $SRC(cache:dta) $xpr] break
                lappend output $jv
            }
        }
    } else {
        set res [json select $SRC(cache:dta) $extraction]
        
        foreach {t id} [[namespace parent]::common::psplit $VAR(-type) ${vars::-separators}] break
        if { $id eq "" } {
            foreach {jt jv} $res {
                lappend output $jv
            }
        } else {
            foreach {js jv} $res {
                set slash [string last "/" $js]
                set xpr [string range $js 0 $slash]
                append xpr $id
                ::utils::debug 5 "Extracting $xpr out of JSON tree to get\
                                  final value"
                
                foreach {jt jv} [json select $SRC(cache:dta) $xpr] break
                lappend output $jv
            }
        }
    }

    eval [linsert $args 0 Store $v $output]
    return 1
}


proc ::biot::vrbl::UpdateTPL { v vars trim args } {
    upvar \#0 $v VAR
    
    # If the type is TPL, then the -extract contains the path to a
    # template to apply on the data.  The template is passed the name
    # of the variable and what it returns will be the value of the
    # variable.  The path is resolved using environment variables and
    # variables global to the program.
    if { $VAR(templater) eq "" } {
        set VAR(templater) [::toclbox::templater::new]
        ::toclbox::templater::alias $VAR(templater) ::biot [namespace parent]::src::biot $VAR(-source)
        set uri [[namespace parent]::common::resolve \
                [lindex $VAR(-extract) 0] $VAR(fname)]
        set tpl [[namespace parent]::uri::content $uri]
        ::toclbox::templater::parse $VAR(templater) $tpl
        ::toclbox::templater::config $VAR(templater) \
                -sourceCmd [list [namespace parent]::common::tplcache $uri]
        
        foreach ve [lrange $VAR(-extract) 1 end] {
            set ve [string trim $vr %]
            ::utils::debug 5 "Passing local/env variable $ve to variable template at $uri"
            ::toclbox::templater::setvar $VAR(templater) $ve \
                    [[namespace parent]::common::resolve %$ve% $VAR(fname)]
        }
    }
    
    foreach { key val } $vars {
        if { $trim eq "" } {
            ::toclbox::templater::setvar $VAR(templater) $key $val
        } else {
            ::toclbox::templater::setvar $VAR(templater) [string trim $key $trim] $val
        }
    }
    set output [::toclbox::templater::render $VAR(templater)]
    
    eval [linsert $args 0 Store $v [string trim $output]]
    return 1
}


# ::biot::vrbl::Pulse -- Variable update pulse
#
#       This installs a pulse that will periodically update orphane
#       variables.  This pulse is typically used to update the auto,
#       date-based variables.
#
# Arguments:
#	period	Period of the pulse
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::biot::vrbl::Pulse {{period 1000}} {
    Schedule;   # Hook in the regular variable update mechanism.
    set vars::pulse [after $period [list [namespace current]::Pulse $period]]
}

package provide biot::vrbl 0.1

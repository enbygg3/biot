##################
## Module Name     --  syslog.tcl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##     This library is a tcl-only implementation of the syslog
##     protocol according to RFC5424 (and to a lesser extent 3164.
##     The library is able to connect to remote (or local) servers on
##     TCP or UDP, and to the local facility using UNIX domain
##     sockets.  For UDP connections to work, the library will require
##     the presence of the TclUDP package.  For UNIX domain
##     connections, the library uses the services of "nc" a binary
##     which is part of most unix distributions.  When sending to
##     local UNIX domain sockets, the library needs to use RFC 3164,
##     having to let aside some of the parameters of messages that are
##     not covered by the obsolete RFC.
##
##################
package require Tcl 8.5;

package require utils


namespace eval ::syslog {
    variable SYSLOG
    if {![info exists SYSLOG] } {
	array set SYSLOG {
	    idGene         0
	    idClamp        10000
	    idFormat       7
	    verbose        5
	    dbgfd          stderr
	    dateLogHeader  "\[%Y%m%d %H%M%S\] "
	    syslogDate     "%Y-%m-%dT%T%z"
	    reserved       {timeQuality origin meta}
	    myHost         ""

	    -version       1
	}

	array set FACILITY {
	    kern          0
	    kernel        0
	    user          1
	    mail          2
	    daemon        3
	    security      4
	    auth          4
	    authorization 4
	    syslog        5
	    lpr           6
	    printer       6
	    news          7
	    uucp          8
	    clock         9
	    authpriv      10
	    ftp           11
	    ntp           12
	    local0        16
	    local1        17
	    local2        18
	    local3        19
	    local4        20
	    local5        21
	    local6        22
	    local7        23
	}

	array set SEVERITY {
	    panic         0
	    emerg         0
	    emergency     0
	    alert         1
	    crit          2
	    critical      2
	    err           3
	    error         3
	    warn          4
	    warning       4
	    notice        5
	    info          6
	    debug         7
	}

	variable version 0.1
	variable libdir [file dirname [file normalize [info script]]]
    }
}


# ::syslog::UpVar -- Find true caller
#
#       Finds how many stack levels there are between the direct
#       caller to this procedure and the true caller of that caller,
#       accounting for indirection procedures aiming at making
#       available some of the local procedures from this namespace to
#       child namespaces.
#
# Arguments:
#	None.
#
# Results:
#       Number of levels to jump up the stack to access variables as
#       if upvar 1 had been used in regular cases.
#
# Side Effects:
#       None.
proc ::syslog::UpVar {} {
    set signature [info level -1]
    for { set caller -1} { $caller>-10 } { incr caller -1} {
	if { [info level $caller] eq $signature } {
	    return [expr {-$caller}]
	}
    }
    return 1
}


# ::syslog::Identifier -- Return a unique identifier
#
#       Generate a well-formated unique identifier to be used, for
#       example, to generate variable names or similar.
#
# Arguments:
#	pfx	String prefix to prepend to id, empty for none
#
# Results:
#       A (possibly prefixed) unique identifier in space and time
#       (almost).
#
# Side Effects:
#       None.
proc ::syslog::Identifier { {pfx "" } } {
    variable SYSLOG
    
    set unique [incr SYSLOG(idGene)]
    ::append unique [expr {[clock clicks -milliseconds] % $SYSLOG(idClamp)}]
    return [format "${pfx}%.$SYSLOG(idFormat)d" $unique]
}


# ::syslog::IsA -- Check variable object type
#
#       Check if the variable which name is passed as an argument is
#       of a given type.  This variable name should have been created
#       by the Instance procedure.
#
# Arguments:
#	varname	Name of variable
#	type	Type of the "object"
#
# Results:
#       Return 1 if the variable exists, 0 otherwise
#
# Side Effects:
#       None.
proc ::syslog::IsA { varname type } {
    variable SYSLOG

    if { [lsearch [info vars [namespace current]::*${type}@*] $varname]<0 } {
	return 0
    }
    return 1
}


# ::syslog::Instance -- Create an "object" varname
#
#       Create an instance of the class which name is passed as an
#       argument, an instance that should be a children of the parent
#       which variable name is also possibly passed as an argument.
#       The name of the instance, a variable name ready for array
#       operations, for example contains the whole inheritance paths.
#
# Arguments:
#	cname	Name of the class
#	parent	Parent object, if any.
#
# Results:
#       Return a fully qualified unique variable name that contains
#       the whole inheritance path and can be used to instanciate
#       unique variables within this namespace.
#
# Side Effects:
#       None.
proc ::syslog::Instance { cname { parent "" } } {
    if { $parent eq "" } {
	return [namespace current]::${cname}@[Identifier]
    } else {
	foreach {p_cl p_id } \
	    [split [string map [list [namespace current]:: ""] $parent] @] break
	return [namespace current]::${p_cl}|${cname}@${p_id}|[Identifier]
    }
}


# ::syslog::application -- Create a syslog application client
#
#       Create an "application" client for the syslog communication
#       protocol.  Applications contains global context about the
#       sender of the events, a context that will not change from one
#       event to the next.  While there will usually by only one such
#       context per program or script, the API opens up for the
#       existence of several applications.  Arguments to this
#       procedure are led by a dash (possibly followed by a value),
#       good defaults are provided.  The procedure accepts the
#       following arguments:
#       -hostname   Local hostname where application is installed,
#                   this will default to the hostname made accessible
#                   to Tcl as soon as a socket connection has been
#                   opened (see connect)
#       -name       Name of the application, defaults to the root
#                   name of the script, without extension.
#       -pid        Process identifier, defaults to the current pid.
#       -enterprise IANA enterprise id, empty by defaults since this
#                   is only necessary when creating structured data.
#
# Arguments:
#	Dash-led options and their values, as described above.
#
# Results:
#       Return a unique identifier for the application, an identifier
#       that will be used throughout the rest of this API.
#
# Side Effects:
#       Errors are generated for all unacceptable parameters according
#       to RFC 5424.
proc ::syslog::application { args } {
    variable SYSLOG

    array set APP {}
    if { [::utils::getopt args -hostname APP(-hostname) "-"] } {
	# Check for allowed contents and characters, IP numbers
	# allowed, etc.
    } else {
	# SYSLOG(myHost) is updated at the first connection to a
	# (remote) server.
	set APP(-hostname) $SYSLOG(myHost)
    }
    if { [string length $APP(-hostname)] > 255 } {
	return -code error "Host name too long (255 chars. max)"
    }

    # Name of application, pick up the rootname of the script as a
    # default (which is probably a good guess).
    ::utils::getopt args -name APP(-name) [file rootname [file tail $::argv0]]
    if { [string length $APP(-name)] > 48 } {
	return -code error "Application name too long (48 chars. max)"
    }
    
    # Process id
    ::utils::getopt args -pid APP(-pid) [pid]
    if { [string length $APP(-pid)] > 128 } {
	return -code error "Process identifier too long (128 chars. max)"
    }

    # Set application enterprise ID.
    set APP(-enterprise) ""
    # Check official IANA Enterprise Id, if any. We only check for
    # correctness, we might be checking against the content of
    # http://www.iana.org/assignments/enterprise-numbers/enterprise-numbers
    if { [::utils::getopt args -enterprise enterpriseId] } {
	foreach char [split $enterpriseId ""] {
	    if { ! [string match \[0-9.\] $char] } {
		return -code error \
		    "$enterpriseId is not an official IANA Enterprise Id"
	    }
	}
	set APP(-enterprise) $enterpriseId
    }
    if { [string length $APP(-enterprise)] > 32 } {
	return -code error "Enterprise identifier too long (32 chars. max)"
    }

    foreach k [list hostname name pid enterprise] {
	set APP(-$k) [string trim $APP(-$k)]
    }

    set app [Instance app]
    upvar \#0 $app A
    set A(self) $app
    set A(connection) ""
    array set A [array get APP]

    # Create the standard structured data types, in case we needed
    # them...
    type $app timeQuality tzKnown isSynced syncAccuracy
    type $app origin ip enterpriseId software!48 swVersion!32
    type $app meta sequenceId sysUpTime language

    return $app
	
}


# ::syslog::delete -- Delete any object from the API
#
#       Delete any object created by its API and its descendants.
#       This will free all memory used by the variables and make their
#       identifier invalid.
#
# Arguments:
#	v	Identifier of object, e.g. returned by application proc.
#
# Results:
#       None.
#
# Side Effects:
#       Frees all memory for variable and descendants, on return name
#       is invalid.
proc ::syslog::delete { v } {
    # Recursively delete children
    foreach {cl id } [split [string map [list [namespace current]:: ""] $v] @] break
    foreach sv [info vars [namespace current]::${cl}|*@${id}|*] {
	# But only pick direct children, since we'll recurse again.
	foreach {s_cl s_id } [split [string map [list [namespace current]:: "" \
						     ${cl}| "" ${id}| ""] $sv] @] break
	if { [string first "|" $s_cl] < 0 } {
	    delete $sv
	}
    }

    # Deletion specific to object types, connection and/or messages.
    if { [IsA $v connection] } {
	upvar \#0 $v CX
	if { $CX(sock) ne "" } {
	    fconfigure $CX(sock) -blocking off;  # For UX sockets, but
						 # of no immediate
						 # harm for the other
						 # sockets.
	    close $CX(sock)
	}
    }

    if { [IsA $v message] } {
	upvar \#0 $v MSG
	foreach e $MSG(-elements) {
	    delete $e
	}
    }
    
    # Remove the whole array and its content from memory, the point of
    # no return.
    catch {unset $v}
}


# ::syslog::type -- Create a type
#
#       Create a type to create structured data entities that will be
#       attached to messages.  A type needs to be bound to an
#       application.  Each argument will be the name of a field, if
#       that argument ends with a ! followed by an integer, this will
#       be understood as the maximum number of characters allowed in
#       the value.
#
# Arguments:
#	a	Identifier of an application, as returned by ::syslog::application
#	name 	name of type within the application
#	args 	List of allowed fields in the type.
#
# Results:
#       Return the identifier of the type, an identifier that should
#       be used to instantiate structured data before associating them
#       to a message later on.
#
# Side Effects:
#       None.
proc ::syslog::type { app name args } {
    variable SYSLOG

    if { [lsearch -exact $SYSLOG(reserved) $name] < 0 } {
	if { ![IsA $app app] } {
	    return -code error "$app does not identify a known app"
	}
	upvar \#0 $app APP
	if { $APP(-enterprise) eq "" } {
	    return -code error \
		"No IANA enterprise ID set in app"
	}
    }

    # Check content of name
    foreach char [list @ = \] \"] {
	if { [string first $char $name] >= 0 } {
	    return -code error "Character '$char' not allowed in type name"
	}
    }
    foreach char [split $name ""] {
	set ascii [scan $char %c]
	if { $ascii <= 32 || $ascii > 127 } {
	    return -code error \
		"ASCII character \#$ascii not allowed in type name"
	}
    }

    # Create an "object" representing the type.
    set t [Instance type $app]
    upvar \#0 $t TYPE
    set TYPE(self) $t
    set TYPE(app) $app
    set TYPE(-name) $name
    set TYPE(-elements) $args

    return $t
}


# ::syslog::EncodeType -- Encode type name to string
#
#       Encodes the name of a type to a string.  This procedure will
#       arrange to combine the name of the type with the enterprise
#       identifier associated with the application.
#
# Arguments:
#	t	Identifier of the type, as returned by ::syslog::type
#
# Results:
#       Return the string representation of the type, ready for
#       inclusion in a message
#
# Side Effects:
#       None.
proc ::syslog::EncodeType { t } {
    variable SYSLOG

    upvar \#0 $t TYPE
    if { [lsearch -exact $SYSLOG(reserved) $TYPE(-name)] < 0 } {
	upvar \#0 $TYPE(app) APP
	set type "$APP(-enterprise)@[string trim $TYPE(-name)]"
    } else {
	set type $TYPE(-name)
    }
    return $type
}


# ::syslog::add -- Add a field/value to an element
#
#       Elements contain an instance of a type, this procedure will
#       add the value of one of the field names registered under the
#       type.  If length restrictions were declared for the type
#       (using the ! construct), these will be checked here and the
#       value clamped as appropriate.
#
# Arguments:
#	e	Identifier of the element, as returned by ::syslog::element
#	param	Name of the field
#	value	Value of the field.
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::syslog::add { e param value } {
    if { ![IsA $e element] } {
	return -code error "$e is not a registered type"
    }
    upvar \#0 $e ELEMENT
    upvar \#0 $ELEMENT(type) TYPE

    foreach p $TYPE(-elements) {
	foreach {name len} [split $p !] break
	if { $name eq $param } {
	    if { $len ne "" } {
		set value [string range $value 0 $len]
	    }
	    lappend ELEMENT(-params) $param $value
	}
    }
}


# ::syslog::element -- Create a structured data element
#
#       Create an instance of a structured data element and return an
#       identifier for the element.  The arguments should be a list of
#       key-value pairs, where the keys are the name of the fields (as
#       declared in ::syslog::type). According to the RFC, there can
#       be several values for the same field name.
#
# Arguments:
#	t	Identifier of the type, as returned by ::syslog::type
#	args	List of field names followed by their values.
#
# Results:
#       Return an identifier for the element.
#
# Side Effects:
#       None.
proc ::syslog::element { t args } {
    # Should we create elements under a message directly, so deleting
    # the message would automatically delete the elements and so that
    # they do not need to be appended with the append procedure???
    if { ![IsA $t type] } {
	return -code error "$t is not a registered type"
    }
    upvar \#0 $t TYPE
    set e [Instance element $t]
    upvar \#0 $e ELEMENT
    set ELEMENT(self) $e
    set ELEMENT(type) $t
    set ELEMENT(-params) {}

    foreach {k v} $args {
	add $e $k $v
    }

    return $e
}


# ::syslog::EncodeElement -- Encode an element
#
#       Encode an element for inclusion in a syslog message. This
#       procedure will perform the necessary escapings according to
#       the specification.
#
# Arguments:
#	e	Identifier of the element, as returned by ::syslog::element
#
# Results:
#       Returned the encoded string
#
# Side Effects:
#       None.
proc ::syslog::EncodeElement { e } {
    upvar \#0 $e ELEMENT
    set str "\[[EncodeType $ELEMENT(type)]"
    foreach {k v} $ELEMENT(-params) {
	::append str " ${k}=\""
	::append str [string map [list "\"" "\\\"" "\]" "\\\]" "\\" "\\\\"] $v]
	::append str "\""
    }
    ::append str "\]"
}


# ::syslog::append -- Append an element to message
#
#       This procedure will append the instance of a structured data
#       element created with ::syslog::element and perhaps
#       ::syslog::add to a message.  At present, there are no ways to
#       remove elements from messages, but this has been deemed
#       unnecessary since messages are typically transient and created
#       on the fly.
#
# Arguments:
#	m	Identifier of the message
#	e	Identifier of the element
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::syslog::append { m e } {
    if { ![IsA $m message] || ![IsA $e element] } {
	return -code error "Invalid arguments"
    }
    upvar \#0 $m MSG
    lappend MSG(-elements) $e
}


# ::syslog::message -- Create a message
#
#       Create a syslog message, at specific priorities and at
#       specific times if necessary.  The procedure always uses good
#       defaults so as to offer the simplest interface for the
#       simplest messages ("notice" messages for the "user" facility).
#       Apart from the application and the text of the message, this
#       procedure takes a number of dash-led options (tk-style), with
#       their values.  Note that for these options, you can either
#       choose to specify the complete priority, or to specify the
#       facility and the severity separately. These options are:
#       -priority   Either an integer, or the facility and severity
#                   separated by a ., e.g. user.notice (the default)
#       -facility   The facility emitting the message, these can be
#                   an integer or the strings contained in the
#                   FACILITY array (see at top of library).
#       -severity   The severity of the message, this can be an
#                   integer or one of the strings contained in the
#                   SEVERITY array (see at top of library).
#       -id         The identifier of the message, defaults to - (none)
#       -timestamp  The number of seconds since 1970, defaults to "now"
#
# Arguments:
#	app	Identifier of application, as returned by ::syslog::application
#	msg	The text of the message
#	args	List of dash-led options and their values, see above.
#
# Results:
#       Return the identifier of the message. It hasn't been sent yet!
#
# Side Effects:
#       None.
proc ::syslog::message { app msg args } {
    variable SYSLOG
    variable FACILITY
    variable SEVERITY

    # Create empty message
    array set MSG {}

    # Get priority, could be a whole integer or facility.severity
    set skip 0
    if { [::utils::getopt args -priority MSG(-priority)] } {
	if { ! [string is integer $MSG(-priority)] } {
	    foreach { MSG(-facility) MSG(-severity) } \
		[split $MSG(-priority) "."] break
	    set skip 1
	} elseif { $MSG(-priority) < 0 || $MSG(-priority) > 191 } {
	    return -code error \
		"-priority must be between 0 and 191"
	}
    }

    if { ! $skip } {
	# Other interface, specify facility and severity separetely.
	::utils::getopt args -facility MSG(-facility) 1;   # Default facility is user
	::utils::getopt args -severity MSG(-severity) 5;   # Default severity is notice
    }

    # Allow facility to be an integer (for the initiated) or a
    # string that can be read (for most of us)
    if { ![string is integer $MSG(-facility)] } {
	if { [llength [array names FACILITY $MSG(-facility)]] == 0 } {
	    return -code error \
		"-facility must be one of [join [array names FACILITY] ", "]"
	}
	set MSG(-facility) $FACILITY($MSG(-facility))
    } elseif { $MSG(-facility) < 0 || $MSG(-facility) > 23 } {
	return -code error \
	    "-facility must be between 0 and 23"
    }
    
    # Allow severity to be an integer (for the initiated) or a
    # string that can be read (for most of us)
    if { ![string is integer $MSG(-severity)] } {
	if { [llength [array names SEVERITY $MSG(-severity)]] == 0 } {
	    return -code error \
		"-severity must be one of [join [array names SEVERITY] ", "]"
	}
	set MSG(-severity) $SEVERITY($MSG(-severity))
    } elseif { $MSG(-severity) < 0 || $MSG(-severity) > 7 } {
	return -code error \
	    "-severity must be between 0 and 23"
    }
    
    # Once we have reached here arrange once and for all to have the
    # priority in -priority, whichever interface we had chosen...
    set MSG(-priority) [expr {8*$MSG(-facility) + $MSG(-severity)}]
    ::utils::getopt args -id MSG(-id) "-"
    if { [string length $MSG(-id)] > 32 } {
	return -code error "Message id too long, max 32 chars"
    }
    set MSG(-message) $msg
    ::utils::getopt args -timestamp MSG(-timestamp) [clock seconds]
    foreach k [list severity facility priority id message timestamp] {
	set MSG(-$k) [string trim $MSG(-$k)]
    }

    set m [Instance message $app]
    upvar \#0 $m M
    set M(self) $m
    set M(app) $app
    array set M [array get MSG]
    set M(-elements) {}

    return $m
}


# ::syslog::EncodeMessage5424 -- Encode message in RFC5424 format
#
#       Encode a message according to the RFC5424 specification.  This
#       will return a complete string description of the message, with
#       all necessary escaping performed.
#
# Arguments:
#	m	Identifier of the message, as returned by syslog::message
#
# Results:
#       Return the encoded content of the message
#
# Side Effects:
#       None.
proc ::syslog::EncodeMessage5424 { m } {
    variable SYSLOG

    upvar \#0 $m MSG
    upvar \#0 $MSG(app) APP
    set tstamp [clock format $MSG(-timestamp) -format $SYSLOG(syslogDate)]
    if { [string match "*%z" $SYSLOG(syslogDate)] \
	     && [lsearch -exact {+ -} [string index $tstamp end-4]] >= 0 } {
	set tstamp [string range $tstamp 0 end-2]:[string range $tstamp end-1 end]
    }
    set msg "<$MSG(-priority)>$SYSLOG(-version) $tstamp $APP(-hostname)"
    ::append msg " " $APP(-name)
    ::append msg " $APP(-pid)"
    ::append msg " $MSG(-id) "
    if { [llength $MSG(-elements)] == 0 } {
	::append msg "-"
    } else {
	foreach e $MSG(-elements) {
	    ::append msg [EncodeElement $e]
	}
    }

    if { $MSG(-message) ne "" } {
	::append msg " $MSG(-message)"
    }

    return $msg
}


# ::syslog::EncodeMessage3164 -- Encode message in RFC3164 format
#
#       Encode a message according to the RFC3164 specification.  This
#       will return a complete string description of the message, with
#       all necessary escaping performed.  However, some of the
#       content of the message will be left aside, since they have no
#       counterpart in the obsolete RFC3164.
#
# Arguments:
#	m	Identifier of the message, as returned by syslog::message
#
# Results:
#       Return the encoded content of the message
#
# Side Effects:
#       None.
proc ::syslog::EncodeMessage3164 { m } {
    variable SYSLOG

    upvar \#0 $m MSG
    upvar \#0 $MSG(app) APP
    set msg "<$MSG(-priority)>$APP(-name)\[$APP(-pid)\]:"
    if { $MSG(-message) ne "" } {
	::append msg " $MSG(-message)"
    }

    return $msg
}


# ::syslog::bind -- Bind a connection to an application
#
#       This procedure will bind a connection to an application.
#       These bindings are necessary so that the sending operations
#       know how and where to send the messages.  An application can
#       only be bound to a connection at a time, but can be bound to
#       several connections during its lifetime.
#
# Arguments:
#	a	Identifier of the application, returned by ::syslog::application
#	c	Identifier of the connection, returned by ::syslog::connect
#
# Results:
#       None.
#
# Side Effects:
#       None.
proc ::syslog::bind { a c } {
    if { ![IsA $a app] || ![IsA $c connection] } {
	return -code error "Invalid arguments, make sure you use an existing\
                            app and connection"
    }
    upvar \#0 $a APP
    set APP(connection) $c
}


# ::syslog::connect -- Connect to syslog server
#
#       This procedure will connect to a remote syslog server or to
#       the local syslog facility.  Given the connections facilities
#       that are in the core, TCP connections will always work (as
#       long as the server listens for connections on TCP!).  UDP
#       connections will require the availability of the TclUDP
#       package, while connections to the local system (thus to the
#       UNIX domain socket) will pass through a forked instance of nc.
#       This procedure takes a number of dash-led options and their
#       (possible) values (tk-style).  These are:
#       -server   The FQDN of the server to connect to.
#       -host     An alias for -server
#       -hostname Another alias for -server
#       -port     The port number to connect to.
#       -domain   The path to the domain socket (usually /dev/log)
#                 to connect on.
#       -udp      If set and -domain was not specified, choose UDP
#                 transport
#       -tcp      If set and -domain and -udp werw not specified,
#                 choose TCP transport (the default).
#       -counting A boolean that specifies how messages should be
#                 separated and encoded on the wire.  The default on
#                 TCP is to count, i.e. specify the length of each
#                 message before the content of the message.  The
#                 default on UDP and domain socket is to use
#                 linefeeds to separate messages.
#
# Arguments:
#	args	List of dash-led options, sometimes with values, see above.
#
# Results:
#       Return the identifier of the connection.  A connection has to
#       be bound to an application to be able to be used.
#
# Side Effects:
#       Will open connection to remote or local servers, or fork nc
proc ::syslog::connect { args } {
    variable SYSLOG

    set c [Instance connection]
    upvar \#0 $c CX

    set CX(-server) localhost
    ::utils::getopt args -server CX(-server)
    ::utils::getopt args -host CX(-server)
    ::utils::getopt args -hostname CX(-server)

    ::utils::getopt args -port CX(-port) 514

    if { [::utils::getopt args -domain uxpath "/dev/log"] } {
	if { $::tcl_platform(platform) ne "unix" } {
	    return -code error \
		"Connecting to domain sockets only works on UNIX systems"
	}
	set CX(-domain) $uxpath
	set CX(-transport) UX
	set CX(-counting) off
    } else {
	set CX(-domain) ""

	if { [::utils::getopt args -udp] } {
	    if { [catch {package require udp} ver] == 0 } {
		::utils::debug 5 "UDP support via udp at version $ver"
	    } else {
		unset $c
		return -code error \
		    "Cannot find TclUDP package: $ver"
	    }
	    set CX(-transport) UDP
	    set CX(-counting) off
	} else {
	    set CX(-transport) TCP
	    ::utils::getopt args -counting CX(-counting) on
	}
    }
    
    switch $CX(-transport) {
	"UDP" {
	    # Open a local socket and connect it to the remote server and
	    # port.
	    set CX(sock) [udp_open]
	    fconfigure $CX(sock) -remote [list $CX(-server) $CX(-port)]
	}
	"TCP" {
	    # Open a socket to the remote server and port
	    set CX(sock) [socket $CX(-server) $CX(-port)]
	}
	"UX" {
	    # Use external nc command to write to unix socket, until
	    # we find a better way
	    set CX(sock) [open "|[auto_execok nc] -U -u $CX(-domain)" w]
	}
    }

    if { [string is true $CX(-counting)] } {
	fconfigure $CX(sock) -translation binary
    } else {
	fconfigure $CX(sock) -translation lf
    }
    if { $SYSLOG(myHost) eq "" && $CX(-transport) ne "UX" } {
	if { [catch {fconfigure $CX(sock) -sockname} localInfo] == 0 } {
	    foreach {ip hostname port} $localInfo break
	    set SYSLOG(myHost) $hostname
	} else {
	    set SYSLOG(myHost) [info hostname]
	}
    }

    return $c
}


# ::syslog::send -- Send a message
#
#       Send a message through a given connection.  If the connection
#       is not specified, the connection that is bound to the
#       application to which the message is associated will be used.
#       Messages sent over UNIX domain sockets will be encoded
#       according to RFC3164, other messages according to the later
#       RFC5424.
#
# Arguments:
#	m	Identifier of the message, as returned by ::syslog::message
#	c	Identifier of the message, as returned by ::syslog::connect,
#               empty for connection associated to application of message.
#
# Results:
#       None.
#
# Side Effects:
#       The message is sent and forced on the wire with flush.  There
#       are no queues and no attempt will be made to resent messages
#       that could not be sent.
proc ::syslog::send { m { c "" } } {
    if {![IsA $m message] } {
	return -code error "Only messages created by this library can be sent"
    }
    upvar \#0 $m MSG
    if { $c eq "" } {
	upvar \#0 $MSG(app) APP
	set c $APP(connection)
    }
    if {![IsA $c connection] } {
	return -code error "Messages can only be sent through a connection!"
    }

    upvar \#0 $c CX
    if { $CX(-transport) eq "UX" } {
	set msg [EncodeMessage3164 $m]
    } else {
	set msg [EncodeMessage5424 $m]
    }
    ::utils::debug 5 "Sending $msg on the wire"
    if { [string is true $CX(-counting)] } {
	puts -nonewline $CX(sock) "[string length $msg] $msg"
    } else {
	puts $CX(sock) $msg
    }
    flush $CX(sock)
}


# ::syslog::log -- Send a log message
#
#       This procedure is a wrapper through the message and send
#       procedure.  It will create a message, following the same
#       options and values as specified in syslog::message and will
#       send the message to the connection bound to the application at
#       once.  The message will then be deleted from memory.  This is
#       basically a convenience procedure that performs all the
#       necessary work to send a single message when no structured
#       data need to be associated to the message.
#
# Arguments:
#	a	Identifier of application, as returned by ::syslog::application
#	msg	The text of the message
#	args	List of dash-led options and their values, see ::syslog::message
#
# Results:
#       None.
#
# Side Effects:
#       Will attempt to send the message using the proper transport
#       protocol.
proc ::syslog::log { a msg args } {
    if { ![IsA $a app] } {
	return -code error "Logging needs an application"
    }

    set m [message $a $msg {*}$args]
    send $m
    delete $m
}

# If we are not being included in another script, run a quick test
if {[file normalize $::argv0] eq [file normalize [info script]]} {
    # Connect to a remote/local hosts
    set tcp [::syslog::connect -host localhost -port 10514]
    set udp [::syslog::connect -host localhost -port 514 -udp]
    set ux [::syslog::connect -domain /dev/log]
    
    # Create an application and give it an IANA enterprise ID, this is
    # seldom used so [::syslog::application] works in most cases.
    set a [::syslog::application -enterprise 32473]
    
    # Bind the application to the domain socket and send a single message at
    # the default priority.
    ::syslog::bind $a $ux
    ::syslog::log $a "Hello the beautiful simplified world"
    
    # Bind to the TCP socket and send some more
    ::syslog::bind $a $tcp
    ::syslog::log $a "More to say??"
    
    # Bind on UDP
    ::syslog::bind $a $udp
    
    # Create a complex type to be used in structured data
    set t [::syslog::type $a myType field1 field2]
    
    # Prepare a message for sending, this time at another priority
    set m [::syslog::message $a "Testing another more complex example" \
	       -priority local0.info]
    # Append structured data to the message, send it and then free memory.
    ::syslog::append $m [::syslog::element $t field1 45 field1 34 field2 78]
    ::syslog::send $m
    ::syslog::delete $m

    ::syslog::delete $a
    
    ::syslog::delete $tcp
    ::syslog::delete $udp
    ::syslog::delete $ux
}

package provide syslog $::syslog::version

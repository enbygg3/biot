# Exercising Disque support

This document provides some quick instructions to exercise and manually test
Disque support in biot.

## Server

Arrange to run a server on your localhost or a remote server.

### In Docker Environment

Provided that you have Docker installed on your machine, running a server should
be as easy as running the following command thanks to
[richnorth/disque](https://hub.docker.com/r/richnorth/disque).

```console
docker run -d --name=disque richnorth/disque
```

Once the container is running, capture its IP address using the following command:

```console
export DISQUE_HOST=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' disque)
```

## Source

The [`sources.cfg`](consumer/sources.cfg) file in the `consumer` directory
contains the description of a source called `disque`. The source definition uses
a number of variables (environment or biot globals) to tune where to get data
from. These are:

* `DISQUE_HOST`: the hostname to contact, this should be one of the disque
  server clusters and should have been set if you followed the Docker
  instructions above. It defaults to `localhost`.
* `DISQUE_PORT` is the port to connect at the server, defaults to `7711`.
* `DISQUE_PASSWD` is the password for connection and defaults to empty.
* `DISQUE_QUEUE` is the name of the queue to get data from, it defaults to
  `default`.

The source will poll for jobs every 5 seconds, and every time a job is available
its content will be funnelled further into biot for variable extraction.

### Biot

Arrange for the environment variables above to be present if necessary. Assuming
you are running in a Docker environment as above, you already have the only
necessary variable, i.e. `DISQUE_HOST`. Then, run the following command, from
the same directory as this README file. The command arranges to switch off auto
variables in biot and add some extra debugging on the variable module to let you
see the results of your actions.

```console
../../bin/biot.tcl -sources @./consumer/sources.cfg -vars @./consumer/vars.cfg -auto -1 -verbose\< "vrbl 5"
```

### Job Production

To push data to the Disque queue, do as follows, provided that `172.17.0.2` is
the IP address of the Disque container.

```console
docker run -it --rm  richnorth/disque disque -h 172.17.0.2
```

Then, at the Disque prompt, you should be able to execute disque commands. For
example, issue the following to send some dummy data to the `default` queue.

```console
addjob default thisisatest 1000
```

### Consumption

In this configuration, there is not any consumer. But you should be able to
manually check that biot is properly updating a variable (declared in
`vars.cfg`) called `body` to the same content as what you pushed to the queue.

```console
[20180726 084913] [info] [vrbl] Value of body is now 'thisisatest'
[20180726 084913] [notice] [vrbl] Computing values of orphane variables
[20180726 084913] [notice] [vrbl] Flushing out content of 1 variable(s)
```

## Destination

The [`sources.cfg`](producer/sources.cfg) file in the `producer` directory
contains the description of a source that uses the content of this file as the
content of jobs to send to the queue. New jobs are created every 30s.

The [`destinations.cfg`](producer/destinations.cfg) file in the `producer`
directory contains the description of a disque destination called `disque`. The
destination definition uses a number of variables (environment or biot globals)
to tune where to send data to. These are:

* `DISQUE_HOST`: the hostname to contact, this should be one of the disque
  server clusters and should have been set if you followed the Docker
  instructions above. It defaults to `localhost`.
* `DISQUE_PORT` is the port to connect at the server, defaults to `7711`.
* `DISQUE_PASSWD` is the password for connection and defaults to empty.
* `DISQUE_QUEUE` is the name of the queue to send data to, it defaults to
  `default`.

### Biot

Arrange for the environment variables above to be present if necessary. Assuming
you are running in a Docker environment as above, you already have the only
necessary variable, i.e. `DISQUE_HOST`. Then, run the following command, from
the same directory as this README file. The command arranges to switch off auto
variables in biot and add some extra debugging on the variable module to let you
see the results of your actions.

```console
../../bin/biot.tcl -sources @./producer/sources.cfg -vars @./producer/vars.cfg -dests @./producer/destinations.cfg -auto -1 -verbose\< "vrbl 5"
```

### Job Consumption

To get this data back from the Disque queue, do as follows, provided that
`172.17.0.2` is the IP address of the Disque container.

```console
docker run -it --rm  richnorth/disque disque -h 172.17.0.2
```

Then, at the Disque prompt, you should be able to execute disque commands. For
example, issue the following to get the content of this file back from the
`default` queue.

```console
getjob from default
```

Do not forget to acknowledge the job with `ackjob` and stop biot so that you
will not fill in the job queue with content every 30 seconds.

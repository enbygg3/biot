# Tcl-based Sources

For cases that are outside of the original capabilities of biot, you have the
choice to implement your sources not as a new plugin, but instead via the
generic `interp://` mechanisms. This will bound the value of variables (or their
extraction) to any Tcl code placed in an interpreter. To allow for maximum
flexibility this is *not* a safe interpreter, meaning that security is at its
minimum.

For more details, read the comments at the beginning of the files `sources.cfg`
and `vars.cfg`. A dummy implementation, bound to those files can be found in
`producer.tcl`, which hardly produces anything...
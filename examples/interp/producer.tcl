# This is an example of a producer, see the main comment in sources.cfg for more
# details.
package require Tcl

# This procedure will be called once and only once when the interpreter is
# created and initialised (as specified by sources.cfg). It can take any number
# of variables to initialise connections, etc.
proc init { args } {
    puts stderr "Interp initialised using: $args"
}

# The result of this procedure, as constructed by sources.cfg will be pushed
# further for extraction into variables. We return some slightly formatted
# string in this constructed example.
proc main { platform } {
    puts stderr "Requesting some data on $platform"
    return "willBeIgnoredByTheRegEXP = [expr {int(rand()*255)}]"
}

# This procedure is bound to variables instead. The variables are of type
# DELEGATED and will call it at regular intervals (as specified by the frequency
# in sources.cfg)
proc compute { varname } {
    puts stderr "Requesting value of $varname"
    switch -glob -- $varname {
        "my*var" {
            return [expr {int(rand()*32767)}]
        }
    }
    return ""
}
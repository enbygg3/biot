proc limiter { howoften old new } {
    variable lasttime
    
    set trigger 1
    if { [info exists lasttime] } {
        set now [clock seconds]
        set trigger [expr {$now-$lasttime>=$howoften}]
        if { $trigger } {
            set lasttime $now
        }
    }
    
    return $trigger
}

if { $argc >= 3 } {
    limiter {*}$argv    
}

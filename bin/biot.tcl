#!/usr/bin/env tclsh

##################
## Program Name    --  biot.tcl
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##    Biot implements a generic network-aware information pipe to move
##    data from anywhere to anywhere: from devices to the cloud
##    (sensing), from the cloud to devices (actuating) but also
##    between clouds (integration).  Biot is tuned to be embedded in
##    modern cloud architectures based on micro-services and has been
##    deployed within CoreOS and Docker Swarm clusters.
##
##################

# Global state, including "options" that cannot be changed at the
# command-line but good to have visible this high-up.
array set BDG {
    -separators ".:,"
    -maxiter    200
}

# Access our dependencies, including til and tclxmpp. Easiest it to
# check them out under the lib directory as exemplified below:
# svn checkout http://tclxmpp.googlecode.com/svn/trunk/ ../lib/tclxmpp
set rootdir [file normalize [file dirname [info script]]]
lappend auto_path [file join $rootdir .. lib] [file join $rootdir lib]
foreach dir [list toclbox modules] {
    foreach d [list [file join $rootdir .. lib $dir] [file join $rootdir lib $dir]] {
        if { [file isdirectory $d] } {
            if { $dir eq "modules" } {
                foreach external [list disque mqtt] {
                    if { [file isdirectory [file join $d $external]] } {
                        ::tcl::tm::path add [file join $d $external]
                    }
                }
            } else {
                ::tcl::tm::path add $d
            }
        }
    }
}
foreach external [list til tclxmpp stomp tockler] {
    foreach td [list [file join $rootdir .. .. .. .. common $external] \
		    [file join $rootdir .. lib $external]] {
        if { [file isdirectory $td] } {
            lappend auto_path $td
        }
    }
}

# Require all necessary packages.
package require utils
package require http
package require sha1
package require tls
package require uri
package require base64

# Require all internal packages, most of the heavy-work is done within
# those packages.
package require biot::uri
package require biot::common
package require biot::src
package require biot::vrbl
package require biot::rest
package require biot::dst

# Create a list with all command-line options to program, including a
# short description of what they do.
set prg_args {
    -env      "cfgdir %progdir%/../cfg tpldir %progdir%/../cfg/tpl"     "Set of variables that can be used for substitution in files and in arguments."
    -sources  "@%cfgdir%/sources.cfg"       "URL(s) of sources, start with @ for filename indirection"
    -vars     "@%cfgdir%/vars.cfg"          "Variables and how to extract from sources, start with @ for filename indirection or < for templating with arguments"
    -dests    "@%cfgdir%/destinations.cfg"  "Destinations, start with @ for filename indirection"
    -mounts   ""                            "List of location and (VFS) mount points, leading @ for file indirection"
    -flush    100                           "Flushing timeout, in ms"
    -timeout  20                            "Timeout in seconds for HTTP calls"
    -config   "%progdir%/../cfg/%progname%.cfg" "File to get additional command-line arguments from"
    -silent   ""                            "List of patterns to match against destination names, matching names will be silent and not trigger"
    -port     "http:8800"                   "Where to listen for REST requests"
    -www      ""                            "Which directory to serve for HTTP"
    -verbose  "templater 3 utils 2 * 4"     "Verbosity specification for internal modules"
    -auto     1000                          "Period (in ms) to update automatic variables, negative to switch off"
    -watchdog 600                           "Maximum silence (in s) for a source before reconnection, negative to switch off"
    -log      ""                            "Name of file to receive log, URI to syslog daemon or empty for stderr"
    -h        ""                            "Print this help and exit"
    -tweaks   ""                            "List of internal tweaks to configure semi-internal features"
    -update   ""                            "When to force internal updating: idle, full or empty for never"
}



# ::help:dump -- Dump help
#
#       Dump help based on the command-line option specification and
#       exit.
#
# Arguments:
#	hdr	Leading text to prepend to help message
#
# Results:
#       None.
#
# Side Effects:
#       Exit program
proc ::help:dump { { hdr "" } } {
    if { $hdr ne "" } {
	puts $hdr
	puts ""
    }
    puts "IoT Bridge: source(s) -> extraction -> destination(s)"
    foreach { arg val dsc } $::prg_args {
	puts "\t${arg}\t$dsc (default: ${val})"
    }
    exit
}


# Did we ask for help at the command-line, print out all command-line
# options described above and exit.
if { [::utils::getopt argv -h] } {
    ::help:dump
}

# Rest of the program initialisation happens further down...


# ::httpd:init -- Initialise HTTPd
#
#       Start listening to the ports specified for incoming HTTP
#       connections.  Apart from the REST-like interface, this will
#       also arrange to serve a directory.  This only implements HTTP
#       at present, but extending to HTTPS isn't far off as the
#       underlying server supports it.
#
# Arguments:
#	None.
#
# Results:
#       Return the list of server identifiers for the instances that
#       were created with success.
#
# Side Effects:
#       Will serve on the specified ports.
proc ::httpd:init {} {
    global BDG

    set servers {}
    foreach p $BDG(-port) {
	set srv -1
	
	if { [string is integer -strict $p] } {
	    set srv [::biot::rest::listen $port \
			 [::utils::resolve $BDG(-www) [::biot::common::environment]]]
	} elseif { [string first ":" $p] >= 0 } {
	    foreach {proto port} [split $p ":"] break
	    switch -nocase -- $proto {
		"HTTP" {
 		    set srv [::biot::rest::listen $port \
				 [::utils::resolve $BDG(-www) [::biot::common::environment]]]
		}
	    }
	}
	
	if { $srv > 0 } {
	    lappend servers $srv
	}
    }

    return $servers
}


# Extract list of command-line options into array that will contain
# program state.  The description array contains help messages, we get
# rid of them on the way into the main program's status array.
array set BDG {}
foreach { arg val dsc } $prg_args {
    set BDG($arg) $val
}

# First pass of options propagation for initial behaviour
eval ::utils::verbosity $BDG(-verbose)
::biot::common::defaults "" -env $BDG(-env) -log $BDG(-log)
::biot::uri::defaults -timeout $BDG(-timeout)
::biot::src::defaults -timeout $BDG(-timeout) -flush $BDG(-flush) -separators $BDG(-separators) -watchdog $BDG(-watchdog)
::biot::vrbl::defaults -flush $BDG(-flush) -separators $BDG(-separators) -maxiter $BDG(-maxiter) -update $BDG(-update)
::biot::dst::defaults -flush $BDG(-flush) -separators $BDG(-separators) -timeout $BDG(-timeout) -silent $BDG(-silent)

# Extract command-line options from the command line, defaulting to
# values specified at the top.  This supports syntactic sugaring that
# will prepend or append the value passed on the command line to the
# value (possibly the default), when the name of the argument ends
# respectively with < or >. In order to be able to support
# configuration via files, we first extract the value of -config, read
# and parse its content, for then possibly overriding with the content
# of the arguments, both passes allow prepending and appending sugaring.
::utils::pushopt argv -env BDG
::biot::common::defaults "" -env $BDG(-env); # Send further in case it changed
::utils::pushopt argv -config BDG
set modified [list]
set fname ""
if { $BDG(-config) ne "" } {
    set fname [::biot::common::resolve $BDG(-config)]
    ::utils::debug 4 "Reading configuration file at $fname"
    set opts [list]
    foreach {k v} [::utils::lread $fname 2 "configuration"] {
        lappend opts -[string trimleft $k -] $v
    }
    foreach opt [array names BDG -*] {
	set modified [concat $modified [::utils::pushopt opts $opt BDG]]
    }
}
foreach opt [array names BDG -*] {
    ::utils::pushopt argv $opt BDG
}
if { [lsearch $modified -env] >= 0 } {
    ::biot::common::defaults $fname -env $BDG(-env)
}

# Remaining args? Dump help and exit
if { [llength $argv] > 0 } {
    ::help:dump "[lindex $argv 0] is an unknown command-line option!"
}

# Basic check on option values
set failed [::utils::chkopt BDG \
		flush integer \
		timeout double \
		auto integer \
		watchdog double]
if { [llength $failed] > 0 } {
    set msg ""
    foreach {opt type} $failed {
	append msg "Option $opt should be a(n) $type. "
    }
    ::help:dump [string trim $msg]
}

# Second pass of options propagation for life-long onwards behaviour.
# This happens since the options might have changed as part of reading
# the configuration.
eval ::utils::verbosity $BDG(-verbose)
::biot::common::defaults $fname -log $BDG(-log)
::biot::uri::defaults -timeout $BDG(-timeout)
::biot::src::defaults -timeout $BDG(-timeout) -flush $BDG(-flush) -separators $BDG(-separators) -watchdog $BDG(-watchdog)
::biot::vrbl::defaults -flush $BDG(-flush) -separators $BDG(-separators) -maxiter $BDG(-maxiter)
::biot::dst::defaults -flush $BDG(-flush) -separators $BDG(-separators) -timeout $BDG(-timeout) -silent $BDG(-silent)

# Tell how we were started to make sure we provide feedback over how
# we believe things are...
set startup "Starting [file rootname [file tail [info script]]] with options\n"
foreach {k v} [array get BDG -*] {
    append startup "\t$k:\t$v\n"
}
::utils::debug 5 [string trim $startup]

# Give away a separate file that will receive all log outputs
if { $BDG(-log) ne "" } {
    ::utils::logger @::biot::common::outlog
}

# Initialise mount points, this requires the vfs package to function, and will
# rely on the underlying protocol implementations available locally.
set mounts [::biot::common::inline $BDG(-mounts) 2 "mounts" fname]
if { [llength $mounts] > 0 && [catch {package require vfs} ver] == 0 } {
    # Resolve file content to list of 2 elements.
    foreach {src location} $mounts {
        set src [::biot::common::resolve $src $fname]
        set dst [::biot::common::resolve $location $src]
        set i [string first "://" $src]
        if { $i >= 0 } {
            incr i -1
            set proto [string range $src 0 $i]
            switch -- $proto {
                "http" -
                "https" {
                    if { [catch {package require vfs::http} ver] == 0 } {
                        ::utils::debug NOTICE "Mounting $src onto $dst"
                        ::vfs::http::Mount $src $dst
                    } else {
                        ::utils::debug WARN "Cannot mount from $src, don't know about http!"
                    }
                }
                default {
                    if { [catch {package require vfs::$proto} ver] == 0 } {
                        ::utils::debug NOTICE "Mounting $src onto $dst"
                        ::vfs::${proto}::Mount $src $dst
                    } else {
                        ::utils::debug WARN "Cannot mount from $src, don't know about $proto!"
                    }
                }
            }
        } else {
            set ext [string trimleft [file extension $src] .]
            if { [catch {package require vfs::$ext} ver] == 0 } {
                ::utils::debug NOTICE "Mounting $src onto $dst"
                ::vfs::${ext}::Mount $src $dst
            } else {
                ::utils::debug WARN "Cannot mount from $src, don't know about $ext!"
            }
        }
    }
} else {
    ::utils::debug ERROR "No VFS support, will not be able to mount!"
}

# Initialise tweaks early on
set tweaks [::biot::common::tweaks $BDG(-tweaks)]
::utils::debug 2 "Applied [llength $tweaks] internal tweak(s)"
# Initialise sources, variables and destinations.
set sources [::biot::src::multi $BDG(-sources)]
::utils::debug 2 "Created [llength $sources] source(s) for information" source
set vars [::biot::vrbl::multi $BDG(-vars) $BDG(-auto)]
::utils::debug 2 "Created [llength $vars] variable(s) to extract from\
                  sources" vars
if { [llength $vars] <= 0 } {
    ::utils::debug 1 "No variables declared, exiting" vars
    exit
}
# Create magic variable __INIT__ so we can initiate destinations, if
# necessary.
set init_v [::biot::vrbl::new __INIT__ INIT "" ""]
set dests [::biot::dst::multi $BDG(-dests)]
::utils::debug 2 "Created [llength $dests] destination(s) of variable\
                  changes" dest

# Make sure we use TLS whenever we can, now that POODLE has been
# here... Then connect to all sources and start living forever.
::toclbox::network::https
::httpd:init

::biot::src::connectall
::biot::dst::connectall

$init_v setval 1;   # Now set __INIT__ to 1 to tell that initialisation is done

vwait forever;      # Run, Forrest, Run


# TODO?
#
# Change the implementation of the HTTP so that, if it has no polling
# period, we suppose a long-going HTTP connection and thus start
# listening for data as soon as there is (i.e. do some "tail").
#
# Implement a "-once" command line argument that will check all
# sources ONCE and only once, trigger all possible destinations out of
# the variables that could be isolated and then exit gently.  This
# would be nice for cron jobs.
#
# Implement a < command as part of the source transformations.  This
# would take a template, pass it the data that was acquired from the
# source, and consider the result of the template as being the content
# of the source.


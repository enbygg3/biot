# ::json:cleaner -- Clean variable name
#
#       This procedure cleans away characters from a variable names
#       that are not suitable for use as a JSON variable or object
#       index.
#
# Arguments:
#	vname	Name of variable
#	rpl	Character to use as a replacement in output name
#
# Results:
#       Return the cleaned name of the variable, where all occurrences
#       of forbidden characters have been replaced by the replacement
#       character passed as an argument.
#
# Side Effects:
#       None.
proc ::json:cleaner { vname {rpl ""} } {
   set cln ""
   foreach c [split $vname ""] {
       if { [lsearch {\" \\ \/ \b \f \n \r \t} $c] >= 0 } {
	   if { $rpl ne "" } {
	       append cln $rpl
	   }
       } else {
	   append cln $c
       }
   }
   return $cln
}


# ::json:value -- Return JSON value representation
#
#       This procedure performs qualified guesses as whether the data
#       passed as argument is a number, a boolean or a string and
#       outputs a construct that can be ready to insert in a JSON
#       object.  Strings will automatically be quoted, while numbers
#       and booleans not.
#
# Arguments:
#	v	Value to insert as JSON value
#
# Results:
#       Returns a JSON compatible representation of the incoming value.
#
# Side Effects:
#       None.
proc ::json:value { v } {
    if { [string is double -strict $v] } {
     	return $v
    }
    if { [string is boolean -strict $v] } {
      	return [expr {[string is true $v]?"true":"false"}]
    }
    return "\"$v\""
}

# ::xml:extract:attr -- Extract value of attribute
#
#       This will look in the incoming string for a tag that would
#       contain a set of attributes.  It will then return the value of
#       a given attribute.  In addition, the value of this attribute
#       might only be returned if it matches a number of conditions,
#       where the conditions are the name or attributes a patterns
#       matching their values.
#
# Arguments:
#	dta	Incoming XML text
#	tag	Tag to dig in attributes for
#	attr	Attribute of tag which value is to be returned if present
#	conds	List of attributes and value filter that need to exist and match
#
# Results:
#       The value of the first matching attribute if it existed, empty
#       string otherwise.
#
# Side Effects:
#       None.
proc xml:extract:attr { dta tag attr conds } {
    set idx 0
    while 1 {
	set idx [string first "<$tag" $dta $idx]
	if { $idx < 0 } {
	    return ""
	} else {
	    incr idx [string length $tag]
	    incr idx 1
	    set end [string first ">" $dta $idx]
	    set cnt [string trim [string range $dta $idx [expr {$end-1}]]]
	    set cnt [string trimright $cnt "/"]
	    set cnt [string trim $cnt]
	    set map [string map [list "=" " "] $cnt]
	    if { [catch {array set TAG $map}] == 0 } {
		foreach k [array names TAG] {
		    set TAG($k) [string trim [string trim $TAG($k) "\"'"]]
		}

		set match 1
		foreach {k v} $conds {
		    if { ![info exists TAG($k)] \
			     || ([info exists TAG($k)] \
				     && ![string match $v $TAG($k)]) } {
			set match 0
		    }
		}

		if { $match } {
		    if { [info exists TAG($attr)] } {
			return $TAG($attr)
		    }
		}
	    }
	}
    }
}

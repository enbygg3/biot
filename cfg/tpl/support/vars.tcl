# ::vars:static -- Non automatic variable selection
#
#       This procedure takes the list of changes that is typically
#       passed to a destination template and output a new list of
#       variables where variables which names start with __ (such as
#       __INIT__) and which names start with < (such as the ones used
#       to automatically create variables) are removed.
#
# Arguments:
#	changes	List of variables describing the full change
#
# Results:
#       Cleaned list of changes, where only "real" variables are
#       available.
#
# Side Effects:
#       None.
proc ::vars:static { changes } {
    set realchanges {}
    foreach itm $changes {
	if { ![string match "__*" $itm] && ![string match "<*" $itm] } {
	    lappend realchanges $itm
	}
    }
    return $realchanges
}

proc ::vars:timestamp { changes_ {rm 1} } {
    upvar $changes_ changes

    # Prefer any timestamp that could be found in the data
    set i 0
    foreach var $changes {
	if { [lsearch -nocase {time timestamp} $var] >= 0 } {
	    upvar $var v
		if { [string is double -strict $v] } {
			if { [string is true $rm] } {
				set changes [lreplace $changes $i $i]
			}
			if { $v > pow(2,31) } {
				return [expr {int($v)}]
			} else {
				return [expr {int($v)*1000}]
			}
		}
	}
	incr i
    }

    # Otherwise pick up the one that would be set by biot, or create
    # an entirely new one.
    if { [info exists ::__timestamp__] } {
        return $::__timestamp__
    } else {
        return -1
    }
}


# ::vars:english -- Englishized variable name
#
#       Replace the occurence of a number of common letters in
#       European languages to their english counterparts in lower
#       range of the ASCII table.
#
# Arguments:
#	vname	Variable name
#
# Results:
#       Return a new variable name where "strange" letters have been
#       replaced.
#
# Side Effects:
#       None.
proc ::vars:english { vname } {
    return [string map {Å A Ä A Ö O å a ä a ö o} $vname]
}


proc ::vars:nosep { vname { rpl "_" } } {
    set cln ""
    foreach c [split $vname ""] {
	if { $c eq "," || $c eq "." || $c eq ":" } {
	    append cln $rpl
	} else {
	    append cln $c
	}
    }
    return $cln
}

proc ::vars:aggessive { vname {rpl ""}} {
    set cln ""
    foreach c [split [vars:english $vname] ""] {
	if { [string match -nocase {[a-z0-9_]} $c] } {
	    append cln $c
	} else {
	    append cln $rpl
	}
    }
    return $cln
}

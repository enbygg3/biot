# This is the template that will be expanded to get the list of
# variables that the bridge daemon will extract.  This template
# implements the etcd API and extracts the data from the recursive
# listing of data that was requested from etcd using the source.
<% foreach s $args {%>
etcd.<%=$s%>
	JSON.value
	/node/nodes(*)/key match */<%=$s%>
<% } %>
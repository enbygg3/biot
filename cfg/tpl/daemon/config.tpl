# This is the template that will be expanded to form the configuration
# of the bridge that is started by the etcd-watching bridge.  Mainly,
# this will be getting the information that was written to the files
# placed under rootdir by that bridge and containing the real settings
# for what we want to perform.
-vars
	"< %tpldir%/vars.tpl @%rootdir%/vars.lst"

-sources
	@%rootdir%/sources.cfg

-dests
	@%rootdir%/destinations.cfg

# Arrange to pass further the root directory that was an argument of
# the first bridge into the second bridge so that it gets to know
# where to find its configuration files.
-env<
	"rootdir <%=$rootdir%> "
# This is the template that will be expanded to generate the
# destinations of the bridge that is in charge of getting/watching
# data in the etcd data space and (re)starting another bridge with
# that information.

# First create local copies of the files coming from etcd.  These
# files are all created into the rootdir directory.  We have a special
# case around the extension of vars (that is not .cfg, but rather
# .lst).
<% foreach v $vars { %>
   <% if { [string match "var*" $v] } { %>
<%=$v%>:<%=$v%>
	file:://%rootdir%/<%=$v%>.lst
	@%tpldir%/daemon/inline.tpl
	-
   <% } else { %>
<%=$v%>:<%=$v%>
	file:://%rootdir%/<%=$v%>.cfg
	@%tpldir%/daemon/inline.tpl
	-
   <% } %>
<% } %>

# Make sure we have a configuration file that forwards the root
# directory, since this will be where we host all our local files.
# The bridge that we will start need to know where to find its files,
# which is what rootdir exists for.
config:*
	file://%rootdir%/config.cfg
	@%tpldir%/daemon/config.tpl rootdir
	-

# Start a copy of ourselves, with the copy of the files coming from
# etcd as arguments.  Also, make sure that we log to the proper,
# i.e. the URI contained in loguri.
restart:<%=[join $vars ","]%>
	exec+restart+nofwd://tclsh8.6 %progdir%/%progname%.tcl -config %rootdir%/config.cfg -log %loguri%
	@%tpldir%/daemon/inlineall.tpl
	-

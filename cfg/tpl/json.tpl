<% source %tpldir%/support/json.tcl %>
<% source %tpldir%/support/vars.tcl %>
<% set __changes__ [vars:static $__changes__] %>
<% set __timestamp__ [vars:timestamp __changes__] %>
<% set len [llength $__changes__]; set i 0 %>
<% if { $__timestamp__ > 0 } { %>
{<% foreach v $__changes__ { incr i; %>"<%=[vars:aggessive $v]%>":<%=[json:value [set $v]],%><%}%> timestamp: <%=$__timestamp__%>}
<% } else { %>
{<% foreach v $__changes__ { incr i; %>"<%=[vars:aggessive $v]%>":<%=[json:value [set $v]]%><%if {$i<$len} {%>,<%}%><%}%>}
<% } %>

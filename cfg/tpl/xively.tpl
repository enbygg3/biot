<% proc cleaner { str } {
   set cln ""
   foreach c [split $str ""] {
   	   if { [string match {[A-Za-z0-9_\-]} $c] } {
	      append cln $c
	   } else {
	      append cln "_"
           }
   }
   return $cln
} %>
{
    "version" : "1.0.0",
    "datastreams" : [
<% set len [llength $__changes__]; set i 0 %>
<% foreach v $__changes__ {%>
<% incr i %>
	{
	    "id" : "<%=[cleaner $v]%>",
	    "current_value" : "<%=[set $v]%>"
	}<% if { $i < $len } {%><%=,%><%}%>
<% } %>
    ]
}

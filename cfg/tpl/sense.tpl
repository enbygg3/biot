<% source %tpldir%/support/json.tcl %>
<% source %tpldir%/support/vars.tcl %>
<% set __changes__ [vars:static $__changes__] %>
<% set len [llength $__changes__]; set i 0 %>
{
<% foreach v $__changes__ {%>
<% incr i %>
  "<%=[json:cleaner $v]%>" : <%=[json:value [set $v]]%><% if { $i < $len } {%><%=,%><%}%>
<% } %>
}

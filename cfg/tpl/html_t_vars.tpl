# This file is to be used as the templating specification whenever
# starting the bridge with a -vars options which value starts with a <
# sign. You can use the %-notation to use various local variables to
# access data.
#
# When starting with such an option, you can either specify a number
# of arguments after the template specification, separated by
# whitespaces.  These arguments will be given to the template as the
# content of the variable "args" (see below).  Alternatively, you can
# grab the content of these arguments from a file, using an argument
# that starts with a @ sign.
#
# The following assumes HTML tabular data

<% proc cleaner { str } {
   set cln ""
   foreach c [split $str ""] {
   	   if { [string match {[A-Za-z0-9_\-]} $c] } {
	      append cln $c
	   } else {
	      append cln "_"
           }
   }
   return $cln
}%>

<% foreach s $sources {%>
<% foreach v $args {%>
<%=$s%>.<%=[cleaner $v]%>
	RX.1
	<td.*?><%=$v%></td.*?>.*?<td.*?>([\-\.\d]+)</td.*?>
<% } %>
<% } %>
##################
## Module Name     --  rate_limit
## Original Author --  Emmanuel Frecon - emmanuel@sics.se
## Description:
##
##      This module hosts a number of procedures ready to be used via the new
##      variable triggers mechanisms. The aim of these procedures is to provide
##      a standard implementation for rate limiting of various sorts, i.e.
##      arranging to output the values of variables only if they have changed
##      significantly, or at a maximum pace.
##
##      In a standard installation, specifying a trigger with
##      <rate_limit!varname!10s@%cfgdir%/support/rate_limit.tcl would arrange
##      for variable varname never to propagate to destinations more often than
##      10 seconds.
##
## Commands Exported:
##      All procedures starting with a lowercase are exported, commands starting
##      with an underscore are local to the implementation.
#################

# We will keep state in a namespace, meant to be private.
namespace eval ::flush {};   # Will contain flushing timestamps for variables.

# ::__time_convert -- Convert human-readable time strings
#
#       Converts strings written a human-friendly format, e.g. 10s, into an
#       actual number of milliseconds. The procedure recognises an integer or a
#       floating value, followed by a timelapse specification. Recognised
#       specifications are: s for seconds, m for minutes, h for hours, d for
#       days. This implementation is able to cope with space delimiters between
#       the amount and the lapse, and will always return an integer number of
#       milliseconds, e.g. it will cope properly with "0.45 s" (without the
#       quotes).
#
# Arguments:
#	lapse	Specification of lapse, see above.
#
# Results:
#       Return the length of the lapse in milliseconds.
#
# Side Effects:
#       None.
proc __time_convert { lapse } {
    if { [string is integer -strict $lapse ] } {
        return $lapse
    } else {
        set last [string index $lapse end]
        set val [string trim [string range $lapse 0 end-1]]
        set mult [dict create s 1000 m 60000 h 3600000 d 86400000]
        if { [dict exists $mult $last] } {
            return [expr {int($val*[dict get $mult $last])}]
        } else {
            return -code error "$last is not a known multiplier!"
        }
    }
}


# ::rate_limit -- Limit output rate based on a given period
#
#       Limit output of a variable based on a given maximum period. This will
#       force an update at the period passed as a parameter, but will ensure
#       that there will be no more triggers that the specified period. The
#       period can either be a number of milliseconds, or a human-friendly
#       string such as 10s or 0.2d.
#
# Arguments:
#	varname	Name of variable.
#	period	Period, see __time_convert.
#	oldval	Old value of variable.
#	newval	New value of variable.
#
# Results:
#       1 if the variable hasn't been updated for more that period milliseconds,
#       0 otherwise.
#
# Side Effects:
#       Keeps flushing timings in local namespace.
proc rate_limit { varname period oldval newval } {
    set ms [__time_convert $period]
    set now [clock milliseconds]
    if { [info exists ::flush::$varname] } {
        if { $now-[set ::flush::$varname] >= $ms } {
            return 1
        } else {
            return 0
        }
    } else {
        set ::flush::$varname $now
        return 1
    }
    return 0
}


# ::diff_limit -- Avoid propagating small variations.
#
#       This procedure will avoid variable updating propagation when the
#       variable has changed little. The implementation supports numbers or list
#       of numbers, as long as both lists used for comparison are of equal
#       length. The procedure cooperates properly with the rate limiting
#       procedure above and remembers whenever an output for a given variable
#       was triggered.
#
# Arguments:
#	varname	Name of variable.
#	diff	Delta of change for trigger (either one number or a list).
#	oldval	Old value of variable (can be a list of numbers).
#	newval	New value of variable (can be a list of numbers).
#
# Results:
#       1 if the value of the variable has changed by more than diff (where diff
#       can also be a list of deltas, a list which is as long as the old and new
#       value of the variable). 0 otherwise.
#
# Side Effects:
#       Keeps flushing timings in local namespace.
proc diff_limit { varname diff oldval newval } {
    if { [catch {llength $oldval} oldlen] == 0 && [catch {llength $newval} newlen] == 0 } {
        if { $oldlen == $newlen } {
            if { [llength $diff] == 1 } {
                set diff [lrepeat $oldlen $diff]
            }
            
            set changed 0
            foreach old $oldval new $newval d $diff {
                if { abs($new-$old) > abs($d) } {
                    # Mark that we have changed.
                    set changed 1

                    # Remember that we have triggered, so we can properly
                    # cooperate with rate_limit procedure above.
                    set now [clock milliseconds]
                    set ::flush::$varname $now
                    
                    break
                }
            }
            
            return $changed
        }
    }
    return 1
}


# ::rate_diff_limit -- Avoid propagating small variations and reduce update frequency.
#
#       This procedure simply combine both procedures above to avoid the
#       propagation of variables that have only slightly changed and to arrange
#       for updates to occur at a minimum interval.
#
# Arguments:
#	varname	Name of variable.
#	diff	Delta of change for trigger (either one number or a list).
#	period	Period, see __time_convert.
#	oldval	Old value of variable.
#	newval	New value of variable.
#
# Results:
#       1 or 0
#
# Side Effects:
#       Keeps flushing timings in local namespace.
proc rate_diff_limit { varname diff period oldval newval } {
    return [expr {[diff_limit $varname $diff $oldval $newval] || [rate_limit $varname $period $oldval $newval]}]
}

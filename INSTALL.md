# Installation notes

To build a docker component out of this, run

    sudo docker build -t emfr/bridge .

Then, assuming you are logged in (otherwise, do a `docker login`):

    sudo docker push emfr/bridge


# Running

    sudo docker run -it emfr/bridge -env\< "rootdir %cfgdir%/hs/romo2 " -vars "<%tpldir%/vars.tpl @%rootdir%/vars.lst" -sources @%rootdir%/sources.cfg -dests @%rootdir%/destinations.cfg



# Introduction

Biot is a network-aware information pipe for the IoT.  Biot stands for
"Bridge for the Internet of Things", it aims at easily bridging
together the various APIs that are involved in IoT projects.  Simply
put biot accepts a number of sources, acquires data from these
sources, extracts any number of variables from this data and pushes
templates using these variables to any number of destinations.  In
other words, biot is an easy API masher for the Internet of Things.

biot supports a wide number of source and destinations types and makes the
difference between those using URL schemes. For example, biot implements `file`
and `exec` to receive or send data from and to local files and external
programs. In addition, biot supports schemes such as `http` (encrypted and clear
text HTTP), `ws` (encrypted and clear text WebSockets), `stomp`, `xmpp`,
`syslog`, `disque` (for getting/pushing jobs to [Disque][6] queues), `postgres`,
`mysql`, `pipe` (for interfacing to external programs), `mqtt`, etc. All these
schemes are flexible enough to, through options and sugaring, access remote
services through REST-like vocabularies or initiate topic subscriptions within
WebSockets protocols. 

Biot is tuned to work in modern cloud architectures through its
ability to be made available as a [docker][1] component.  Further, you
can easily instantiate as many biot information processing pipes in a
cluster using [fleet][2] (and [CoreOS][3]).  In this context, biot is
for example able to acquire its data from the etcd key-value store and
process data as instructed. This is achieved through the implementation
of an `etcd` URL scheme on top of the [etcd-tcl][5] library and the ability
for biot to acquire its command-line parameters using the `etcd`scheme
(or more regular `http` accesses).

For more details, consult the [Wiki][4].

  [1]: http://docker.io/
  [2]: https://github.com/coreos/fleet
  [3]: https://coreos.com/
  [4]: https://bitbucket.org/enbygg3/biot/wiki/Home
  [5]: https://github.com/efrecon/etcd-tcl
  [6]: https://github.com/antirez/disque